package org.alexsem.libcs.util;

/**
 * Instances of this interface implement the hyphenation algorithms for various languages
 */
public interface Hyphenator {

    /**
     * Perform hyphenation of the specified word
     * @param word          Word to hyphenate
     * @return array of positions at which hyphenation marks may be inserted
     */
    int[] hyphenateWord(String word);

}
