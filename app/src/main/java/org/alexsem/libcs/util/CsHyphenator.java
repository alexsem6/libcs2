package org.alexsem.libcs.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Church-Slavonic implementation of {@link org.alexsem.libcs.util.Hyphenator} interface
 */
public class CsHyphenator implements Hyphenator {


    private final String MARK = "/";
    private final String TYPE_x = "FPTVXfptvxЅѕБВГДЖЗКЛМНПРСТФХЦЧШЩбвгджзклмнпрстфхцчшщ";
    private final String TYPE_X = "9DGLRlr…ђ™¦§©®";
    private final String TYPE_o = "0ABEHIJKMNOQSUWYZaehijkmnoqsuwyz{|ЂЃѓ†‡‰ЉЊЌЋЏљ›њќћџЎўЈҐЁЇ±ІіґµёєјїАЕИОУЫЭЮЯаеиоуыэюя";
    private final String TYPE_O = "}‹№";
    private final String TYPE_i = "8_ЙЪЬйъь";
    private final String TYPE__ = "!\"#$%'()*,-.123456:;@[\\]^`~‚„‘’“”•–—¤Є«¬°¶·»";
    private final String TYPE_t = "&+7<=>?Cbcdg€";
    private final String[][] RULES = {
            {"ioo", "1"},
            {"iox", "1"},
            {"ixo", "1"},
            {"ixx", "1"},
            {"xooxo", "3"}, //2
            {"oxxxxo", "3"},
            {"oxxxo", "2"}, //3
            {"xoxo", "2"},
            {"oxxo", "2"},
            {"xooo", "2"},
            {"xoox", "2"}};
    private final int MAX_PREFIXES = 2;
    private final Set<String> PREFIX3 = new HashSet<String>(Arrays.asList(
            new String[]{"Не", "не", "Из", "из", "Ис", "ис", "По", "по", "Со", "со", "Пре", "пре", "При", "при", "Про", "про", "Пр0", "пр0", "Прi",
                    "прi", "Прі", "прі", "Прї", "прї", "Прј", "прј", "Вос", "вос", "Воз", "воз", "Рос", "рос", "Р0с", "р0с", "Роз", "роз", "Р0с", "р0с",
                    "Рас", "рас", "Рaс", "рaс", "Рaз", "рaз", "Без", "без", "Бeз", "бeз", "Бес", "бес", "Бeс", "бeс", "Все", "все", "Пред", "пред", "Пере",
                    "пере", "Пeре", "пeре", "Перe", "перe", "Перво", "перво", "Пeрво", "пeрво"}
    ));


    private boolean checkPrefixes = false;

    public CsHyphenator() {
    }

    public CsHyphenator(boolean checkPrefixes) {
        this.checkPrefixes = checkPrefixes;
    }

    /**
     * Defines whether more complex (and low) check is required
     * @param checkPrefixes tru to find common prefixes, false for regular (quicker check)
     */
    public void setCheckPrefixes(boolean checkPrefixes) {
        this.checkPrefixes = checkPrefixes;
    }

    /**
     * Define the type of specified character
     * @param source Character which type to define
     * @return char with certain code value
     */
    private char getLetterType(char source) {
        if (TYPE_x.indexOf(source) > -1 || TYPE_X.indexOf(source) > -1)
            return 'x';
        if (TYPE_o.indexOf(source) > -1 || TYPE_O.indexOf(source) > -1)
            return 'o';
        if (TYPE_i.indexOf(source) > -1)
            return 'i';
        if (TYPE__.indexOf(source) > -1 || TYPE_t.indexOf(source) > -1)
            return '_';
        return ' ';
    }

    /**
     * Define how many special characters are placed before the specified letter
     * @param specials Array of specials to look through
     * @param index    Position of letter in question
     * @return number of special characters
     */
    private int specialsBefore(int[] specials, int index) {
        int result = 0;
        for (int s : specials) {
            if (s > -1 && s <= index) {
                result++;
            }
        }
        return result;
    }

    /**
     * Increases values of all special which are greater than specified index
     * @param specials Array of specials to shift
     * @param index    Position at which shift begins
     */
    private void shiftSpecials(int[] specials, int index) {
        for (int i = 0; i < specials.length; i++) {
            if (specials[i] > index) {
                specials[i] = specials[i] + 1;
            }
        }
    }

    /**
     * Perform hyphenation of the specified word
     * @param text          Word to hyphenate
     * @return array of positions at which hyphenation marks may be inserted
     */
    public int[] hyphenateWord(String text) {
        if (text.length() <= 3) { //Word too short
            return new int[0];
        }
        int[] prefixIdx = new int[MAX_PREFIXES];
        int[] specials = new int[text.length()];
        StringBuilder appender = new StringBuilder();

        StringBuilder currentCode = new StringBuilder();
        String currentPrefix = "";
        int vowels = 0;
        int prefixes = 0;
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            char type = getLetterType(c);
            if (type == '_') { //Special character (should skip)
                specials[i] = currentCode.length() - prefixes;
            } else { //Regular letter
                specials[i] = -1;
                vowels += (type == 'o' ? 1 : 0);
                if (checkPrefixes) { //Need to check prefixes
                    if ((type != 'i' && prefixes < MAX_PREFIXES && PREFIX3.contains(currentPrefix))
                            & !(currentPrefix.equalsIgnoreCase("пре") && c == 'д' && i < text.length() - 1 && getLetterType(text.charAt(i + 1)) != 'o')) {
                        prefixIdx[prefixes] = currentCode.length() - prefixes;
                        currentCode.append(MARK);
                        currentPrefix = "";
                        prefixes++;
                    }
                    currentPrefix += c;
                }
                currentCode.append(type);
            }
        }
        String hyphenatedText = currentCode.toString();

        if (vowels <= 1) { //Only one syllable
            return new int[0];
        }

        if (checkPrefixes) {
            int offset = 0;
            for (int i = 0; i < prefixes; i++) {
                int actualIndex = prefixIdx[i] + offset;
                int specializedIndex = actualIndex + specialsBefore(specials, actualIndex);
                appender.setLength(0);
                text = appender.append(text.substring(0, specializedIndex)).append(MARK).append(text.substring(specializedIndex)).toString();
                shiftSpecials(specials, actualIndex);
                offset++;
            }
        }

        for (String[] rule : RULES) {
            int index = hyphenatedText.indexOf(rule[0]);
            while (index > -1) {
                int actualIndex = index + Integer.valueOf(rule[1]);
                int specializedIndex = actualIndex + specialsBefore(specials, actualIndex);
                appender.setLength(0);
                hyphenatedText = appender.append(hyphenatedText.substring(0, actualIndex)).append(MARK).append(hyphenatedText.substring(actualIndex)).toString();
                appender.setLength(0);
                text = appender.append(text.substring(0, specializedIndex)).append(MARK).append(text.substring(specializedIndex)).toString();
                shiftSpecials(specials, actualIndex);
                index = hyphenatedText.indexOf(rule[0]);
            }
        }

        String[] splittedText = text.split(MARK);
        int[] result = new int[splittedText.length - 1];
        for (int i = 0; i < result.length; i++) {
            result[i] = (i == 0 ? 0 : result[i - 1]) + splittedText[i].length();
        }
        return result;
    }

}
