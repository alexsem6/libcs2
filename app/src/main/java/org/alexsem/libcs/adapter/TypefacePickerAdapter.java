package org.alexsem.libcs.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.alexsem.libcs.activity.R;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;

/**
 * Adapter class used to represent list of available typefaces
 * @author Semeniuk A.D.
 */
public class TypefacePickerAdapter extends ArrayAdapter<String> {

	/**
	 * Modifies array of file names by removing extension part
	 * @param source Source
	 * @return Modified array
	 */
	private static List<String> trimFileExtensions(String[] source) {
		List<String> result = new ArrayList<String>();
		for (String item : source) {
			result.add(item.split("\\.")[0]);
		}
		Collections.sort(result);
		return result;
	}

	private final int RADIO_PADDING;

	private LayoutInflater inflater;
	private String currentItem = "";
	private Set<Integer> remoteItems = new HashSet<Integer>();

	public TypefacePickerAdapter(Context context, String[] data, String currentItem) {
		super(context, R.layout.picker_typeface_item, trimFileExtensions(data));
		this.currentItem = currentItem;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		float scale = context.getResources().getDisplayMetrics().density;
		if (Build.VERSION.SDK_INT < 17) { //Hack needed
			RadioButton rb = new RadioButton(context);
			this.RADIO_PADDING = rb.getPaddingLeft() + Math.round(8.0f * scale);
		} else { //Hack not needed
			this.RADIO_PADDING = Math.round(8.0f * scale);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RadioButton radio = (RadioButton) convertView;
		if (radio == null) {
			radio = (RadioButton) inflater.inflate(R.layout.picker_typeface_item, parent, false);
		}
		String model = this.getItem(position);
		radio.setText(model);
		radio.setChecked(model.equals(this.currentItem));
		radio.setPadding(RADIO_PADDING, radio.getPaddingTop(), radio.getPaddingRight(), radio.getPaddingBottom());
		radio.setEnabled(!isItemRemote(position));
		return radio;
	}

	/**
	 * Defines whether specified item is remote
	 * @param position Position of item in question
	 * @return true is item is remote, false otherwise
	 */
	public boolean isItemRemote(int position) {
		return this.remoteItems.contains(position);
	}

	public void setCurrentItem(String currentItem) {
		this.currentItem = currentItem;
		this.notifyDataSetChanged();
	}

	/**
	 * Adds list of items excluding those already present
	 * @param data List of Strings
	 * @return Number of added items
	 */
	public int appendAll(List<String> data) {
		int result = 0;
		for (String font : data) {
			if (this.getPosition(font) < 0) {
				this.remoteItems.add(this.getCount());
				this.add(font);
				result++;
			}
		}
		this.notifyDataSetChanged();
		return result;
	}

	/**
	 * Notifies adapter about remote item being downloaded
	 * @param position Position of loaded item
	 * @return Item at position
	 */
	public String remoteLoaded(int position) {
		String item = getItem(position);
		this.remoteItems.remove(position);
		this.setCurrentItem(item);
		this.notifyDataSetChanged();
		return item;
	}

}