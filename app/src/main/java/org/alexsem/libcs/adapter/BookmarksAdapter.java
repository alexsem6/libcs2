package org.alexsem.libcs.adapter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.alexsem.libcs.activity.R;
import org.alexsem.libcs.model.BmGroup;
import org.alexsem.libcs.model.Bookmark;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class BookmarksAdapter extends ArrayAdapter<Bookmark> {
    private Context context;
    private OnBookmarksItemClickListener onItemClickListener = null;
    private int groupId = 1;

    public BookmarksAdapter(Context context, List<Bookmark> data) {
        super(context, R.layout.bookmarks_item, data);
        this.context = context;
    }

    public void setOnItemClickListener(OnBookmarksItemClickListener l) {
        this.onItemClickListener = l;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder wrapper;

        if (row == null || !(row.getTag() instanceof ViewHolder)) {
            row = LayoutInflater.from(context).inflate(R.layout.bookmarks_item, parent, false);
            wrapper = new ViewHolder(row);
            wrapper.getExtra().setOnClickListener(extraClickListener);
            row.setOnClickListener(itemClickListener);
            row.setFocusable(false);
            row.setTag(R.id.bookmarks_item_name, wrapper);
        } else {
            wrapper = (ViewHolder) row.getTag(R.id.bookmarks_item_name);
        }

        Bookmark model = this.getItem(position);
        wrapper.getName().setText(model.getName());
        wrapper.getExtra().setTag(position);
        row.setTag(position);

        if (groupId > -1 && model.getGroupId() != groupId) { //Hide bookmarks from other books
            return new ViewStub(context);
        }
        return row;
    }

    @Override
    public void add(Bookmark object) {
        super.add(object);
        this.notifyDataSetChanged();
    }

    public void addAll(List<Bookmark> objects) {
        for (Bookmark bm : objects) {
            super.add(bm);
        }
        this.notifyDataSetChanged();
    }

    @Override
    public void remove(Bookmark object) {
        super.remove(object);
        this.notifyDataSetChanged();
    }

    /**
     * Removes bookmarks of the specific group
     * @param groupId Bookmark group identifier
     */
    public void removeGroup(int groupId) {
        Set<Bookmark> toDelele = new HashSet<Bookmark>();
        for (int i = 0; i < getCount(); i++) {
            if (getItem(i).getGroupId() == groupId) {
                toDelele.add(getItem(i));
            }
        }
        for (Bookmark bookmark : toDelele) {
            this.remove(bookmark);
        }
        notifyDataSetChanged();
    }

    //------------------------------------------------------------------------------------------

    private OnClickListener itemClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick((Integer) v.getTag());
            }
        }
    };

    private OnClickListener extraClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onExtraClick((Integer) v.getTag());
            }
        }
    };

    //------------------------------------------------------------------------------------------

    /**
     * Interface for item click listener
     * @author Semeniuk A.D.
     */
    public interface OnBookmarksItemClickListener {
        /**
         * Fires when item itself is clicked
         * @param position Item position within list
         */
        void onItemClick(int position);

        /**
         * Fires when extra button is clicked
         * @param position Item position within list
         */
        void onExtraClick(int position);
    }

    /**
     * Class used for view data storage
     * @author Semeniuk A.D.
     */
    private class ViewHolder {
        private View base;
        private TextView name = null;
        private ImageButton move = null;
        private View moveDivider = null;
        private ImageButton extra = null;

        /**
         * Constructor
         * @param base Parent view
         */
        public ViewHolder(View base) {
            this.base = base;
        }

        public TextView getName() {
            if (name == null) {
                name = (TextView) base.findViewById(R.id.bookmarks_item_name);
            }
            return (name);
        }

        public ImageButton getExtra() {
            if (extra == null) {
                extra = (ImageButton) base.findViewById(R.id.bookmarks_item_extra);
            }
            return (extra);
        }

    }

}
