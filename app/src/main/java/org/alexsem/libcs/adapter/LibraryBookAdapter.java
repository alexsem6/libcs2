package org.alexsem.libcs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.alexsem.libcs.activity.R;
import org.alexsem.libcs.model.Book;

import java.util.List;

public class LibraryBookAdapter extends ArrayAdapter<Book> {
    private Context context;
    private final String STATUS_OLD;
    private final String STATUS_NEW;
    private final String STATUS_UPD;
    private final String STATUS_DEL;
    private final String STATUS_IA;
    private final String STATUS_UPD_IA;
    private final String STATUS_DEL_IA;
    private LayoutInflater inflater = null;
    private boolean showDisabledBooks = true;

    public LibraryBookAdapter(Context context, List<Book> data) {
        super(context, R.layout.library_item_book, data);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.showDisabledBooks = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getBoolean("showDisabledBooks", true);
        this.STATUS_OLD = context.getString(R.string.library_status_old);
        this.STATUS_NEW = context.getString(R.string.library_status_new);
        this.STATUS_UPD = context.getString(R.string.library_status_upd);
        this.STATUS_DEL = context.getString(R.string.library_status_del);
        this.STATUS_IA = context.getString(R.string.library_status_ia);
        this.STATUS_UPD_IA = context.getString(R.string.library_status_upd_ia);
        this.STATUS_DEL_IA = context.getString(R.string.library_status_del_ia);
    }

    public void setShowDisabledBooks(boolean showDisabledBooks) {
        if (showDisabledBooks != this.showDisabledBooks) {
            this.showDisabledBooks = showDisabledBooks;
            this.notifyDataSetChanged();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder wrapper;

        if (row == null || !(row.getTag() instanceof ViewHolder)) {
            row = inflater.inflate(R.layout.library_item_book, parent, false);
            wrapper = new ViewHolder(row);
            row.setTag(wrapper);
        } else {
            wrapper = (ViewHolder) row.getTag();
        }

        Book model = this.getItem(position);
        wrapper.getTitle().setText(model.getName());
        wrapper.getTitle().setEnabled(model.isEnabled());
        wrapper.getStatus().setEnabled(model.isEnabled());
        switch (model.getTag()) {
            case 0: //OLD
                wrapper.getStatus().setText(model.isEnabled() ? STATUS_OLD : STATUS_IA);
                row.getBackground().setLevel(model.isEnabled() ? 0 : 5);
                break;
            case -1: //NEW
                wrapper.getStatus().setText(String.format(STATUS_NEW, model.getKb()));
                row.getBackground().setLevel(1);
                break;
            case -2: //UPD
                wrapper.getStatus().setText(String.format(model.isEnabled() ? STATUS_UPD : STATUS_UPD_IA, model.getKb()));
                row.getBackground().setLevel(2);
                break;
            case -3: //DEL
                wrapper.getStatus().setText(model.isEnabled() ? STATUS_DEL : STATUS_DEL_IA);
                row.getBackground().setLevel(3);
                break;
            case -4: //IGN
                return new ViewStub(context);
        }

        if (!showDisabledBooks && !model.isEnabled()) { //Hide disabled and ignored books
            return new ViewStub(context);
        }
        return row;
    }

    @Override
    public void remove(Book object) {
        super.remove(object);
        this.notifyDataSetChanged();
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Class used for view data storage
     * @author Semeniuk A.D.
     */
    private class ViewHolder {
        private View base;
        private TextView title = null;
        private TextView status = null;

        /**
         * Constructor
         * @param base Parent view
         */
        public ViewHolder(View base) {
            this.base = base;
        }

        public TextView getTitle() {
            if (title == null) {
                title = (TextView) base.findViewById(R.id.library_book_title);
            }
            return (title);
        }

        public TextView getStatus() {
            if (status == null) {
                status = (TextView) base.findViewById(R.id.library_book_status);
            }
            return (status);
        }
    }

}
