package org.alexsem.libcs.adapter;

import java.util.ArrayList;
import java.util.List;

import org.alexsem.libcs.activity.R;
import org.alexsem.libcs.model.HistoryItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class HistoryAdapter extends ArrayAdapter<HistoryItem> {
	private LayoutInflater inflater = null;
	private OnHistoryItemClickListener onItemClickListener = null;

	public HistoryAdapter(Context context, List<HistoryItem> data) {
		super(context, R.layout.history_item, data);
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setOnItemClickListener(OnHistoryItemClickListener l) {
		this.onItemClickListener = l;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder wrapper;

		if (row == null) {
			row = inflater.inflate(R.layout.history_item, parent, false);
			wrapper = new ViewHolder(row);
			row.setTag(R.id.history_item_name, wrapper);
		} else {
			wrapper = (ViewHolder) row.getTag(R.id.history_item_name);
		}

		HistoryItem model = this.getItem(position);
		wrapper.getName().setText(model.getName());
		wrapper.getDelete().setTag(position);
		wrapper.getDelete().setOnClickListener(deleteClickListener);
		row.setOnClickListener(rowClickListener);
		row.setTag(R.id.history_item_delete, position);
		row.setFocusable(false);
		return row;
	}

	/**
	 * Adds new history item
	 * @param item Item to add
	 */
	public void addHistoryItem(HistoryItem item) {
		this.remove(item);
		this.insert(item, 0);
		this.notifyDataSetChanged();
	}

	/**
	 * Trims list to the specified number of elements
	 * @param size Size in question
	 */
	public void trimToSize(int size) {
		List<HistoryItem> toDelete = new ArrayList<HistoryItem>();
		for (int i = size; i < this.getCount(); i++) {
			toDelete.add(getItem(i));
		}
		for (HistoryItem item : toDelete) {
			this.remove(item);
		}
		this.notifyDataSetChanged();
	}

	@Override
	public void clear() {
		super.clear();
		this.notifyDataSetChanged();
	}

	private OnClickListener rowClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (onItemClickListener != null) {
				onItemClickListener.onItemClick((Integer) v.getTag(R.id.history_item_delete));
			}
		}
	};

	private OnClickListener deleteClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			HistoryAdapter.this.remove(HistoryAdapter.this.getItem((Integer) v.getTag()));
			HistoryAdapter.this.notifyDataSetChanged();
		}
	};
	
	//----------------------------------------------------------------------------------------------

	/**
	 * Interface for item click listener
	 * @author Semeniuk A.D.
	 */
	public interface OnHistoryItemClickListener {
		public void onItemClick(int position);
	}
	
	/**
	 * Class used for view data storage
	 * @author Semeniuk A.D.
	 */
	private class ViewHolder {
		private View base;
		private TextView name = null;
		private ImageButton delete = null;

		/**
		 * Constructor
		 * @param base Parent view
		 */
		public ViewHolder(View base) {
			this.base = base;
		}

		public ImageButton getDelete() {
			if (delete == null) {
				delete = (ImageButton) base.findViewById(R.id.history_item_delete);
			}
			return (delete);
		}

		public TextView getName() {
			if (name == null) {
				name = (TextView) base.findViewById(R.id.history_item_name);
			}
			return (name);
		}

	}

}
