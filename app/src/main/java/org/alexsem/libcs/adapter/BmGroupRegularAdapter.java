package org.alexsem.libcs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.alexsem.libcs.activity.R;
import org.alexsem.libcs.model.BmGroup;

import java.util.List;

public class BmGroupRegularAdapter extends ArrayAdapter<BmGroup> {

  private LayoutInflater mInflater;
  private final String mDefaultName;

  public BmGroupRegularAdapter(Context context, List<BmGroup> data, String defName) {
    super(context, android.R.layout.simple_spinner_item, data);
    this.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    this.mInflater = LayoutInflater.from(context);
    this.mDefaultName = defName;
  }

  @Override
  public long getItemId(int position) {
    return getItem(position).getId();
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View row = convertView;
    ViewHolder wrapper;

    if (row == null) {
      row = mInflater.inflate(android.R.layout.simple_spinner_item, parent, false);
      wrapper = new ViewHolder(row);
      row.setTag(wrapper);
    } else {
      wrapper = (ViewHolder) row.getTag();
    }

    BmGroup model = this.getItem(position);
    wrapper.getName().setText(model.getId() == 1 ? mDefaultName : model.getName());
    return row;
  }

  @Override
  public View getDropDownView(int position, View convertView, ViewGroup parent) {
    View row = convertView;
    ViewHolder wrapper;

    if (row == null) {
      row = mInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
      wrapper = new ViewHolder(row);
      row.setTag(wrapper);
    } else {
      wrapper = (ViewHolder) row.getTag();
    }

    BmGroup model = this.getItem(position);
    wrapper.getName().setText(model.getId() == 1 ? mDefaultName : model.getName());
    return row;
  }

  //------------------------------------------------------------------------------------------

  /**
   * Class used for view data storage
   * @author Semeniuk A.D.
   */
  private class ViewHolder {
    private View base;
    private TextView name = null;

    /**
     * Constructor
     * @param base Parent view
     */
    public ViewHolder(View base) {
      this.base = base;
    }

    public TextView getName() {
      if (name == null) {
        name = (TextView) base.findViewById(android.R.id.text1);
      }
      return (name);
    }

  }

}
