package org.alexsem.libcs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.alexsem.libcs.model.BmGroup;

import java.util.ArrayList;
import java.util.List;

public class BmGroupMoveAdapter extends ArrayAdapter<BmGroup> {

  private LayoutInflater mInflater;
  private final String mDefaultName;

  /**
   * Excludes item with specified identifier from the list
   * @param source List to exclude item from
   * @param excludeId Identifier of item to exclude
   * @return Original list without specified item
   */
  private static List<BmGroup> excludeGroup(List<BmGroup> source, int excludeId) {
    List<BmGroup> result = new ArrayList<BmGroup>();
    for (BmGroup group: source) {
      if (group.getId() != excludeId) {
        result.add(group);
      }
    }
    return result;
  }

  public BmGroupMoveAdapter(Context context, List<BmGroup> data, String defName, int excludeId) {
    super(context, android.R.layout.select_dialog_item, excludeGroup(data, excludeId));
    this.mInflater = LayoutInflater.from(context);
    this.mDefaultName = defName;
  }

  @Override
  public long getItemId(int position) {
    return getItem(position).getId();
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View row = convertView;
    ViewHolder wrapper;

    if (row == null) {
      row = mInflater.inflate(android.R.layout.select_dialog_item, parent, false);
      wrapper = new ViewHolder(row);
      row.setTag(wrapper);
    } else {
      wrapper = (ViewHolder) row.getTag();
    }

    BmGroup model = this.getItem(position);
    wrapper.getName().setText(model.getId() == 1 ? mDefaultName : model.getName());
    return row;
  }

  //------------------------------------------------------------------------------------------

  /**
   * Class used for view data storage
   * @author Semeniuk A.D.
   */
  private class ViewHolder {
    private View base;
    private TextView name = null;

    /**
     * Constructor
     * @param base Parent view
     */
    public ViewHolder(View base) {
      this.base = base;
    }

    public TextView getName() {
      if (name == null) {
        name = (TextView) base.findViewById(android.R.id.text1);
      }
      return (name);
    }

  }

}
