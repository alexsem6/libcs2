package org.alexsem.libcs.adapter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

/**
 * Implementation of array adapter which utilizes Map functionality to create
 * grouped list with predefined headers
 * (these headers are represented differently than regular items)
 * @author Semeniuk A.D.
 * 
 * @param <T> Class that will be used for headers (parent items) model
 * @param <Q> Class that will be used for regular (child) items model
 */
public abstract class MapAdapter<T, Q> extends BaseAdapter {
	private final int TYPE_SECTION_HEADER = -100;

	private Context context;
	protected Map<T, ArrayAdapter<Q>> sections = new LinkedHashMap<T, ArrayAdapter<Q>>();
	protected ArrayAdapter<T> headers;

	private final int childLayout;

	/**
	 * Constructor
	 * @param context Context
	 * @param parentLayout Parent layout resource identifier
	 * @param childLayout Child layout resource identifier
	 */
	public MapAdapter(Context context, int parentLayout, int childLayout) {
		this.context = context;
		this.childLayout = childLayout;
		headers = new ArrayAdapter<T>(context, parentLayout);
	}

	/**
	 * Constructor
	 * @param context Context
	 * @param parentLayout Parent layout resource identifier
	 * @param childLayout Child layout resource identifier
	 * @param data Map structure which contains adapter data
	 */
	public MapAdapter(Context context, int parentLayout, int childLayout, Map<T, List<Q>> data) {
		this(context, parentLayout, childLayout);
		headers = new ArrayAdapter<T>(context, parentLayout, new ArrayList<T>(data.keySet()));
		for (T section : data.keySet()) {
			this.sections.put(section, new ArrayAdapter<Q>(context, childLayout, data.get(section)));
		}
	}

	@Override
	public final int getCount() {
		int total = 0;
		for (ArrayAdapter<Q> section : this.sections.values()) {
			total += section.getCount() + 1;
		}
		return total;
	}

	@Override
	public final Object getItem(int position) {
		for (T sect : this.sections.keySet()) {
			ArrayAdapter<Q> section = sections.get(sect);
			int size = section.getCount() + 1;

			if (position == 0) { //Header found
				return sect;
			} else if (position < size) { //Section data found
				return section.getItem(position - 1);
			} else { //Next section
				position -= size;
			}
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public abstract View getParentView(int parentIndex, View convertView, ViewGroup parent);

	public abstract View getChildView(int parentIndex, int childIndex, View convertView, ViewGroup parent);

	@Override
	public final View getView(int position, View convertView, ViewGroup parent) {
		int sectIndex = 0;
		for (T sect : this.sections.keySet()) {
			ArrayAdapter<Q> section = sections.get(sect);
			int size = section.getCount() + 1;

			if (position == 0) { //Header found
				return getParentView(sectIndex, convertView, parent);
			} else if (position < size) { //Section data found
				return getChildView(sectIndex, position - 1, convertView, parent);
			} else { //Next section
				position -= size;
				sectIndex++;
			}
		}
		return null;
	}

	/**
	 * Returns header of the specified section
	 * @param parentIndex Number of section
	 * @return section header
	 */
	public T getParentItem(int parentIndex) {
		return headers.getItem(parentIndex);
	}

	/**
	 * Returns specified item of the specified section
	 * @param parentIndex Number of section
	 * @param childIndex Number of item within section
	 * @return item data
	 */
	public Q getChildItem(int parentIndex, int childIndex) {
		return sections.get(headers.getItem(parentIndex)).getItem(childIndex);
	}

	//------------------ Make headers disabled ------------------
	@Override
	public int getViewTypeCount() {
		// assume that headers count as one, then total all sections  
		int total = 1;
		for (ArrayAdapter<Q> adapter : this.sections.values()) {
			total += adapter.getViewTypeCount();
		}
		return total;
	}

	@Override
	public int getItemViewType(int position) {
		int type = 1;
		for (T section : this.sections.keySet()) {
			ArrayAdapter<Q> adapter = sections.get(section);
			int size = adapter.getCount() + 1;

			if (position == 0) { //Header found
				return TYPE_SECTION_HEADER;
			} else if (position < size) { //Section data found
				return type + adapter.getItemViewType(position - 1);
			} else { //Next section
				position -= size;
				type += adapter.getViewTypeCount();
			}
		}
		return -1;
	}

	@Override
	public boolean isEnabled(int position) {
		return (getItemViewType(position) != TYPE_SECTION_HEADER);
	}

	//-----------------------------------------------------------

	/**
	 * Adds item to the specified section
	 * If section is not exists, it will be created
	 * @param parent Section to which item should be added
	 * @param child Item to add
	 */
	public void put(T parent, Q child) {
		if (headers.getPosition(parent) < 0) { //Section not present
			headers.add(parent);
			sections.put(parent, new ArrayAdapter<Q>(context, childLayout));
		}
		sections.get(parent).add(child);
	}

	/**
	 * Removes specific item from the specific section
	 * @param parent Section from which item should be removed
	 * @param child Item to remove
	 */
	public void remove(T parent, Q child) {
		if (sections.containsKey(parent)) {
			sections.get(parent).remove(child);
			if (sections.get(parent).getCount() <= 0) { //No items in section
				headers.remove(parent);
				sections.remove(parent);
			}
		}
	}

	/**
	 * Removes specific item from all sections
	 * @param child Item to remove
	 */
	public void remove(Q child) {
		Set<T> toDelete = new HashSet<T>();  
		for (T section : sections.keySet()) {
			sections.get(section).remove(child);
			if (sections.get(section).getCount() <= 0) { //No items in section
				toDelete.add(section);
			}
		}
		for (T section: toDelete) {
			headers.remove(section);
			sections.remove(section);
		}
	}

	/**
	 * Delete all items from the adapter
	 */
	public void clear() {
		headers.clear();
		sections.clear();
	}

	/**
	 * Sort specific section
	 * @param parent Section to sort
	 * @param comparator Comparator used for sorting
	 */
	public void sortSection(T parent, Comparator<? super Q> comparator) {
		if (sections.containsKey(parent)) {
			sections.get(parent).sort(comparator);
		}
	}

	/**
	 * Sort all sections and all items inside sections
	 * @param comparator Child items (items in sections) comparator
	 */
	public void sortAllSections(Comparator<? super Q> comparator) {
		for (T section : sections.keySet()) {
			sortSection(section, comparator);
		}
	}

}
