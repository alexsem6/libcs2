package org.alexsem.libcs.adapter;

import org.alexsem.libcs.activity.R;
import org.alexsem.libcs.model.Book;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

public class LibraryIgnoredAdapter extends MapAdapter<String, Book> {
  private final String STATUS_IGN;
  private LayoutInflater inflater = null;

  public LibraryIgnoredAdapter(Context context) {
    super(context, R.layout.library_item_section, R.layout.library_item_book);
    this.inflater = LayoutInflater.from(context);
    this.STATUS_IGN = context.getString(R.string.library_status_ign);
  }

  @Override
  public View getParentView(int parentIndex, View convertView, ViewGroup parent) {
    TextView row;
    if (convertView == null || !(convertView instanceof TextView)) {
      row = (TextView) inflater.inflate(R.layout.library_item_section, parent, false);
    } else {
      row = (TextView) convertView;
    }
    row.setText(this.getParentItem(parentIndex).toUpperCase(Locale.getDefault()));
    return row;
  }

  @Override
  public View getChildView(int parentIndex, int childIndex, View convertView, ViewGroup parent) {
    View row = convertView;
    BookHolder wrapper;

    if (row == null || !(row.getTag() instanceof BookHolder)) {
      row = inflater.inflate(R.layout.library_item_book, parent, false);
      wrapper = new BookHolder(row);
      row.setTag(wrapper);
    } else {
      wrapper = (BookHolder) row.getTag();
    }

    Book model = this.getChildItem(parentIndex, childIndex);
    wrapper.getTitle().setText(model.getName());
    wrapper.getTitle().setEnabled(false);
    wrapper.getStatus().setEnabled(false);
    wrapper.getStatus().setText(STATUS_IGN);
    row.getBackground().setLevel(4);

    return row;
  }

  //----------------------------------------------------------------------------------------------

  /**
   * Class used for view data storage
   * @author Semeniuk A.D.
   */
  private class BookHolder {
    private View base;
    private TextView title = null;
    private TextView status = null;

    /**
     * Constructor
     * @param base Parent view
     */
    public BookHolder(View base) {
      this.base = base;
    }

    public TextView getTitle() {
      if (title == null) {
        title = (TextView) base.findViewById(R.id.library_book_title);
      }
      return (title);
    }

    public TextView getStatus() {
      if (status == null) {
        status = (TextView) base.findViewById(R.id.library_book_status);
      }
      return (status);
    }
  }

}