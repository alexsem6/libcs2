package org.alexsem.libcs.activity;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import org.alexsem.libcs.model.Section;
import org.alexsem.libcs.transfer.DatabaseAdapter;
import org.alexsem.libcs.transfer.ServerConnector;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class UpdateService extends Service {

    private final int INTERVAL_FIRST_RUN = 5;

    private AlarmManager alarms;
    private DatabaseAdapter database;

    private UpdatesCheckingTask lastCheck = null;

    @Override
    public void onCreate() {
        alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        database = new DatabaseAdapter(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent intentToFire = new Intent(UpdateAlarmReceiver.ACTION_CHECK_UPDATES_ALARM);
        if (PendingIntent.getBroadcast(this, 0, intentToFire, PendingIntent.FLAG_NO_CREATE) == null) { //Alarm not set yet
            launchAlarm(INTERVAL_FIRST_RUN); //Set alarm to be launched
        } else { //Alarm triggered
            if (intent != null && intent.getExtras() != null) {
                if (intent.getExtras().containsKey("noupdate")) { //No need to restart timer
                    //Do nothing
                } else if (intent.getExtras().containsKey("ratechanged")) { //Rate preference changed
                    launchAlarm(INTERVAL_FIRST_RUN); //Set alarm to be launched
                }
            } else { //Not a nocheck intent
                int rate = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getString("notify_rate", "0"));
                if (rate > 0) {
                    performCheck();
                }
                launchAlarm(rate); //Set regular alarm
            }
        }
        return Service.START_NOT_STICKY;
    }

    /**
     * Launches the check updates alarm
     * rate Time interval on which to fire the Service
     */
    private void launchAlarm(int rate) {
        Intent intentToFire = new Intent(UpdateAlarmReceiver.ACTION_CHECK_UPDATES_ALARM);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intentToFire, 0);
        if (rate > 0) { //Need to check for updates
            int alarmType = AlarmManager.ELAPSED_REALTIME;
            long timeToRefresh = SystemClock.elapsedRealtime() + rate * 60 * 1000;
            alarms.setRepeating(alarmType, timeToRefresh, rate * 60 * 1000, alarmIntent);
        } else {
            alarms.cancel(alarmIntent);
            alarmIntent.cancel();
        }
    }

    /**
     * Launches the background task to check for updates
     */
    private void performCheck() {
        if (lastCheck == null || lastCheck.getStatus().equals(AsyncTask.Status.FINISHED)) {
            lastCheck = new UpdatesCheckingTask();
            lastCheck.execute();
        }
    }

    //=========================== ASYNCHRONOUS TASKS ===========================

    /**
     * Class which is used for Internet synchronization
     * @author Semeniuk A.D.
     */
    private class UpdatesCheckingTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                List<Section> remoteSections = ServerConnector.getAvailableSections();
                database.openRead();
                int newBooks = database.getNewModifiedBookCount(remoteSections, getSharedPreferences(getPackageName(), MODE_PRIVATE));
                database.close();
                return newBooks;
            } catch (Exception ex) { //Error happened
                return -1;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (result > -1) { //Success
                if (result > 0) {
                    String message;
                    if (result % 10 == 1 && result % 100 != 11) { //1, 21, 31, 101, 121... etc
                        message = String.format(getString(R.string.reader_update_found1), result);
                    } else if (result % 10 >= 2 && result % 10 <= 4 && !(result % 100 >= 12 && result % 100 <= 14)) { //2, 3, 4, 22, 23, 24 ... etc
                        message = String.format(getString(R.string.reader_update_found24), result);
                    } else {//Everything other
                        message = String.format(getString(R.string.reader_update_found00), result);
                    }
                    final NotificationManager mgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    Notification note = new Notification(R.drawable.icon, getString(R.string.reader_update_status), System.currentTimeMillis());
                    Intent intent = new Intent(UpdateService.this, LibraryActivity.class);
                    intent.putExtra("sync", true);
                    PendingIntent pending = PendingIntent.getActivity(UpdateService.this, 0, intent, 0);
                    try {
                        Method deprecatedMethod = note.getClass().getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class);
                        deprecatedMethod.invoke(note, UpdateService.this, getString(R.string.app_name), message, pending);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    note.flags |= Notification.FLAG_AUTO_CANCEL;
                    note.number = result;
                    mgr.notify(LibraryActivity.UPDATE_CHECK_NOTIFICATION_ID, note);
                }
                stopSelf();
            } else { //Error happened
                launchAlarm(INTERVAL_FIRST_RUN);
            }
        }
    }

}
