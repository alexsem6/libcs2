package org.alexsem.libcs.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.alexsem.libcs.model.OptionsActions;
import org.alexsem.libcs.model.QuickAction;
import org.alexsem.libcs.model.ReaderZone;
import org.alexsem.libcs.transfer.OptionsManager;
import org.alexsem.libcs.widget.ActionToolbar;
import org.alexsem.libcs.adapter.BmGroupMoveAdapter;
import org.alexsem.libcs.adapter.BmGroupRegularAdapter;
import org.alexsem.libcs.adapter.BmGroupStyledAdapter;
import org.alexsem.libcs.adapter.BookmarksAdapter;
import org.alexsem.libcs.adapter.BookmarksAdapter.OnBookmarksItemClickListener;
import org.alexsem.libcs.adapter.HistoryAdapter;
import org.alexsem.libcs.adapter.HistoryAdapter.OnHistoryItemClickListener;
import org.alexsem.libcs.model.BmGroup;
import org.alexsem.libcs.model.Book;
import org.alexsem.libcs.model.Bookmark;
import org.alexsem.libcs.model.HistoryItem;
import org.alexsem.libcs.model.OptionsMain;
import org.alexsem.libcs.model.Position;
import org.alexsem.libcs.model.Report;
import org.alexsem.libcs.model.Section;
import org.alexsem.libcs.transfer.DatabaseAdapter;
import org.alexsem.libcs.transfer.FileOperations;
import org.alexsem.libcs.transfer.ServerConnector;
import org.alexsem.libcs.widget.BookReader;
import org.alexsem.libcs.widget.BookReader.OnWordLinkClickedListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ReaderActivity extends Activity {

    private static final int REQUEST_PREFERENCES = 1;
    private static final int REQUEST_MANAGER = 2;
    private static final int REQUEST_ACTIONS = 3;


    private View reader;
    private View readerUpdate;
    private Button readerUpdateText;
    private ImageButton readerUpdateButton;
    private Animation readerUpdateShowAnimation;
    private Animation readerUpdateHideAnimation;

    private ActionToolbar bookmarks;
    private View bookmarksAdd;
    private Spinner bookmarksGroup;
    private View bookmarksGroupDelete;
    private BookmarksAdapter bookmarksAdapter;
    private List<BmGroup> bmGroups;
    private EditText bookmarksDialogName;
    private Spinner bookmarksDialogGroup;

    private ActionToolbar history;
    private ListView historyList;

    private View readerLoader;
    private Button readerEmpty;
    private BookReader readerReader = null;

    private Toast dictToast;
    private TextView dictToastWord;

    private Typeface regularFont;

    private OptionsMain optionsMain;
    private OptionsActions optionsActions;
    private String storagePath;
    private boolean updatesChecked = false;

    private DatabaseAdapter database;
    private BookLoaderTask loaderTask = null;

    private Book displayedBook = null; //Book which is currently displayed
    private List<String>[] displayedBookText = null; //Text of currently displayed book

    private Menu menu;

    @SuppressLint("ShowToast")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        //-------------------- First run --------------------
        final SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (!prefs.contains("SDpath")) { //No book path specified yet
            startActivity(new Intent(this, InitActivity.class));
            this.finish();
            return;
        } else { //Need to load and store book path
            storagePath = prefs.getString("SDpath", "");
        }

        //-------------------- Set content --------------------
        setContentView(R.layout.reader);
        database = new DatabaseAdapter(this);

        //-------------------- Reader field --------------------
        reader = findViewById(R.id.reader_main);
        readerReader = (BookReader) findViewById(R.id.reader_reader);
        readerReader.setDictionaryListener(readerDictionaryListener);
        readerReader.setErrorsListener(readerErrorsListener);
        readerReader.setTextSizeListener(readerTextSizeListener);
        readerReader.setQuickActionListener(readerQuickActionsListener);
        readerReader.setBrightnessListener(readerBrightnessChangedListener);
        readerReader.setTranscendCurrentBookListener(readerTranscendCurrentBookListener);
        readerLoader = findViewById(R.id.reader_loader);
        readerEmpty = (Button) findViewById(R.id.reader_empty);
        readerEmpty.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReaderActivity.this, LibraryActivity.class);
                intent.putExtra("read", true);
                startActivityForResult(intent, REQUEST_MANAGER);
            }
        });

        //-------------------- Dictionary integration --------------------
        dictToastWord = (TextView) getLayoutInflater().inflate(R.layout.dict_toast, null);
        dictToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        dictToast.setView(dictToastWord);

        //-------------------- Reader update --------------------
        readerUpdate = findViewById(R.id.reader_update);
        readerUpdateText = (Button) findViewById(R.id.reader_update_text);
        readerUpdateText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReaderActivity.this, LibraryActivity.class);
                intent.putExtra("sync", true);
                intent.putExtra("read", true);
                startActivityForResult(intent, REQUEST_MANAGER);
                readerUpdateButton.performClick();
            }
        });
        readerUpdateButton = (ImageButton) findViewById(R.id.reader_update_close);
        readerUpdateButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                readerUpdate.startAnimation(readerUpdateHideAnimation);
            }
        });
        readerUpdateShowAnimation = AnimationUtils.loadAnimation(this, R.anim.reader_update_show);
        readerUpdateHideAnimation = AnimationUtils.loadAnimation(this, R.anim.reader_update_hide);
        readerUpdateHideAnimation.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                readerUpdate.setVisibility(View.GONE);
            }
        });

        //-------------------- Bookmarks drawer --------------------
        bookmarks = (ActionToolbar) findViewById(R.id.bookmarks);
        bookmarks.setHotPoint(0f, 0f);
        bookmarks.setOrientation(ActionToolbar.Orientation.TOP_RIGHT);
        bookmarks.requestHide();
        bookmarks.setOnTouchListener(stub);
        bookmarks.setOnShowAnimationEndsListener(new ActionToolbar.OnAnimationEndListener() {
            @Override
            public void animationEnds() {
                bookmarks.bringToFront();
            }
        });

        //-------------------- Bookmarks list --------------------
        bookmarksAdapter = new BookmarksAdapter(this, new ArrayList<Bookmark>());
        bookmarksAdapter.setOnItemClickListener(bookmarksItemClickListener);
        ListView bookmarksList = (ListView) findViewById(R.id.bookmarks_list);
        bookmarksList.setEmptyView(findViewById(R.id.bookmarks_list_empty));
        bookmarksList.setAdapter(bookmarksAdapter);
        findViewById(R.id.bookmarks_title).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                bookmarks.hide();
            }
        });

        //-------------------- Bookmarks buttons --------------------
        bookmarksAdd = findViewById(R.id.bookmarks_add);
        bookmarksAdd.setOnClickListener(bookmarksAddClickListener);
        bookmarksGroup = (Spinner) findViewById(R.id.bookmarks_group);
        bookmarksGroupDelete = findViewById(R.id.bookmarks_group_delete);
        bookmarksGroupDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = String.format(Locale.getDefault(), getString(R.string.bookmarks_group_delprompt), bookmarksGroup.getSelectedItem().toString());
                new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.bookmarks_group_delete).setIcon(android.R.drawable.ic_dialog_alert).setMessage(message)
                        .setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                int id = (int) bookmarksGroup.getSelectedItemId();
                                database.openWrite();
                                database.deleteBmGroup(id);
                                database.close();
                                bmGroups.remove(bookmarksGroup.getSelectedItemPosition());
                                bookmarksGroup.setSelection(0);
                                bookmarksAdapter.removeGroup(id);
                            }
                        }).setNegativeButton(R.string.dialog_no, null).show();
            }
        });
        bookmarksGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                bookmarksAdapter.setGroupId((int) id);
                bookmarksGroupDelete.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        //-------------------- Load bookmarks group data --------------------
        database.openRead();
        bmGroups = database.selectBmGroups();
        bookmarksGroup.setAdapter(new BmGroupStyledAdapter(this, bmGroups, getString(R.string.bookmarks_group_default)));
        database.close();

        //-------------------- History drawer --------------------
        history = (ActionToolbar) findViewById(R.id.history);
        history.setHotPoint(0f, 0f);
        history.setOrientation(ActionToolbar.Orientation.TOP_RIGHT);
        history.requestHide();
        history.setOnTouchListener(stub);
        history.setOnShowAnimationEndsListener(new ActionToolbar.OnAnimationEndListener() {
            @Override
            public void animationEnds() {
                history.bringToFront();
            }
        });
        historyList = (ListView) findViewById(R.id.history_list);
        historyList.setEmptyView(findViewById(R.id.history_list_empty));
        findViewById(R.id.history_clear).setOnClickListener(historyClearClickListener);
        findViewById(R.id.history_title).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                history.hide();
            }
        });

        //-------------------- Quick Actions adjustments --------------------
        OptionsManager.saveAction(this, false, ReaderZone.CENTER, QuickAction.NAVIGATE_MENU_MAIN);

        //-------------------- Load preferences --------------------
        optionsMain = OptionsManager.loadSettings(this);
        optionsMain.daytime = prefs.getBoolean("daytime", true);
        optionsActions = OptionsManager.loadActions(this);
        updateFromPreferences();

        //-------------------- Load history --------------------
        database.openRead();
        historyList.setAdapter(new HistoryAdapter(this, database.selectHistory()));
        ((HistoryAdapter) historyList.getAdapter()).trimToSize(optionsMain.historySize);
        ((HistoryAdapter) historyList.getAdapter()).setOnItemClickListener(historyItemClickListener);
        historyList.setSelectionFromTop(0, 0);
        database.close();

        //-------------------- Load saved state --------------------
        int startingBookId = prefs.getInt("bookID", -1);
        if (startingBookId > -1) { //Need to load book
            showSpecificBook(startingBookId, null);
        } else { //Do not need to load anything
            showEmptyBook();
        }
        int bmGroupIndex = prefs.getInt("bmGroupIndex", 0);
        if (bmGroupIndex > -1 && bmGroupIndex < bmGroups.size()) {
            bookmarksGroup.setSelection(bmGroupIndex);
        }

        //-------------------- Start update service --------------------
        Intent updateIntent = new Intent(this, UpdateService.class);
        updateIntent.putExtra("noupdate", true);
        startService(updateIntent);

        //-------------------- Show "Whats new?" message --------------------
        try {
            String version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            if (!prefs.contains("version") || !prefs.getString("version", "").equals(version)) { //New version released
                prefs.edit().putString("version", version).commit();
                new AlertDialog.Builder(this).setTitle(R.string.whatsnew_title).setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(R.string.whatsnew_message).setPositiveButton(R.string.dialog_close, null).show();
            }
        } catch (PackageManager.NameNotFoundException ex) {
            //Do nothing
        }
    }

    //------------------------------------------------------------------------------------

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(updateReceiver, new IntentFilter(LibraryActivity.ACTION_LIBRARY_UPDATED));

        if (database == null) {
            return;
        }

        //-------------------- Load bookmarks --------------------
        database.openRead();
        bookmarksAdapter.clear();
        bookmarksAdapter.addAll(database.selectBookmarks());
        database.close();

        //-------------------- Check updates --------------------
        if (optionsMain.everyRun && !updatesChecked) {
            new UpdatesCheckingTask().execute((Void[]) null);
        }

        //-------------------- Send reports --------------------
        startService(new Intent(this, SendReportService.class));
    }

    //------------------------------------------------------------------------------------

    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (optionsMain.immersive) { //Need to seitch to immersive mode
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            } else { //Just use low-profile instead
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LOW_PROFILE
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_FULLSCREEN);
            }
        }
    }

    //------------------------------------------------------------------------------------

    /**
     * Opens specific book in reader (restoring last position)
     * @param bookId   ID of book to be displayed or -1 for first book in section
     * @param position Position to open in new book or null to open last position
     */
    private void showSpecificBook(int bookId, Position position) {
        if (displayedBook != null) { //Store current book position
            database.openWrite();
            database.updatePosition(displayedBook.getId(), readerReader.getCurrentPosition(false));
            database.close();
        }
        if (loaderTask != null) {
            loaderTask.cancel(true);
        }
        loaderTask = new BookLoaderTask(bookId, position);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            loaderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            loaderTask.execute();
        }
    }

    /**
     * Performs actions which reset all needed fields and variables
     * in case no book is needed to be shown
     */
    private void showEmptyBook() {
        readerReader.setVisibility(View.INVISIBLE);
        readerEmpty.setVisibility(View.VISIBLE);
        displayedBook = null;
        displayedBookText = null;
        bookmarksAdd.setVisibility(View.GONE);
    }

    //------------------------------------------------------------------------------------

    /**
     * Generates suggestion for the bookmark name
     * based on the names of currently displayed book and chapter
     * @return Generated name
     */
    private String generateBookmarkName() {
        StringBuilder result = new StringBuilder();
        if (displayedBook != null) {
            String[] words = displayedBook.getName().replaceAll("[\\-\\(\\),\"]", " ").split(" ");
            for (String word : words) {
                if (word.length() > 2) {
                    result.append(word.substring(0, 1).toUpperCase());
                    result.append(word.substring(1, 3));
                } else if (word.matches("^[0-9].*")) {
                    result.append(word);
                }

            }
            int chapter = readerReader.getCurrentChapter();
            if (chapter > -1 && chapter < displayedBook.getChapters().size()) {
                result.append(':').append(' ');
                String[] chapWords = displayedBook.getChapters().get(chapter).getName().replace('-', ' ').split(" ");
                for (int i = 0; i < chapWords.length; i++) {
                    if (i == 0 || chapWords[i].length() == 0) { //First word
                        result.append(chapWords[i]);
                    } else { //Subsequent words
                        result.append(chapWords[i].substring(0, 1).toUpperCase());
                        result.append(chapWords[i].substring(1, Math.min(3, chapWords[i].length())));
                    }
                }
            }
        }
        return result.toString();
    }

    /**
     * Shows dialog which is used for bookmark adding
     * @param name       Default bookmark name to use (or null to generate new one)
     * @param groupIndex Selected bookmark group position
     */
    private void showAddBookmarkDialog(String name, int groupIndex) {
        View layout = getLayoutInflater().inflate(R.layout.bookmarks_add_dialog, null);
        bookmarksDialogName = (EditText) layout.findViewById(R.id.bookmarks_name);
        bookmarksDialogGroup = (Spinner) layout.findViewById(R.id.bookmarks_group);
        bookmarksDialogName.setText(name != null ? name : generateBookmarkName());
        bookmarksDialogName.setSelection(bookmarksDialogName.getText().length());
        bookmarksDialogGroup.setAdapter(new BmGroupRegularAdapter(ReaderActivity.this, bmGroups, getString(R.string.bookmarks_group_default)));
        bookmarksDialogGroup.setSelection(groupIndex > -1 ? groupIndex : bookmarksGroup.getSelectedItemPosition());
        new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.bookmarks_add).setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (bookmarksDialogName.getText().length() > 0) {
                            addBookmark(bookmarksDialogName.getText().toString(), (int) bookmarksDialogGroup.getSelectedItemId());
                            bookmarksGroup.setSelection(bookmarksDialogGroup.getSelectedItemPosition());
                        }
                    }
                })
                .setNeutralButton(R.string.dialog_add_group, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showAddBookmarkDialog(bookmarksDialogName.getText().toString(), bookmarksDialogGroup.getSelectedItemPosition());
                        View layout = getLayoutInflater().inflate(R.layout.bookmarks_rename_dialog, null);
                        final EditText grN = (EditText) layout.findViewById(R.id.bookmarks_name);
                        grN.setText(String.format(Locale.getDefault(), getString(R.string.bookmarks_group_new), bmGroups.size()));
                        grN.setSelection(grN.getText().length());
                        new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.bookmarks_group_add).setIcon(android.R.drawable.ic_dialog_info)
                                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (grN.getText().length() > 0) {
                                            BmGroup group = new BmGroup();
                                            group.setName(grN.getText().toString());
                                            database.openWrite();
                                            group.setId((int) database.insertBmGroup(group));
                                            database.close();
                                            if (group.getId() > -1) {
                                                bmGroups.add(group);
                                                bookmarksDialogGroup.setSelection(bmGroups.size() - 1);
                                            }
                                        }
                                    }
                                }).setNegativeButton(R.string.dialog_cancel, null).setView(layout).show();
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, null).setView(layout).show();
    }


    /**
     * Adds new bookmark
     * @param name    Bookmark name
     * @param groupId Bookmark group identifier
     */
    private void addBookmark(String name, int groupId) {
        Bookmark bookmark = new Bookmark(name);
        bookmark.setBookId(displayedBook.getId());
        Position position = readerReader.getCurrentPosition(true);
        if (position == null) {
            position = new Position(-1);
        }
        bookmark.setChapterNumber(position.getChapterNumber());
        bookmark.setLineNumber(position.getLineNumber());
        bookmark.setCharIndex(position.getCharIndex());
        bookmark.setGroupId(groupId);
        bookmarksAdapter.add(bookmark);
    }

    //------------------------------------------------------------------------------------

    @Override
    public void openOptionsMenu() {
        Configuration config = getResources().getConfiguration();
        if ((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_LARGE) {
            int originalScreenLayout = config.screenLayout;
            config.screenLayout = Configuration.SCREENLAYOUT_SIZE_LARGE;
            super.openOptionsMenu();
            config.screenLayout = originalScreenLayout;
        } else {
            super.openOptionsMenu();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(getApplication()).inflate(R.menu.main, menu);
        this.menu = menu;
        return (super.onCreateOptionsMenu(menu));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.getItem(0).setEnabled(displayedBook != null);
        menu.getItem(5).getSubMenu().getItem(1).setVisible(!optionsMain.daytime);
        menu.getItem(5).getSubMenu().getItem(2).setVisible(optionsMain.daytime);
        menu.getItem(5).getSubMenu().getItem(3).setEnabled(displayedBook != null && readerReader.canShowWordLinks());
        menu.getItem(5).getSubMenu().getItem(4).setEnabled(displayedBook != null && readerReader.canShowWordLinks());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_contents:
                readerReader.switchContents(true);
                break;
            case R.id.menu_bookmarks:
                performAction(QuickAction.NAVIGATE_BOOKMARKS);
                break;
            case R.id.menu_history:
                performAction(QuickAction.NAVIGATE_HISTORY);
                break;
            case R.id.menu_library:
                performAction(QuickAction.NAVIGATE_LIBRARY);
                break;
            case R.id.menu_settings:
                performAction(QuickAction.NAVIGATE_SETTINGS_MAIN);
                break;
            case R.id.menu_actions:
                performAction(QuickAction.NAVIGATE_SETTINGS_ACTIONS);
                break;
            case R.id.menu_day:
                optionsMain.daytime = true;
                reader.setBackgroundColor(optionsMain.fc_back_color_day);
                readerReader.invalidate();
                break;
            case R.id.menu_night:
                optionsMain.daytime = false;
                reader.setBackgroundColor(optionsMain.fc_back_color_night);
                readerReader.invalidate();
                break;
            case R.id.menu_dictionary:
                readerReader.switchDictionary(true);
                break;
            case R.id.menu_errors:
                readerReader.switchErrors(true);
                break;
            case R.id.menu_manual:
                performAction(QuickAction.NAVIGATE_MANUAL);
                break;
            case R.id.menu_exit:
                performAction(QuickAction.NAVIGATE_EXIT);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    //------------------------------------------------------------------------------------

    /**
     * Performs action (passed from  BookReader)
     * @param action Action to perform
     */
    public void performAction(QuickAction action) {
        if (action == null) {
            return;
        }
        switch (action) {
            case GOTO_NEXT_PAGE_1:
                readerReader.goToNextPage(1);
                break;
            case GOTO_NEXT_PAGE_5:
                readerReader.goToNextPage(5);
                break;
            case GOTO_NEXT_PAGE_10:
                readerReader.goToNextPage(10);
                break;
            case GOTO_PREVIOUS_PAGE_1:
                readerReader.goToPreviousPage(1);
                break;
            case GOTO_PREVIOUS_PAGE_5:
                readerReader.goToPreviousPage(5);
                break;
            case GOTO_PREVIOUS_PAGE_10:
                readerReader.goToPreviousPage(10);
                break;
            case GOTO_FIRST_PAGE:
                readerReader.goToFirstPage();
                break;
            case GOTO_LAST_PAGE:
                readerReader.goToLastPage();
                break;
            case GOTO_NEXT_CHAPTER:
                readerReader.goToNextChapter();
                break;
            case GOTO_PREVIOUS_CHAPTER:
                readerReader.goToPreviousChapter();
                break;
            case GOTO_NEXT_BOOK:
                readerTranscendCurrentBookListener.onGoToNextBook();
                break;
            case GOTO_PREVIOUS_BOOK:
                readerTranscendCurrentBookListener.onGoToPreviousBook();
                break;
            case NAVIGATE_CONTENTS:
                readerReader.switchContents(true);
                break;
            case MODE_SWITCH_DICTIONARY:
                readerReader.switchDictionary(true);
                break;
            case MODE_SWITCH_ERRORS:
                readerReader.switchErrors(true);
                break;
            case NAVIGATE_MENU_MAIN:
                this.openOptionsMenu();
                break;
            case NAVIGATE_MENU_OTHER:
                this.openOptionsMenu();
                if (this.menu != null) {
                    this.menu.performIdentifierAction(R.id.menu_other, 0);
                }
                break;
            case NAVIGATE_HISTORY:
                if (bookmarks.isOpened()) {
                    bookmarks.hide();
                }
                if (!history.isOpened()) {
                    history.show();
                }
                break;
            case GOTO_HISTORY_RECENT:
                if (historyList != null && historyList.getCount() > 1) {
                    showSpecificBook(((HistoryAdapter) historyList.getAdapter()).getItem(1).getBookId(), null);
                }
                break;
            case NAVIGATE_BOOKMARKS:
                if (history.isOpened()) {
                    history.hide();
                }
                if (!bookmarks.isOpened()) {
                    bookmarksAdd.setVisibility(displayedBook != null && !readerReader.isContentsShowing() ? View.VISIBLE : View.GONE);
                    bookmarks.show();
                }
                break;
            case ADD_BOOKMARK_QUICK:
                if (bookmarksAdd.isEnabled()) {
                    String name = generateBookmarkName();
                    addBookmark(name, 1);
                    Toast.makeText(this, String.format(getString(R.string.bookmarks_created), name), Toast.LENGTH_SHORT).show();
                }
                break;
            case NAVIGATE_LIBRARY:
                Intent intent = new Intent(this, LibraryActivity.class);
                intent.putExtra("read", true);
                if (displayedBook != null) {
                    intent.putExtra("bookID", displayedBook.getId());
                }
                startActivityForResult(intent, REQUEST_MANAGER);
                break;
            case NAVIGATE_SETTINGS_MAIN:
                if (bookmarks.isOpened()) {
                    bookmarks.hide();
                }
                if (history.isOpened()) {
                    history.hide();
                }
                startActivityForResult(new Intent(this, EditPreferences.class), REQUEST_PREFERENCES);
                break;
            case NAVIGATE_SETTINGS_ACTIONS:
                if (bookmarks.isOpened()) {
                    bookmarks.hide();
                }
                if (history.isOpened()) {
                    history.hide();
                }
                startActivityForResult(new Intent(this, ActionsActivity.class), REQUEST_ACTIONS);
                break;
            case NAVIGATE_SETTINGS_FONTS:
                if (bookmarks.isOpened()) {
                    bookmarks.hide();
                }
                if (history.isOpened()) {
                    history.hide();
                }
                Intent i1 = new Intent(this, EditPreferences.class);
                i1.putExtra("fonts", true);
                startActivityForResult(i1, REQUEST_PREFERENCES);
                break;
            case MODE_SWITCH_DAYNIGHT:
                optionsMain.daytime = !optionsMain.daytime;
                reader.setBackgroundColor(optionsMain.daytime ? optionsMain.fc_back_color_day : optionsMain.fc_back_color_night);
                readerReader.invalidate();
                break;
            case BRIGHTNESS_INCREASE:
                readerReader.increaseBrightness();
                break;
            case BRIGHTNESS_DECREASE:
                readerReader.decreaseBrightness();
                break;
            case NAVIGATE_MANUAL:
                startActivity(new Intent(this, HelpActivity.class));
                break;
            case NAVIGATE_EXIT:
                this.finish();
                break;
        }
    }

    //------------------------------------------------------------------------------------

    @Override
    protected void onPause() {
        super.onPause();
        if (database == null) {
            return;
        }
        //--------------- Save current state ---------------
        SharedPreferences.Editor edit = getSharedPreferences(getPackageName(), MODE_PRIVATE).edit();
        edit.putBoolean("daytime", optionsMain.daytime);
        edit.remove("bookID");
        if (displayedBook != null) { //Book opened
            try {
                database.openWrite();
                database.updatePosition(displayedBook.getId(), readerReader.getCurrentPosition(false));
                database.close();
            } catch (SQLException ex) {
                //Do nothing
            }
            edit.putInt("bookID", displayedBook.getId()).commit();
        } else { //No books opened
            edit.remove("bookID");
        }
        edit.putInt("bmGroupIndex", bookmarksGroup.getSelectedItemPosition());
        edit.commit();
    }

    //------------------------------------------------------------------------------------

    /**
     * Populate necessary preferences
     */
    @SuppressLint("WrongConstant")
    private void updateFromPreferences() {
        try {
            regularFont = Typeface.createFromFile(new File(new File(storagePath, getString(R.string.typeface_folder)), optionsMain.fc_text_face + ".ttf"));
        } catch (Exception ex) {
            regularFont = Typeface.createFromAsset(getAssets(), getString(R.string.typeface_regular_asset));
        }
        dictToastWord.setTypeface(regularFont);
        onWindowFocusChanged(true);
        readerReader.postDelayed(new Runnable() {
            @Override
            public void run() {
                readerReader.setOptions(optionsMain, regularFont);
            }
        }, 100);
        reader.setBackgroundColor(optionsMain.fc_back_color());
        switch (optionsMain.orientation) {
            case 0: //Sensor
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                break;
            case 1: //Portrait (0)
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            case 2: //Landscape (90)
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                break;
            case 3: //Portrait (180)
                setRequestedOrientation(9);
                break;
            case 4: //Landscape (270)
                setRequestedOrientation(8);
                break;
            case 5: //Sensor Portrait
                setRequestedOrientation(7);
                break;
            case 6: //Sensor Landscape
                setRequestedOrientation(6);
                break;
            case 7: //System
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
                break;
        }
    }

    //------------------------------------------------------------------------------------

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_PREFERENCES: //Preferences changed
                    optionsMain = OptionsManager.loadSettings(this);
                    updateFromPreferences();
                    break;
                case REQUEST_ACTIONS: //Quick actions setup changed
                    optionsActions = OptionsManager.loadActions(this);
                    break;
                case REQUEST_MANAGER: //Books changed
                    int bookId = data.getIntExtra("bookId", -2);
                    if (bookId > -2) {
                        showSpecificBook(bookId, null);
                    }
                    updateFromPreferences();
                    break;
            }
        }
    }

    //------------------------------------------------------------------------------------

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_VOLUME_UP:
                    if (readerReader.isContentsShowing()) {
                        readerReader.goToContentsPreviousPage();
                    } else {
                        performAction(optionsActions.actionsVolume.get(true));
                    }
                    return true;
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                    if (readerReader.isContentsShowing()) {
                        readerReader.goToContentsNextPage();
                    } else {
                        performAction(optionsActions.actionsVolume.get(false));
                    }
                    return true;
            }
        }
        return (event.getAction() == KeyEvent.ACTION_UP && (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN)) || super.dispatchKeyEvent(event);
    }

    //------------------------------------------------------------------------------------

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(updateReceiver);
        if (database == null) {
            return;
        }
        readerUpdate.setVisibility(View.GONE); //Hide updates found field

        //--------------- Save bookmarks list ---------------
        try {
            if (bookmarksAdapter != null) {
                database.openWrite();
                database.deleteBookmarks();
                for (int i = 0; i < bookmarksAdapter.getCount(); i++) {
                    database.insertBookmark(bookmarksAdapter.getItem(i));
                }
                database.close();
            }
        } catch (Exception ex) {
            Toast.makeText(this, R.string.reader_error_bookmarks, Toast.LENGTH_SHORT).show();
        }

        //--------------- Save history ---------------
        try {
            if (historyList.getAdapter() != null) {
                database.openWrite();
                database.deleteHistory();
                for (int i = 0; i < historyList.getAdapter().getCount(); i++) {
                    database.insertHistory(((HistoryAdapter) historyList.getAdapter()).getItem(i));
                }
                database.close();
            }
        } catch (Exception ex) {
            Toast.makeText(this, R.string.reader_error_history, Toast.LENGTH_SHORT).show();
        }
    }

    //------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        if (bookmarks != null && bookmarks.isOpened()) {
            bookmarks.hide();
            return;
        }
        if (history != null && history.isOpened()) {
            history.hide();
            return;
        }
        if (readerUpdate.getVisibility() == View.VISIBLE) {
            readerUpdateButton.performClick();
            return;
        }

        if (readerReader.isContentsShowing()) { //Contents visible
            readerReader.switchContents(false);
            return;
        }
        if (readerReader.isDictionaryEnabled()) { //Dictionary links visible
            readerReader.switchDictionary(false);
            return;
        }
        if (readerReader.isErrorsEnabled()) { //Error links visible
            readerReader.switchErrors(false);
            return;
        }
        if (readerReader.isChapterLinkShowing()) {
            readerReader.returnFromChapterLink();
            return;
        }

        //--- Exit application ---
        new AlertDialog.Builder(this).setTitle(R.string.reader_exit_title).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(R.string.reader_exit_prompt).setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ReaderActivity.this.finish();
            }
        }).setNegativeButton(R.string.dialog_no, null).show();
    }

    //=========================== CLASSES ===========================

    /**
     * Receiver for "Library updated" action
     */
    private final BroadcastReceiver updateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals(LibraryActivity.ACTION_LIBRARY_UPDATED) && database != null) {
                readerUpdate.setVisibility(View.GONE); //Hide updates found field
                // ------ Temporary storing current display indexes ------
                if (displayedBook != null) {
                    showSpecificBook(displayedBook.getId(), null);
                }
            }
        }
    };

    //=========================== LISTENERS ===========================

    /**
     * Prevents touches to be sent to reader view "through" bookmarks or history
     * views
     */
    private OnTouchListener stub = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return bookmarks.isOpened() || history.isOpened();
        }
    };

    /**
     * Listener for bookmarks "Add" button
     */
    private OnClickListener bookmarksAddClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (displayedBook != null) {
                showAddBookmarkDialog(null, -1);
            }
        }
    };

    /**
     * Listener for bookmarks item clicks
     */
    private OnBookmarksItemClickListener bookmarksItemClickListener = new OnBookmarksItemClickListener() {
        @Override
        public void onItemClick(int position) {
            bookmarks.hide();
            Bookmark bookmark = bookmarksAdapter.getItem(position);
            Position pos = new Position(bookmark.getChapterNumber(), bookmark.getLineNumber(), bookmark.getCharIndex());
            if (displayedBook != null && displayedBook.getId() == bookmark.getBookId()) { //Bookmark from current book
                readerReader.requestPositionRelocation(pos, false);
            } else { //Bookmark from another book
                showSpecificBook(bookmark.getBookId(), pos);
            }
        }

        @Override
        public void onExtraClick(final int position) {
            List<String> items = new ArrayList<String>();
            items.add(getString(R.string.bookmarks_extra_rename));
            if (bmGroups.size() > 1) {
                items.add(getString(R.string.bookmarks_extra_move));
            }
            items.add(getString(R.string.bookmarks_extra_delete));
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(ReaderActivity.this, android.R.layout.select_dialog_item, items);
            new AlertDialog.Builder(ReaderActivity.this)
                    .setTitle(R.string.bookmarks_extra_choose)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setAdapter(adapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0: //Rename
                                    renameBookmark(position);
                                    break;
                                case 1: //Move/Delete
                                    if (bmGroups.size() > 1) {
                                        moveBookmark(position);
                                    } else {
                                        deleteBookmark(position);
                                    }
                                    break;
                                case 2: //Delete
                                    deleteBookmark(position);
                                    break;
                            }
                        }
                    }).setNegativeButton(R.string.dialog_cancel, null).show();

        }

        /**
         * Delete bookmark
         * @param position Position in the list
         */
        public void renameBookmark(int position) {
            final Bookmark bookmark = bookmarksAdapter.getItem(position);
            View layout = getLayoutInflater().inflate(R.layout.bookmarks_rename_dialog, null);
            final EditText grN = (EditText) layout.findViewById(R.id.bookmarks_name);
            grN.setText(bookmark.getName());
            grN.setSelection(grN.getText().length());
            new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.bookmarks_rename)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (grN.getText().length() > 0) {
                                bookmark.setName(grN.getText().toString());
                                bookmarksAdapter.notifyDataSetChanged();
                            }
                        }
                    }).setNegativeButton(R.string.dialog_cancel, null).setView(layout).show();
        }

        /**
         * Move bookmark to other group
         * @param position Position in the list
         */
        public void moveBookmark(final int position) {
            final Bookmark item = bookmarksAdapter.getItem(position);
            final BmGroupMoveAdapter adapter = new BmGroupMoveAdapter(ReaderActivity.this, bmGroups, getString(R.string.bookmarks_group_default), bookmarksAdapter.getItem(position).getGroupId());
            new AlertDialog.Builder(ReaderActivity.this)
                    .setTitle(R.string.bookmarks_group_move)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setAdapter(adapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            item.setGroupId(adapter.getItem(which).getId());
                            bookmarksAdapter.notifyDataSetChanged();
                        }
                    }).setNegativeButton(R.string.dialog_cancel, null).show();
        }

        /**
         * Delete bookmark
         * @param position Position in the list
         */
        public void deleteBookmark(final int position) {
            new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.bookmarks_delete)
                    .setMessage(String.format(getString(R.string.bookmarks_delete_prompt), bookmarksAdapter.getItem(position).getName()))
                    .setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            bookmarksAdapter.remove(bookmarksAdapter.getItem(position));
                        }
                    }).setNegativeButton(R.string.dialog_no, null).show();
        }

    };

    /**
     * Listener for history item clicks
     */
    private OnHistoryItemClickListener historyItemClickListener = new OnHistoryItemClickListener() {
        @Override
        public void onItemClick(int position) {
            history.hide();
            showSpecificBook(((HistoryAdapter) historyList.getAdapter()).getItem(position).getBookId(), null);
        }
    };

    /**
     * Listener for history "Clear" button
     */
    private OnClickListener historyClearClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.history_clear).setMessage(R.string.history_clear_propmt)
                    .setIcon(android.R.drawable.ic_dialog_alert).setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ((HistoryAdapter) historyList.getAdapter()).clear();
                    history.hide();
                }
            }).setNegativeButton(R.string.dialog_no, null).show();
        }
    };

    /**
     * Listener for dictionary links click
     */
    private OnWordLinkClickedListener readerDictionaryListener = new OnWordLinkClickedListener() {
        @Override
        public void onLinkClicked(String word, int chapter, int line, int index) {
            dictToastWord.setText(word);
            dictToast.show();
            Intent intent = new Intent("org.alexsem.diccs.SEARCH_WORD");
            intent.putExtra("word", word);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.dict_notfound_title).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(R.string.dict_notfound_message).setPositiveButton(R.string.dialog_load, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=org.alexsem.diccs.activity")));
                        } catch (Exception ex1) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                        .parse("https://play.google.com/store/apps/details?id=org.alexsem.diccs.activity")));
                            } catch (Exception ex2) {
                                Toast.makeText(ReaderActivity.this, R.string.dict_error_loadfail, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }).setNegativeButton(R.string.dialog_cancel, null).show();
            }
        }
    };

    /**
     * Listener for error in text links click
     */
    private OnWordLinkClickedListener readerErrorsListener = new OnWordLinkClickedListener() {
        @Override
        public void onLinkClicked(String word, int chapter, int line, int index) {
            View layout = getLayoutInflater().inflate(R.layout.report_dialog, null);
            final TextView tvW = (TextView) layout.findViewById(R.id.report_word);
            final EditText tvC = (EditText) layout.findViewById(R.id.report_comment);
            tvW.setTypeface(regularFont);
            tvW.setText(word);
            final int iC = displayedBook.getChapters().get(chapter).getId();
            final int iL = line;
            final int iI = index;
            new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.report_title).setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton(R.string.dialog_send, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Report report = new Report(tvW.getText().toString(), tvC.getText().toString(), iC, iL, iI);
                            database.openWrite();
                            database.insertReport(report);
                            database.close();
                            Toast.makeText(ReaderActivity.this, R.string.report_sent, Toast.LENGTH_SHORT).show();
                            startService(new Intent(ReaderActivity.this, SendReportService.class));
                        }
                    }).setNegativeButton(R.string.dialog_cancel, null).setView(layout).show();
        }
    };

    /**
     * Listener for changes in text size
     */
    private BookReader.OnTextSizeChangedListener readerTextSizeListener = new BookReader.OnTextSizeChangedListener() {
        @Override
        public void onTextSizeChanged(float newSize) {
            String size = String.valueOf(Math.round(newSize / getResources().getDisplayMetrics().density));
            PreferenceManager.getDefaultSharedPreferences(ReaderActivity.this).edit().putString("fc_text_size", size).commit();
        }
    };

    /**
     * Listener for quick actions
     */
    private BookReader.OnQuickActionsListener readerQuickActionsListener = new BookReader.OnQuickActionsListener() {
        @Override
        public void onQuickActionTriggered(ReaderZone zone, boolean isLong) {
            if (isLong) {
                performAction(optionsActions.actionsLong.get(zone));
            } else {
                performAction(optionsActions.actionsShort.get(zone));
            }
        }

        @Override
        public boolean checkNextPageLongAction(ReaderZone zone) {
            return optionsActions.actionsShort.get(zone) == QuickAction.GOTO_NEXT_PAGE_LONG;
        }

        @Override
        public boolean checkPreviousPageLongAction(ReaderZone zone) {
            return optionsActions.actionsShort.get(zone) == QuickAction.GOTO_PREVIOUS_PAGE_LONG;
        }
    };

    /**
     * Listener for brightness changes
     */
    private BookReader.OnBrightnessListener readerBrightnessChangedListener = new BookReader.OnBrightnessListener() {
        @Override
        public void onBrightnessChanged(float newValue) {
            Window window = (ReaderActivity.this).getWindow();
            WindowManager.LayoutParams params = window.getAttributes();
            params.screenBrightness = newValue;
            window.setAttributes(params);
        }
    };

    /**
     * Listener for reaching end of book
     */
    private BookReader.OnTranscendCurrentBookListener readerTranscendCurrentBookListener = new BookReader.OnTranscendCurrentBookListener() {
        @Override
        public void onGoToPreviousBook() {
            database.openRead();
            final Book previousBook = database.selectNeighbouringBook(displayedBook, false);
            database.close();
            if (previousBook != null) {
                new AlertDialog.Builder(ReaderActivity.this)
                        .setTitle(R.string.reader_previous_book_title)
                        .setMessage(String.format(getString(R.string.reader_previous_book_prompt), previousBook.getName()))
                        .setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                showSpecificBook(previousBook.getId(), new Position(-1));
                            }
                        })
                        .setNegativeButton(R.string.dialog_no, null).show();
            }
        }

        @Override
        public void onGoToNextBook() {
            database.openRead();
            final Book nextBook = database.selectNeighbouringBook(displayedBook, true);
            database.close();
            if (nextBook != null) {
                new AlertDialog.Builder(ReaderActivity.this)
                        .setTitle(R.string.reader_next_book_title)
                        .setMessage(String.format(getString(R.string.reader_next_book_prompt), nextBook.getName()))
                        .setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                showSpecificBook(nextBook.getId(), new Position(-1));
                            }
                        })
                        .setNegativeButton(R.string.dialog_no, null).show();
            }
        }
    };

    //=========================== ASYNCHRONOUS TASKS ===========================

    /**
     * Class which is used for Internet synchronization
     * @author Semeniuk A.D.
     */
    private class UpdatesCheckingTask extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                List<Section> remoteSections = ServerConnector.getAvailableSections();
                database.openRead();
                int newBooks = database.getNewModifiedBookCount(remoteSections, getSharedPreferences(getPackageName(), MODE_PRIVATE));
                database.close();
                if (newBooks == 0) { //No new books found
                    return true;
                }
                publishProgress(newBooks);
                try {
                    Thread.sleep(10000);
                } catch (Exception ex) {
                    //Do nothing
                }
                publishProgress(-2);
                return true;
            } catch (Exception ex) { //Error happened
                return false;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progress = values[0];
            if (progress > 0) { //Show reader Update window
                updatesChecked = true;
                String message;
                if (progress % 10 == 1 && progress % 100 != 11) { //1, 21, 31, 101, 121... etc
                    message = String.format(getString(R.string.reader_update_found1), progress);
                } else if (progress % 10 >= 2 && progress % 10 <= 4 && !(progress % 100 >= 12 && progress % 100 <= 14)) { //2, 3, 4, 22, 23, 24 ... etc
                    message = String.format(getString(R.string.reader_update_found24), progress);
                } else {//Everything other
                    message = String.format(getString(R.string.reader_update_found00), progress);
                }
                readerUpdateText.setText(message);
                readerUpdate.setVisibility(View.VISIBLE);
                readerUpdate.startAnimation(readerUpdateShowAnimation);
            } else if (progress == -2 && readerUpdate.getVisibility() == View.VISIBLE) { //Hide reader Update window
                readerUpdate.startAnimation(readerUpdateHideAnimation);
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!result) { //Error happened
                Toast.makeText(ReaderActivity.this, R.string.reader_error_sync, Toast.LENGTH_SHORT).show();
            }
        }
    }

    //==========================================================================

    /**
     * Codes used to report BookLoaderTask result
     */
    private enum LoaderResultCode {
        SUCCESS, CLEAR, SD_ERROR, NOT_FOUND
    }

    /**
     * Result used by BookLoaderTask
     */
    private class LoaderResult {
        private LoaderResultCode code;
        private Book book;
        private List<String>[] text;

        LoaderResultCode getCode() {
            return code;
        }

        void setCode(LoaderResultCode code) {
            this.code = code;
        }

        public Book getBook() {
            return book;
        }

        public void setBook(Book book) {
            this.book = book;
        }

        public List<String>[] getText() {
            return text;
        }

        public void setText(List<String>[] text) {
            this.text = text;
        }
    }

    /**
     * Task which is used for book loading
     * @author Semeniuk A.D.
     */
    private class BookLoaderTask extends AsyncTask<Void, Void, LoaderResult> {
        private int bookId = -1;
        private Position position = null;

        BookLoaderTask(int bookId, Position position) {
            this.bookId = bookId;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            readerLoader.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onCancelled() {
            readerLoader.setVisibility(View.GONE);
        }

        @SuppressWarnings("unchecked")
        @Override
        protected LoaderResult doInBackground(Void... params) {
            LoaderResult result = new LoaderResult();
            if (bookId < 0) { //No data to display
                result.setCode(LoaderResultCode.CLEAR);
                return result;
            }
            try {
                database.openRead();
                Book book = database.selectBookWithChapters(bookId);
                result.setBook(book);
                if (position == null) {
                    position = database.selectBookPosition(bookId);
                }
                database.close();
                if (book == null) {
                    result.setCode(LoaderResultCode.NOT_FOUND);
                    return result;
                }
                result.setText(FileOperations.loadBookContent(new File(storagePath, book.getSectionPath()), book, onCancelListener));
            } catch (Exception ex) {
                result.setCode(LoaderResultCode.SD_ERROR);
                return result;
            }
            while (readerReader.getHeight() <= 0) { //Just to make sure
                try {
                    Thread.sleep(100);
                } catch (Exception ex) {
                    //Do nothing
                }
            }
            result.setCode(LoaderResultCode.SUCCESS);
            return result;
        }

        @Override
        protected void onPostExecute(LoaderResult result) {
            readerLoader.setVisibility(View.GONE);
            displayedBook = result.getBook();
            displayedBookText = result.getText();
            switch (result.getCode()) {
                case SUCCESS:
                    readerEmpty.setVisibility(View.GONE);
                    readerReader.setVisibility(View.VISIBLE);
                    closeOptionsMenu();
                    ((HistoryAdapter) historyList.getAdapter()).addHistoryItem(new HistoryItem(displayedBook.getId(), displayedBook.getName()));
                    ((HistoryAdapter) historyList.getAdapter()).trimToSize(optionsMain.historySize);
                    historyList.setSelectionFromTop(0, 0);
                    readerReader.setData(displayedBook, displayedBookText, new Position(position), optionsMain);
                    break;
                case CLEAR: //
                    showEmptyBook();
                    break;
                case SD_ERROR: //Book was not found on SD card
                    new AlertDialog.Builder(ReaderActivity.this).setTitle(R.string.dialog_error).setMessage(R.string.reader_error_loadbook)
                            .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        database.openWrite();
                                        database.updateBook(displayedBook.getId(), false);
                                        database.close();
                                    } catch (Exception ex) {
                                        //Do nothing
                                    }
                                    showEmptyBook();
                                }
                            }).setCancelable(false).show();
                    break;
                case NOT_FOUND: //No book with such id exists
                    Toast.makeText(ReaderActivity.this, R.string.reader_error_restorefailed, Toast.LENGTH_SHORT).show();
                    showEmptyBook();
                    break;
            }
        }

        private FileOperations.OnLoadingCanceledListener onCancelListener = new FileOperations.OnLoadingCanceledListener() {
            @Override
            public boolean isCanceled() {
                return BookLoaderTask.this.isCancelled();
            }
        };

    }
}