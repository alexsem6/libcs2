package org.alexsem.libcs.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class UpdateAlarmReceiver extends BroadcastReceiver {

	public static final String ACTION_CHECK_UPDATES_ALARM = "org.alexsem.libcs.ACTION_CHECK_UPDATES_ALARM";

	@Override
	public void onReceive(Context context, Intent intent) {
		context.startService(new Intent(context, UpdateService.class));
	}

}
