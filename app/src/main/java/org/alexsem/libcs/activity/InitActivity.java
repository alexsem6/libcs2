package org.alexsem.libcs.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import org.alexsem.libcs.transfer.DatabaseAdapter;
import org.alexsem.libcs.transfer.FileOperations;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InitActivity extends Activity {

  private File rootPath;
  private File currentPath;
  private DatabaseAdapter database;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // ------ Check whether SD card is available ------
    if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) { //SD card is not present
      new AlertDialog.Builder(this).setTitle(R.string.dialog_error).setIcon(android.R.drawable.ic_dialog_alert).setMessage(R.string.init_error_nosd)
          .setPositiveButton(R.string.dialog_exit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              InitActivity.this.finish();
            }
          }).setCancelable(false).show();
      return;
    }
    // ------------------------------------------------

    this.rootPath = Environment.getExternalStorageDirectory();

    // ------ Prepare path selection dialog ------
    final View pathContent = getLayoutInflater().inflate(R.layout.init_dialog_path, null);
    final RadioButton rabPathSystem = (RadioButton) pathContent.findViewById(R.id.init_path_system_radio);
    final RadioButton rabPathCustom = (RadioButton) pathContent.findViewById(R.id.init_path_custom_radio);
    final LinearLayout llPathSystem = (LinearLayout) pathContent.findViewById(R.id.init_path_system);
    final LinearLayout llPathCustom = (LinearLayout) pathContent.findViewById(R.id.init_path_custom);
    final EditText edtPathBrowse = (EditText) pathContent.findViewById(R.id.init_path_browse_result);
    final Button btnPathBrowse = (Button) pathContent.findViewById(R.id.init_path_browse);
    DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        switch (which) {
          case DialogInterface.BUTTON_POSITIVE: //Accept
            if (rabPathSystem.isChecked()) { //System folder
              currentPath = getFilesDir();
            } else { //CustomFolder
              currentPath = new File(edtPathBrowse.getText().toString());
            }
            generateDatabase();
            break;
          case DialogInterface.BUTTON_NEGATIVE: //Exit
            InitActivity.this.finish();
            break;
        }
      }
    };
    final AlertDialog pathDialog = new AlertDialog.Builder(this).setTitle(R.string.init_path_title).setView(pathContent)
        .setPositiveButton(getString(R.string.dialog_accept), ocl).setNegativeButton(getString(R.string.dialog_exit), ocl).setCancelable(false)
        .create();
    View.OnClickListener rabPathSystemOCL = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        rabPathSystem.setChecked(true);
        rabPathCustom.setChecked(false);
        edtPathBrowse.setEnabled(false);
        btnPathBrowse.setEnabled(false);
        pathDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
      }
    };
    rabPathSystem.setOnClickListener(rabPathSystemOCL);
    llPathSystem.setOnClickListener(rabPathSystemOCL);
    View.OnClickListener rabPathCustomOCL = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        rabPathCustom.setChecked(true);
        rabPathSystem.setChecked(false);
        edtPathBrowse.setEnabled(true);
        btnPathBrowse.setEnabled(true);
        pathDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(edtPathBrowse.getText().length() > 0);
      }
    };
    rabPathCustom.setOnClickListener(rabPathCustomOCL);
    llPathCustom.setOnClickListener(rabPathCustomOCL);
    // -------------------------------------------

    // ------- Browsing through SD directories ------
    btnPathBrowse.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            edtPathBrowse.setText(currentPath.getAbsolutePath());
            pathDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
          }
        };
        final View browseContent = getLayoutInflater().inflate(R.layout.init_dialog_browse, null);
        final ListView lstBrowse = (ListView) browseContent.findViewById(R.id.init_browse_list);
        final AlertDialog browseDialog = new AlertDialog.Builder(InitActivity.this).setView(browseContent)
            .setPositiveButton(getString(R.string.dialog_accept), ocl).setNeutralButton(R.string.dialog_create, null)
            .setNegativeButton(getString(R.string.dialog_cancel), null).create();
        lstBrowse.setAdapter(generateAdapter(rootPath));
        browseDialog.setTitle(currentPath.getAbsolutePath());
        lstBrowse.setOnItemClickListener(new OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            if (position == 0 && l.getItemAtPosition(position).equals("/")) { //Root directory chosen
              lstBrowse.setAdapter(generateAdapter(new File("/")));
              browseDialog.setTitle(currentPath.getAbsolutePath());
              browseDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(currentPath.canWrite());
            } else if (position == 1 && l.getItemAtPosition(position).equals("..")) { //Parent directory chosen
              lstBrowse.setAdapter(generateAdapter(currentPath.getParentFile()));
              browseDialog.setTitle(currentPath.getAbsolutePath());
              browseDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(currentPath.canWrite());
            } else {
              File file = new File(currentPath, (String) l.getItemAtPosition(position));
              if (file.canRead()) { //Directory is readable
                lstBrowse.setAdapter(generateAdapter(file));
                browseDialog.setTitle(currentPath.getAbsolutePath());
                browseDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(currentPath.canWrite());
              } else { //Directory can not be read
                Toast.makeText(InitActivity.this, R.string.init_error_access, Toast.LENGTH_SHORT).show();
              }
            }
          }
        });
        browseDialog.show();
        browseDialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            View content = getLayoutInflater().inflate(R.layout.init_dialog_browse_create, null);
            final EditText input = (EditText) content.findViewById(R.id.init_create_edit);
            new AlertDialog.Builder(InitActivity.this).setTitle(R.string.init_create_title).setView(content)
                .setPositiveButton(R.string.dialog_create, new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialog, int whichButton) {
                    File newDir = new File(currentPath, input.getText().toString());
                    if (newDir.mkdir()) { //Directory was created
                      lstBrowse.setAdapter(generateAdapter(newDir));
                      browseDialog.setTitle(newDir.getAbsolutePath());
                    } else { //Error occurred
                      Toast.makeText(InitActivity.this, R.string.init_error_create, Toast.LENGTH_LONG).show();
                    }
                  }
                }).setNegativeButton(R.string.dialog_cancel, null).show();
            browseDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(currentPath.canWrite());
          }
        });
      }
    });
    // ----------------------------------------------

    pathDialog.show();
  }

  /**
   * Generates ArrayAdapter based on contents of the specified directory
   * @param path Directory in question
   * @return ArryAdapter around list of files
   */
  private ArrayAdapter<String> generateAdapter(File path) {
    if (path != null  && path.isDirectory()) {
      this.currentPath = path;
    }
    File[] files = currentPath.listFiles();
    List<String> result = new ArrayList<String>();
    for (File file : files) {
      if (file.isDirectory() && !file.getName().startsWith(".")) {
        result.add(file.getName());
      }
    }
    Collections.sort(result);
    if (path.getParent() != null) {
      result.add(0, "/");
      result.add(1, "..");
    }
    return new ArrayAdapter<String>(this, R.layout.init_dialog_browse_item, result);
  }

  /**
   * Tries to establish connection to the local database
   * Generates DB structure in process
   */
  private void generateDatabase() {
    database = new DatabaseAdapter(this);
    try {
      database.openWrite();
      database.close();
      importData();
    } catch (SQLException ex) {
      new AlertDialog.Builder(InitActivity.this).setTitle(R.string.dialog_error).setMessage(R.string.init_error_dbconnect)
          .setIcon(android.R.drawable.ic_dialog_alert).setPositiveButton(R.string.dialog_exit, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          InitActivity.this.finish();
        }
      }).setCancelable(false).show();
    }
  }

  /**
   * Imports books from SD storage to local database
   */
  private void importData() {
    if (currentPath == null) { //Bugfix
      currentPath = getFilesDir();
    }
    if (currentPath.list().length > 0) { //Files present in the path
      DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
              new BookImporterTask().execute();
              break;
            case DialogInterface.BUTTON_NEGATIVE:
              complete();
              break;
          }
        }
      };
      new AlertDialog.Builder(InitActivity.this).setTitle(R.string.init_import_title).setMessage(R.string.init_import_prompt)
          .setPositiveButton(R.string.dialog_yes, ocl).setNegativeButton(R.string.dialog_no, ocl).setCancelable(false).show();
    } else {
      complete();
    }
  }

  /**
   * Successfully finalizes the import process
   */
  private void complete() {
    if (currentPath == null) { //Precaution
      currentPath = Environment.getExternalStorageDirectory();
    }
    getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().putString("SDpath", currentPath.getAbsolutePath()).commit();
    InitActivity.this.finish();
    startActivity(new Intent(this, ReaderActivity.class));
  }

  /**
   * Class which is used for asynchronous book importing
   * @author Semeniuk A.D.
   */
  private class BookImporterTask extends AsyncTask<Void, Void, Integer> {
    private ProgressDialog dialog;

    @Override
    protected void onPreExecute() {
      dialog = new ProgressDialog(InitActivity.this);
      dialog.setCancelable(true);
      dialog.setMessage(getString(R.string.init_importing));
      dialog.setOnCancelListener(new OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          BookImporterTask.this.cancel(true);
        }
      });
      dialog.show();
    }

    @Override
    protected Integer doInBackground(Void... arg0) {
      try {
        database.openWrite();
        int booksImported = FileOperations.tryImport(currentPath, database);
        database.close();
        return booksImported;
      } catch (Exception ex) {
        return -1;
      }
    }

    @Override
    protected void onPostExecute(Integer result) {
      dialog.dismiss();
      DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          complete();
        }
      };
      if (result == -1) { //Exception happened
        new AlertDialog.Builder(InitActivity.this).setTitle(R.string.dialog_error).setMessage(R.string.init_error_dbconnect)
            .setIcon(android.R.drawable.ic_dialog_alert).setPositiveButton(R.string.dialog_ok, ocl).setCancelable(false).show();
      } else { //Import succeeded
        String message;
        if (result == 0) { //Nothing
          message = getString(R.string.init_import_result_nothing);
        } else if (result % 10 == 1 && result % 100 != 11) { //1, 21, 31, 101, 121... etc
          message = String.format(getString(R.string.init_import_result_success1), result);
        } else if (result % 10 >= 2 && result % 10 <= 4 && !(result % 100 >= 12 && result % 100 <= 14)) { //2, 3, 4, 22, 23, 24 ... etc
          message = String.format(getString(R.string.init_import_result_success24), result);
        } else {//Everything other
          message = String.format(getString(R.string.init_import_result_success00), result);
        }
        new AlertDialog.Builder(InitActivity.this).setTitle(R.string.init_import_completed).setMessage(message)
            .setIcon(android.R.drawable.ic_dialog_info).setPositiveButton(R.string.dialog_ok, ocl).setCancelable(false).show();
      }
    }

    @Override
    protected void onCancelled() {
      Toast.makeText(InitActivity.this, R.string.dialog_canceled, Toast.LENGTH_SHORT).show();
      complete();
    }
  }
}