package org.alexsem.libcs.activity;

import android.app.IntentService;
import android.content.Intent;

import org.alexsem.libcs.model.Report;
import org.alexsem.libcs.transfer.DatabaseAdapter;
import org.alexsem.libcs.transfer.ServerConnector;

import java.io.IOException;
import java.util.List;


/**
 * Service used for sending reports to the server
 */
public class SendReportService extends IntentService {

    public SendReportService() {
        super("SendReportService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            DatabaseAdapter database = new DatabaseAdapter(this);
            database.openRead();
            List<Report> reports = database.selectReports();
            database.close();
            for (int i = 0; i < reports.size(); i++) {
                Report report = reports.get(i);
                try {
                    ServerConnector.saveReport(report);
                    database.openWrite();
                    database.deleteReport(report.getId());
                    database.close();
                } catch (IOException ioex) { //Server interaction problems
                    //Try next report
                }
            }
        } catch (Exception ex) { //Error happened
            //Do nothing
        }
    }
}
