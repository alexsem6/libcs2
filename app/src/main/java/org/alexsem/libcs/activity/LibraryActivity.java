package org.alexsem.libcs.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.alexsem.libcs.widget.ActionToolbar;
import org.alexsem.libcs.widget.DoubleViewFlipper;
import org.alexsem.libcs.adapter.LibraryBookAdapter;
import org.alexsem.libcs.adapter.LibraryIgnoredAdapter;
import org.alexsem.libcs.model.Book;
import org.alexsem.libcs.model.Chapter;
import org.alexsem.libcs.model.Item;
import org.alexsem.libcs.model.Section;
import org.alexsem.libcs.transfer.DatabaseAdapter;
import org.alexsem.libcs.transfer.FileOperations;
import org.alexsem.libcs.transfer.ServerConnector;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LibraryActivity extends Activity {

    public static final String ACTION_LIBRARY_UPDATED = "org.alexsem.libcs.ACTION_LIBRARY_UPDATED";
    public static final int UPDATE_CHECK_NOTIFICATION_ID = 1337;
    private final int TAG_OLD = 0; //Item present in both DBs, (can be disabled and deleted if book)
    private final int TAG_NEW = -1; //Item present in remote DB only (can be installed if book)
    private final int TAG_UPD = -2; //Item present in both DBs but differs (can be updated, deleted, disabled if book)
    private final int TAG_DEL = -3; //Item present in local DB only (can be disabled and deleted if book)
    private final int TAG_IGN = -4; //Item present in remote DB only and is ignored (can be installed if book)

    private int TOAST_OFFSET_TOP;
    private int TOAST_OFFSET_FIRST;
    private int TOAST_OFFSET_SECOND;
    private int TOAST_OFFSET_THIRD;
    private int COLOR_CINNABAR;
    private String TITLE_MAIN;
    private String TITLE_IGNORED;

    private TextView mActionbarTitle;
    private View mActionbarItems;
    private ImageButton mActionShow;
    private ImageButton mActionSync;
    private ImageButton mActionInsupd;
    private ImageButton mActionIgnored;
    private Toast mActionToast;

    private DoubleViewFlipper mFlipper;

    private HorizontalScrollView hsvSections;
    private LinearLayout llSections;
    private ActionToolbar atbToolbar;
    private View atbRead;
    private View atbInstall;
    private View atbIgnore;
    private View atbUpdate;
    private View atbRemove;
    private View atbDisable;
    private View atbEnable;
    private ListView lstBooks;
    private View bookInfo;
    private ListView lstIgnored;

    private boolean isShowDisabled = false;
    private boolean isSynced = false;

    private OnTouchListener closeActionToolBarListener;

    private List<Section> localSections;
    private List<Section> mergedSections;

    private LibraryIgnoredAdapter miaIgnored;

    private int currentSectionIndex = -1;
    private int startingSectionId = -1;
    private int startingBookId = -1;

    private DatabaseAdapter database = null;
    private File sdPath = null;

    private boolean libraryUpdated = false;
    private boolean ignoredFound = false;
    private boolean readingEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean day = getSharedPreferences(getPackageName(), MODE_PRIVATE).getBoolean("daytime", true);
        setTheme(day ? R.style.AppTheme_Light : R.style.AppTheme_Dark);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.library);

        database = new DatabaseAdapter(this);
        sdPath = new File(getSharedPreferences(getPackageName(), MODE_PRIVATE).getString("SDpath", "."));
        COLOR_CINNABAR = getResources().getColor(R.color.cinnabar);

        closeActionToolBarListener = new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (atbToolbar.isOpened()) {
                    atbToolbar.hide();
                    return true;
                }
                return false;
            }
        };

        //---- Common ActionBar items ----
        findViewById(R.id.library_actionbar_home).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mActionbarTitle = (TextView) findViewById(R.id.library_actionbar_title);
        TITLE_MAIN = getString(R.string.library_title);
        TITLE_IGNORED = getString(R.string.library_action_ignored);
        mActionbarItems = findViewById(R.id.library_actionbar_items);
        mActionToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        TOAST_OFFSET_TOP = Math.round(getResources().getDimension(R.dimen.library_action_toast_top));
        TOAST_OFFSET_FIRST = Math.round(getResources().getDimension(R.dimen.library_action_toast_first));
        TOAST_OFFSET_SECOND = Math.round(getResources().getDimension(R.dimen.library_action_toast_second));
        TOAST_OFFSET_THIRD = Math.round(getResources().getDimension(R.dimen.library_action_toast_third));

        //---- View flipper ----
        mFlipper = (DoubleViewFlipper) findViewById(R.id.library_flipper);
        mFlipper.setNextNewAnimation(AnimationUtils.loadAnimation(this, R.anim.fly_from_right));
        mFlipper.setNextOldAnimation(AnimationUtils.loadAnimation(this, R.anim.fly_to_left));
        mFlipper.setPrevNewAnimation(AnimationUtils.loadAnimation(this, R.anim.fly_from_left));
        mFlipper.setPrevOldAnimation(AnimationUtils.loadAnimation(this, R.anim.fly_to_right));

        hsvSections = (HorizontalScrollView) findViewById(R.id.library_scroll);
        llSections = (LinearLayout) findViewById(R.id.library_sections);
        llSections.setOnTouchListener(closeActionToolBarListener);

        atbToolbar = (ActionToolbar) findViewById(R.id.library_toolbar);
        atbToolbar.setHotPoint(getResources().getDimension(R.dimen.library_toolbar_hotpoint), 0f);
        atbToolbar.requestHide();
        atbRead = findViewById(R.id.library_toolbar_item_read);
        atbInstall = findViewById(R.id.library_toolbar_item_install);
        atbIgnore = findViewById(R.id.library_toolbar_item_ignore);
        atbUpdate = findViewById(R.id.library_toolbar_item_update);
        atbRemove = findViewById(R.id.library_toolbar_item_remove);
        atbDisable = findViewById(R.id.library_toolbar_item_disable);
        atbEnable = findViewById(R.id.library_toolbar_item_enable);

        lstBooks = (ListView) findViewById(R.id.library_books);
        lstBooks.setOnTouchListener(closeActionToolBarListener);
        lstBooks.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RelativeLayout.LayoutParams params = ((RelativeLayout.LayoutParams) atbToolbar.getLayoutParams());
                if (view.getTop() + view.getHeight() / 2 < parent.getHeight() / 2) { //At the top
                    atbToolbar.setOrientation(ActionToolbar.Orientation.TOP_LEFT);
                    params.topMargin = view.getTop() + view.getHeight();
                } else { //At the bottom
                    atbToolbar.setOrientation(ActionToolbar.Orientation.BOTTOM_LEFT);
                    params.topMargin = view.getTop() - atbToolbar.getHeight();
                }
                atbToolbar.setLayoutParams(params);
                changeToolbarButtonsVisibility((Book) lstBooks.getItemAtPosition(position));
                atbToolbar.setTag(position);
                atbToolbar.show();
            }
        });
        lstBooks.setEmptyView(findViewById(R.id.library_list_empty));

        //---- "Install/update all" action item ----
        mActionInsupd = (ImageButton) findViewById(R.id.library_action_insupd);
        mActionInsupd.setOnTouchListener(closeActionToolBarListener);
        mActionInsupd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSynced) {
                    String message;
                    int result = calculateInsUpdBookCount();
                    if (result == 0) { //Nothing
                        return;
                    } else if (result % 10 == 1 && result % 100 != 11) { //1, 21, 31, 101, 121... etc
                        message = String.format(getString(R.string.library_insupdall_prompt1), result);
                    } else if (result % 10 >= 2 && result % 10 <= 4 && !(result % 100 >= 12 && result % 100 <= 14)) { //2, 3, 4, 22, 23, 24 ... etc
                        message = String.format(getString(R.string.library_insupdall_prompt24), result);
                    } else {//Everything other
                        message = String.format(getString(R.string.library_insupdall_prompt00), result);
                    }
                    new AlertDialog.Builder(LibraryActivity.this).setTitle(R.string.library_insupdall).setMessage(message)
                            .setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new InstallUpdateAllTask().execute((Void[]) null);
                                }
                            }).setNegativeButton(R.string.dialog_no, null).show();
                }
            }
        });
        mActionInsupd.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mActionToast.setGravity(Gravity.TOP | Gravity.RIGHT, TOAST_OFFSET_SECOND, TOAST_OFFSET_TOP);
                mActionToast.setText(R.string.library_action_insupd);
                mActionToast.show();
                return true;
            }
        });

        //---- "Show disabled" action item ----
        isShowDisabled = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).getBoolean("showDisabledBooks", true);
        mActionShow = (ImageButton) findViewById(R.id.library_action_show);
        mActionShow.getDrawable().setLevel(isShowDisabled ? 1 : 0);
        mActionShow.setOnTouchListener(closeActionToolBarListener);
        mActionShow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                isShowDisabled = !isShowDisabled;
                mActionShow.getDrawable().setLevel(isShowDisabled ? 1 : 0);
                if (lstBooks != null && lstBooks.getAdapter() != null) {
                    ((LibraryBookAdapter) lstBooks.getAdapter()).setShowDisabledBooks(isShowDisabled);
                    getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit().putBoolean("showDisabledBooks", isShowDisabled).commit();
                }
            }
        });
        mActionShow.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mActionToast.setGravity(Gravity.TOP | Gravity.RIGHT, TOAST_OFFSET_FIRST, TOAST_OFFSET_TOP);
                mActionToast.setText(R.string.library_action_show);
                mActionToast.show();
                return true;
            }
        });

        //---- "Synchronize" action item ----
        mActionSync = (ImageButton) findViewById(R.id.library_action_sync);
        mActionSync.setOnTouchListener(closeActionToolBarListener);
        mActionSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SynchronizationTask().execute();
            }
        });
        mActionSync.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mActionToast.setGravity(Gravity.TOP | Gravity.RIGHT, TOAST_OFFSET_SECOND, TOAST_OFFSET_TOP);
                mActionToast.setText(R.string.library_action_sync);
                mActionToast.show();
                return true;
            }
        });

        //---- "Ignored" action item ----
        mActionIgnored = (ImageButton) findViewById(R.id.library_action_ignored);
        mActionIgnored.setOnTouchListener(closeActionToolBarListener);
        mActionIgnored.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mergedSections != null) {
                    miaIgnored.clear();
                    for (Section section : mergedSections) {
                        for (Book book : section.getBooks()) {
                            if (book.getTag() == TAG_IGN) {//Book is ignored
                                miaIgnored.put(section.getName(), book);
                            }
                        }
                    }
                    lstIgnored.setAdapter(miaIgnored);
                    showFlipperPage(1);
                } else {
                    mActionIgnored.setVisibility(View.GONE);
                }
            }
        });
        mActionIgnored.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mActionToast.setGravity(Gravity.TOP | Gravity.RIGHT, TOAST_OFFSET_THIRD, TOAST_OFFSET_TOP);
                mActionToast.setText(R.string.library_action_ignored);
                mActionToast.show();
                return true;
            }
        });

        bookInfo = getLayoutInflater().inflate(R.layout.library_dialog_info, null);

        miaIgnored = new LibraryIgnoredAdapter(this);
        lstIgnored = (ListView) findViewById(R.id.library_ignored_list);
        lstIgnored.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Book book = (Book) miaIgnored.getItem(position);
                book.setTag(TAG_NEW);
                miaIgnored.remove(book);
                miaIgnored.notifyDataSetChanged();
                getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().remove(String.format("ign%d", book.getId())).commit();
                if (isSynced) {
                    for (int i = 0; i < mergedSections.size(); i++) {
                        if (mergedSections.get(i).getBooks().contains(book)) { //Found section to where book belongs
                            //------ Change current section tag ------
                            ToggleButton cs = (ToggleButton) llSections.getChildAt(i);
                            Integer tag = (Integer) cs.getTag();
                            tag = tag == null ? 1 : tag + 1;
                            setButtonTag(cs, mergedSections.get(i).getName(), tag);
                            if (currentSectionIndex < 0) { //No section selected
                                cs.setChecked(true);
                            }
                            break;
                        }
                    }
                    if (lstBooks.getAdapter() != null) {
                        ((LibraryBookAdapter) lstBooks.getAdapter()).notifyDataSetChanged();
                    }
                }
                if (miaIgnored.getCount() <= 0) { //No ignored items left
                    mActionIgnored.setVisibility(View.GONE);
                    showFlipperPage(0);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).cancel(LibraryActivity.UPDATE_CHECK_NOTIFICATION_ID);
        readingEnabled = (getIntent().getExtras() != null && getIntent().getExtras().containsKey("read"));
        startingBookId = (getIntent().getIntExtra("bookID", -1));
        if (localSections == null) { //No data yet fetched (first run)
            new StartupTask().execute();
        }
    }

    /**
     * Fills respective controls with local database data
     * @param sectionId Section which should be selected or -1 if does not matter
     */
    private void populateLocalData(int sectionId) {
        llSections.removeAllViews();
        LayoutInflater inflater = getLayoutInflater();
        for (Section section : localSections) {
            ToggleButton button = (ToggleButton) inflater.inflate(R.layout.library_tab_section, llSections, false);
            setButtonTag(button, section.getName(), 0);
            button.setOnCheckedChangeListener(localSectionCheckedChangeListener);
            button.setOnTouchListener(closeActionToolBarListener);
            llSections.addView(button);
        }

        // --- Try to select section ---
        currentSectionIndex = -1;
        if (sectionId > -1) {//Need to select specific section
            for (int i = 0; i < localSections.size(); i++) {
                if (localSections.get(i).getId() == sectionId) {
                    ((ToggleButton) llSections.getChildAt(i)).setChecked(true);
                    break;
                }
            }
        }
        if (currentSectionIndex == -1) { //No need to select specific section or no section was found
            if (llSections.getChildCount() > 0) {
                ((ToggleButton) llSections.getChildAt(0)).setChecked(true);
            } else {
                lstBooks.setAdapter(null);
            }
        }
        // -----------------------------

    }

    /**
     * Merges localSections with specified list of sections
     * Stores result in mergedSections (sorted by order)
     */
    private void generateMergedSections(List<Section> remoteSections) {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        mergedSections = new ArrayList<Section>();
        mergedSections.addAll(localSections);
        ignoredFound = false;
        for (Section sm : mergedSections) {
            sm.setTag(TAG_OLD);
            for (Book bm : sm.getBooks()) {
                bm.setTag(TAG_DEL);
            }
        }
        for (Section sr : remoteSections) {
            boolean newSection = true;
            for (Section sl : localSections) {
                if (sr.getId() == sl.getId()) { //Remote section is already present in local database
                    newSection = false;
                    for (Book br : sr.getBooks()) {
                        boolean newBook = true;
                        for (Book bl : sl.getBooks()) {
                            if (br.getId() == bl.getId()) { //Remote book is already present in local database
                                newBook = false;
                                bl.setKb(br.getKb());
                                if (br.getSize() != bl.getSize() || Math.abs(br.getOrder() - bl.getOrder()) > 0.0001f) { //Sizes are different (update available)
                                    bl.setTag(TAG_UPD);
                                    sl.setTag(sl.getTag() + 1); //Increase counter
                                    if (startingSectionId == -1) { //Set starting section
                                        startingSectionId = sl.getId();
                                        if (startingBookId == -1) {
                                            startingBookId = bl.getId();
                                        }
                                    }
                                } else { //Sizes are the same
                                    bl.setTag(TAG_OLD);
                                }
                                break;
                            }
                        }
                        if (newBook) {
                            if (prefs.contains(String.format("ign%d", br.getId()))) { //New book
                                br.setTag(TAG_IGN);
                                ignoredFound = true;
                            } else {
                                br.setTag(TAG_NEW);
                                sl.setTag(sl.getTag() + 1); //Increase counter
                                if (startingSectionId == -1) { //Set starting section
                                    startingSectionId = sl.getId();
                                    if (startingBookId == -1) {
                                        startingBookId = br.getId();
                                    }
                                }
                            }
                            br.setEnabled(true);
                            sl.getBooks().add(br);
                        }
                    }
                    break;
                }
            }
            if (newSection) { //Local section with this id is not present in remote section list
                sr.setTag(0);
                for (Book br : sr.getBooks()) {
                    if (prefs.contains(String.format("ign%d", br.getId()))) {
                        br.setTag(TAG_IGN); //Ignored book
                        ignoredFound = true;
                    } else {
                        br.setTag(TAG_NEW); //Completely new book
                        sr.setTag(sr.getTag() + 1); //Completely new section
                        if (startingSectionId == -1) { //Set starting section
                            startingSectionId = sr.getId();
                            if (startingBookId == -1) {
                                startingBookId = br.getId();
                            }
                        }
                    }
                    br.setEnabled(true);
                }
                mergedSections.add(sr);
            }
        }
        Collections.sort(mergedSections);
        for (Section sm : mergedSections) {
            if (Math.round(Math.floor(sm.getOrder())) == 3) { //Akathists
                Collections.sort(sm.getBooks(), akathistsComparator);
            } else {
                Collections.sort(sm.getBooks());
            }
        }
    }

    /**
     * Fills respective controls with both local and remote database data
     * @param sectionId Section which should be selected or -1 if does not matter
     */
    private void populateMergedData(int sectionId) {
        llSections.removeAllViews();
        LayoutInflater inflater = getLayoutInflater();
        for (Section section : mergedSections) { //For all merged sections
            ToggleButton button = (ToggleButton) inflater.inflate(R.layout.library_tab_section, llSections, false);
            //--- Define whether section is ignored ---
            boolean ignored = true;
            for (Book book : section.getBooks()) {
                if (book.getTag() != TAG_IGN) { //Found not ignored book
                    ignored = false;
                    break;
                }
            }
            if (ignored) { //Section is ignored
                button.setVisibility(View.GONE);
            }
            //---
            setButtonTag(button, section.getName(), section.getTag());
            button.setOnCheckedChangeListener(mergedSectionCheckedChangeListener);
            button.setOnTouchListener(closeActionToolBarListener);
            llSections.addView(button);
        }

        // --- Try to select section ---
        currentSectionIndex = -1;
        if (sectionId == -1 && startingSectionId > -1) //First run (need to select section with changes)
        {
            sectionId = startingSectionId;
            startingSectionId = -1;
        }
        if (sectionId > -1) {//Need to select specific section
            for (int i = 0; i < mergedSections.size(); i++) {
                if (mergedSections.get(i).getId() == sectionId && llSections.getChildAt(i).getVisibility() == View.VISIBLE) {
                    ((ToggleButton) llSections.getChildAt(i)).setChecked(true);
                    break;
                }
            }
        }
        if (currentSectionIndex == -1) { //No need to select specific section or no section was found
            for (int i = 0; i < llSections.getChildCount(); i++) {
                if (llSections.getChildAt(i).getVisibility() == View.VISIBLE) { //Section is not ignored
                    ((ToggleButton) llSections.getChildAt(i)).setChecked(true);
                    break;
                }
            }
            if (currentSectionIndex == -1) { //No appropriate section found
                lstBooks.setAdapter(null);
            }
        }
        // -----------------------------
    }

    /**
     * Assign new/updated books tag (badge) to section title
     * @param button Button to which tag should be assigned
     * @param name   Section original name
     * @param tag    Number of new/updated books within section
     */
    private void setButtonTag(ToggleButton button, String name, int tag) {
        CharSequence newName;
        if (tag > 0) {
            SpannableString ss = new SpannableString(String.format("(%d) %s", tag, name));
            ss.setSpan(new ForegroundColorSpan(COLOR_CINNABAR), 0, String.valueOf(tag).length() + 3, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
            newName = ss;
        } else {
            newName = name;
        }
        button.setTag(tag);
        button.setText(newName);
        button.setTextOn(newName);
        button.setTextOff(newName);
    }

    /**
     * Shows appropriate flipper page changing layout when needed
     * @param page Page to display (0 - original, 1 - ignored list)
     */
    private void showFlipperPage(int page) {
        if (page == 0) { //Original
            mFlipper.showPrevious();
            mActionbarTitle.setText(TITLE_MAIN);
            mActionbarItems.setVisibility(View.VISIBLE);
        } else { //Ignored list
            mFlipper.showNext();
            mActionbarTitle.setText(TITLE_IGNORED);
            mActionbarItems.setVisibility(View.GONE);
        }
    }

    /**
     * Defines which buttons should be visible in ActionToolbar
     * and which should not
     * @param book Book object to perform checks for
     */
    private void changeToolbarButtonsVisibility(Book book) {
        atbDisable.setVisibility(book.isEnabled() ? View.VISIBLE : View.GONE);
        atbEnable.setVisibility(!book.isEnabled() ? View.VISIBLE : View.GONE);
        switch (book.getTag()) {
            case TAG_OLD:
                atbRead.setVisibility(readingEnabled && book.isEnabled() ? View.VISIBLE : View.GONE);
                atbInstall.setVisibility(View.GONE);
                atbIgnore.setVisibility(View.GONE);
                atbUpdate.setVisibility(View.GONE);
                atbRemove.setVisibility(View.VISIBLE);
                break;
            case TAG_NEW:
                atbRead.setVisibility(View.GONE);
                atbInstall.setVisibility(View.VISIBLE);
                atbIgnore.setVisibility(View.VISIBLE);
                atbUpdate.setVisibility(View.GONE);
                atbRemove.setVisibility(View.GONE);
                atbDisable.setVisibility(View.GONE);
                atbEnable.setVisibility(View.GONE);
                break;
            case TAG_UPD:
                atbRead.setVisibility(readingEnabled && book.isEnabled() ? View.VISIBLE : View.GONE);
                atbInstall.setVisibility(View.GONE);
                atbIgnore.setVisibility(View.GONE);
                atbUpdate.setVisibility(View.VISIBLE);
                atbRemove.setVisibility(View.VISIBLE);
                break;
            case TAG_IGN:
                atbRead.setVisibility(View.GONE);
                atbInstall.setVisibility(View.VISIBLE);
                atbIgnore.setVisibility(View.GONE);
                atbUpdate.setVisibility(View.GONE);
                atbRemove.setVisibility(View.GONE);
                atbDisable.setVisibility(View.GONE);
                atbEnable.setVisibility(View.GONE);
                break;
            case TAG_DEL:
                atbRead.setVisibility(readingEnabled && book.isEnabled() ? View.VISIBLE : View.GONE);
                atbInstall.setVisibility(View.GONE);
                atbIgnore.setVisibility(View.GONE);
                atbUpdate.setVisibility(View.GONE);
                atbRemove.setVisibility(View.VISIBLE);
                break;
        }
    }

    /**
     * Get number of books which are needed to be installed/updated
     * @return book count
     */
    private int calculateInsUpdBookCount() {
        if (mergedSections == null) {
            return 0;
        }
        int total = 0;
        for (Section section : mergedSections) {
            for (Book book : section.getBooks()) {
                if (book.getTag() == TAG_NEW || book.getTag() == TAG_UPD) {
                    total++;
                }
            }
        }
        return total;
    }

    /**
     * Defines the state of "Install/Update all button"
     * (whether it should be enabled/disabled and visible/hidden)
     */
    private void defineInsUpdAllState() {
        if (isSynced) { //Working with merged data
            mActionInsupd.setEnabled(calculateInsUpdBookCount() > 0);
            mActionInsupd.setVisibility(View.VISIBLE);
        } else { //Working with local data
            mActionInsupd.setVisibility(View.GONE);
            mActionInsupd.setEnabled(false);
        }
    }

    /**
     * Click listener for ActionToolbar items clicks
     * @param v Button that was clicked
     */
    public void toolbarButtonClick(View v) {
        final Integer position = (Integer) atbToolbar.getTag();
        atbToolbar.hide();
        if (position == null) {
            return;
        }
        switch (v.getId()) {
            case R.id.library_toolbar_item_read:
                Intent resultIntent = new Intent();
                resultIntent.putExtra("bookId", ((Book) lstBooks.getItemAtPosition(position)).getId());
                libraryUpdated = false;
                setResult(RESULT_OK, resultIntent);
                this.finish();
                break;
            case R.id.library_toolbar_item_install:
                installBook(position);
                break;
            case R.id.library_toolbar_item_ignore:
                ignoreBook(position);
                break;
            case R.id.library_toolbar_item_update:
                updateBook(position);
                break;
            case R.id.library_toolbar_item_remove:
                String message2 = String.format(getString(R.string.library_remove_prompt), ((Book) lstBooks.getItemAtPosition(position)).getName());
                new AlertDialog.Builder(LibraryActivity.this).setTitle(R.string.library_remove).setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(message2).setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteBook(position);
                    }
                }).setNegativeButton(R.string.dialog_no, null).show();
                break;
            case R.id.library_toolbar_item_disable:
                disableBook(position);
                break;
            case R.id.library_toolbar_item_enable:
                enableBook(position);
                break;
            case R.id.library_toolbar_item_info:
                bookInfo(position);
                break;
        }
    }

    /**
     * Installs new book
     * @param position Position of book in list
     */
    private void installBook(int position) {
        Book book = (Book) lstBooks.getItemAtPosition(position);
        Section section = mergedSections.get(currentSectionIndex);
        try {
            FileOperations.deleteDir(new File(new File(sdPath, section.getPath()), book.getPath()));
        } catch (Exception ex) {
            String message = String.format(getString(R.string.library_error_bookinstall), book.getName());
            Toast.makeText(LibraryActivity.this, message, Toast.LENGTH_LONG).show();
            return;
        }
        new InstallBookTask().execute(section, book);
    }

    /**
     * Adds book to the ignore list
     * @param position Position of book in list
     */
    private void ignoreBook(int position) {
        Book book = (Book) lstBooks.getItemAtPosition(position);
        getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().putBoolean(String.format("ign%d", book.getId()), true).commit();

        this.libraryUpdated = true; //Contents changed
        //------ Change current section tag ------
        ToggleButton cs = (ToggleButton) llSections.getChildAt(currentSectionIndex);
        Integer tag = (Integer) cs.getTag();
        tag = tag == null ? 0 : tag - 1;
        setButtonTag(cs, mergedSections.get(currentSectionIndex).getName(), tag);
        //------
        book.setTag(TAG_IGN);
        ((LibraryBookAdapter) lstBooks.getAdapter()).notifyDataSetChanged();
        miaIgnored.put(mergedSections.get(currentSectionIndex).getName(), book);
        miaIgnored.notifyDataSetChanged();

        boolean ignored = true;
        for (Book b : mergedSections.get(currentSectionIndex).getBooks()) {
            if (b.getTag() != TAG_IGN) { //Found not ignored book
                ignored = false;
                break;
            }
        }
        if (ignored) { //Ignored section
            cs.setVisibility(View.GONE);
            currentSectionIndex = -1;
            cs.setChecked(false);
            for (int i = 0; i < llSections.getChildCount(); i++) {
                if (llSections.getChildAt(i).getVisibility() == View.VISIBLE) { //Section is not ignored
                    ((ToggleButton) llSections.getChildAt(i)).setChecked(true);
                    break;
                }
            }
            if (currentSectionIndex == -1) { //No appropriate section found
                lstBooks.setAdapter(null);
            }
        }
        defineInsUpdAllState();
        mActionIgnored.setVisibility(View.VISIBLE);
    }

    /**
     * Updates book
     * @param position Position of book in list
     */
    private void updateBook(int position) {
        deleteBook(position);
        Book book = (Book) lstBooks.getItemAtPosition(position);
        Section section = mergedSections.get(currentSectionIndex);
        new InstallBookTask().execute(section, book);
    }

    /**
     * Deletes book from the list (and DB if needed)
     * @param position Position of book in list
     * @return true if book was deleted, false otherwise
     */
    private boolean deleteBook(int position) {
        Book book = (Book) lstBooks.getItemAtPosition(position);

        //------ Remove book from DB and SD card ------
        try {
            Section section = isSynced ? mergedSections.get(currentSectionIndex) : localSections.get(currentSectionIndex);
            database.openWrite();
            database.deleteBook(book.getId());
            FileOperations.deleteDir(new File(new File(sdPath, section.getPath()), book.getPath()));
            if (database.checkSectionEmpty(section.getId())) {
                database.deleteSection(section.getId());
                FileOperations.deleteDir(new File(sdPath, section.getPath()));
            }
            database.close();
        } catch (Exception ex) {
            Toast.makeText(LibraryActivity.this, R.string.library_error_bookdelete, Toast.LENGTH_LONG).show();
            return false;
        }

        this.libraryUpdated = true; //Contents changed
        if (book.getTag() == TAG_DEL || (book.getTag() == TAG_OLD && !isSynced)) { //Need to remove permanently
            //------ Remove book from merged sections ------
            if (isSynced) {
                mergedSections.get(currentSectionIndex).getBooks().remove(position);
                if (mergedSections.get(currentSectionIndex).getBooks().size() == 0) {
                    mergedSections.remove(currentSectionIndex);
                }
            }
            //------ Remove book from local sections ------
            else {
                localSections.get(currentSectionIndex).getBooks().remove(position);
                if (localSections.get(currentSectionIndex).getBooks().size() == 0) {
                    localSections.remove(currentSectionIndex);
                }
            }
            //------ Remove book from ListView ------
            ((LibraryBookAdapter) lstBooks.getAdapter()).remove(book);
            if (lstBooks.getAdapter().isEmpty()) { //No more books left
                llSections.removeViewAt(currentSectionIndex);
                if (llSections.getChildCount() > 0) { //At least 1 section left
                    int previousSectionIndex = currentSectionIndex;
                    currentSectionIndex = -1;
                    ((ToggleButton) llSections.getChildAt(Math.min(previousSectionIndex, llSections.getChildCount() - 1))).setChecked(true);
                }
            }
        } else if (book.getTag() == TAG_UPD || (book.getTag() == TAG_OLD && isSynced)) { //Make NEW
            //------ Change current section tag ------
            if (book.getTag() == TAG_OLD) { //Need to increase section tag
                ToggleButton cs = (ToggleButton) llSections.getChildAt(currentSectionIndex);
                Integer tag = (Integer) cs.getTag();
                tag = tag == null ? 1 : tag + 1;
                setButtonTag(cs, mergedSections.get(currentSectionIndex).getName(), tag);
            }
            book.setTag(TAG_NEW);
            book.setEnabled(true);
            ((LibraryBookAdapter) lstBooks.getAdapter()).notifyDataSetChanged();
        }
        defineInsUpdAllState();
        return true;
    }

    /**
     * Disables book in the list and the DB
     * @param position Position of book in list
     */
    private void disableBook(int position) {
        Book book = (Book) lstBooks.getItemAtPosition(position);
        database.openWrite();
        database.updateBook(book.getId(), false);
        database.close();
        this.libraryUpdated = true; //Contents changed
        if (isSynced && mergedSections != null) { //Disable book in merged sections
            mergedSections.get(currentSectionIndex).getBooks().get(position).setEnabled(false);
        } else { //Disable book in local sections
            localSections.get(currentSectionIndex).getBooks().get(position).setEnabled(false);
        }
        book.setEnabled(false);
        ((LibraryBookAdapter) lstBooks.getAdapter()).notifyDataSetChanged();
    }

    /**
     * Enables book in the list and the DB
     * @param position Position of book in list
     */
    private void enableBook(int position) {
        Book book = (Book) lstBooks.getItemAtPosition(position);
        File dir;
        if (isSynced) { //Search section path in merged sections
            dir = new File(new File(sdPath, mergedSections.get(currentSectionIndex).getPath()), book.getPath());
        } else { //Search section path in local sections
            dir = new File(new File(sdPath, localSections.get(currentSectionIndex).getPath()), book.getPath());
        }
        database.openWrite();
        if (FileOperations.tryEnableBook(dir, database, book.getId())) { //Book can be enabled
            database.updateBook(book.getId(), true);
            this.libraryUpdated = true; //Contents changed
            if (isSynced) { //Enable book in merged sections
                mergedSections.get(currentSectionIndex).getBooks().get(position).setEnabled(true);
            } else { //Enable book in local sections
                localSections.get(currentSectionIndex).getBooks().get(position).setEnabled(true);
            }
            book.setEnabled(true);
            ((LibraryBookAdapter) lstBooks.getAdapter()).notifyDataSetChanged();
        } else { //Book can not be enabled
            String message = String.format(getString(R.string.library_error_enable), ((Book) lstBooks.getItemAtPosition(position)).getName());
            final int index = position;
            new AlertDialog.Builder(LibraryActivity.this).setTitle(R.string.dialog_error).setIcon(android.R.drawable.ic_dialog_alert).setMessage(message)
                    .setPositiveButton(R.string.dialog_delete, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteBook(index);
                        }
                    }).setNegativeButton(R.string.dialog_leave, null).show();
        }
        database.close();
    }

    /**
     * Shows information about the book
     * @param position Position of book in list
     */
    private void bookInfo(int position) {
        Book book = (Book) lstBooks.getItemAtPosition(position);
        switch (book.getTag()) {
            case TAG_DEL: //Local book
            case TAG_OLD: //Local book
                if (book.getChapters() == null || book.getChapters().size() == 0) {
                    database.openRead();
                    book.setChapters(database.selectBookChapters(book.getId()));
                    database.close();
                }
                showBookInfo(book);
                break;
            case TAG_NEW: //Remote book
            case TAG_UPD: //Remote book
            case TAG_IGN: //Remote book
                new LoadBookInfoTask().execute(book.getId());
                break;
        }
    }

    /**
     * Shows information about the book
     * @param book Book which information to show
     */
    private void showBookInfo(Book book) {
        if (bookInfo.getParent() != null) { //View already attached to some dialog
            ((ViewGroup) bookInfo.getParent()).removeView(bookInfo);
        }
        ((TextView) bookInfo.findViewById(R.id.library_info_text)).setText(book.getInfo());
        bookInfo.findViewById(R.id.library_info_scroll).scrollTo(0, 0);
        ((ListView) bookInfo.findViewById(R.id.library_info_list)).setAdapter(new ArrayAdapter<Chapter>(this, R.layout.library_dialog_info_item, book
                .getChapters()));
        new AlertDialog.Builder(LibraryActivity.this).setTitle(book.getName()).setIcon(android.R.drawable.ic_dialog_info).setView(bookInfo)
                .setPositiveButton(R.string.dialog_close, null).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (libraryUpdated) { //Need to notify about library update
            Intent broadcastIntent = new Intent(LibraryActivity.ACTION_LIBRARY_UPDATED);
            sendBroadcast(broadcastIntent);
        }
    }

    @Override
    public void onBackPressed() {
        if (atbToolbar.isOpened()) {
            atbToolbar.hide();
            return;
        }
        if (mFlipper.getDisplayedChild() > 0) {
            showFlipperPage(0);
            return;
        }
        super.onBackPressed();
    }

    //=========================== CLASSES ===========================

    /**
     * Comparator used to compare Akathists (alphabetically)
     */
    private Comparator<Book> akathistsComparator = new Comparator<Book>() {
        @Override
        public int compare(Book book, Book book2) {
            return book.getName().compareTo(book2.getName());
        }
    };

    //=========================== LISTENERS ===========================

    /**
     * Listener for changed selection inside the local section
     */
    private OnCheckedChangeListener localSectionCheckedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            int index = llSections.indexOfChild(buttonView);
            if (isChecked && index != currentSectionIndex) {
                lstBooks.setAdapter(new LibraryBookAdapter(LibraryActivity.this, localSections.get(index).getBooks()));
                int previousSection = currentSectionIndex;
                currentSectionIndex = index;
                if (previousSection > -1) {
                    ((ToggleButton) llSections.getChildAt(previousSection)).setChecked(false);
                }
                hsvSections.scrollTo(buttonView.getLeft() - 6, 0);
                if (startingBookId > -1 && currentSectionIndex > -1 && currentSectionIndex < localSections.size()) { //Need to scroll to specific book
                    for (int i = 0; i < localSections.get(currentSectionIndex).getBooks().size(); i++) {
                        if (localSections.get(currentSectionIndex).getBooks().get(i).getId() == startingBookId) {
                            lstBooks.setSelectionFromTop(i, 0);
                            break;
                        }
                    }
                    startingBookId = -1;
                }
            }
            if (!isChecked && index == currentSectionIndex) {
                buttonView.setChecked(true);
            }
        }
    };

    /**
     * Listener for changed selection inside the merged section
     */
    private OnCheckedChangeListener mergedSectionCheckedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            int index = llSections.indexOfChild(buttonView);
            if (isChecked && index != currentSectionIndex) {
                lstBooks.setAdapter(new LibraryBookAdapter(LibraryActivity.this, mergedSections.get(index).getBooks()));
                int previousSection = currentSectionIndex;
                currentSectionIndex = index;
                if (previousSection > -1) {
                    ((ToggleButton) llSections.getChildAt(previousSection)).setChecked(false);
                }
                hsvSections.scrollTo(buttonView.getLeft() - 6, 0);
                if (startingBookId > -1 && currentSectionIndex > -1 && currentSectionIndex < localSections.size()) { //Need to scroll to specific book
                    for (int i = 0; i < localSections.get(currentSectionIndex).getBooks().size(); i++) {
                        if (localSections.get(currentSectionIndex).getBooks().get(i).getId() == startingBookId) {
                            lstBooks.setSelectionFromTop(i, 0);
                            break;
                        }
                    }
                    startingBookId = -1;
                }
            }
            if (!isChecked && index == currentSectionIndex) {
                buttonView.setChecked(true);
            }
        }
    };

    //================================================ ASYNCHRONOUS TASKS ================================================

    /**
     * Class which is used for Internet synchronization
     * @author Semeniuk A.D.
     */
    private class StartupTask extends AsyncTask<Void, Void, Exception> {
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LibraryActivity.this);
            dialog.setMessage(getString(R.string.library_syncing));
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    StartupTask.this.cancel(true);
                }
            });
            dialog.show();
        }

        @Override
        protected Exception doInBackground(Void... params) {
            try {
                database.openRead();
                localSections = database.selectSectionsWithBooks();
                database.close();
                for (Section s : localSections) {
                    if (Math.round(Math.floor(s.getOrder())) == 3) {
                        Collections.sort(s.getBooks(), akathistsComparator);
                    }
                }
                //--- Find current book
                if (startingBookId > -1) {
                    for (Section section : localSections) {
                        if (startingSectionId > -1) {
                            break;
                        }
                        for (Book book : section.getBooks()) {
                            if (book.getId() == startingBookId) {
                                startingSectionId = section.getId();
                                break;
                            }
                        }
                    }
                }
                //---
                return null;
            } catch (Exception ex) {
                return ex;
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(LibraryActivity.this, R.string.dialog_canceled, Toast.LENGTH_SHORT).show();
            LibraryActivity.this.finish();
        }

        @Override
        protected void onPostExecute(Exception error) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if (error == null) { //Success
                if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("sync")) { //Need to sync right away
                    new SynchronizationTask().execute();
                } else { //Fetch local data only
                    populateLocalData(startingSectionId);
                }
            } else { //Error happened
                new AlertDialog.Builder(LibraryActivity.this).setTitle(R.string.dialog_error).setIcon(android.R.drawable.ic_dialog_alert).setMessage(R.string.library_error_db)
                        .setPositiveButton(R.string.dialog_close, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                LibraryActivity.this.finish();
                            }
                        }).setCancelable(false).show();
            }
        }

    }

    //====================================================================================================================

    /**
     * Class which is used for Internet synchronization
     * @author Semeniuk A.D.
     */
    private class SynchronizationTask extends AsyncTask<Void, Void, Exception> {
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            lstIgnored.setAdapter(null);
            dialog = new ProgressDialog(LibraryActivity.this);
            dialog.setMessage(getString(R.string.library_syncing));
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    SynchronizationTask.this.cancel(true);
                }
            });
            dialog.show();
        }

        @Override
        protected Exception doInBackground(Void... params) {
            try {
                generateMergedSections(ServerConnector.getAvailableSections());
                return null;
            } catch (Exception ex) { //Error happened
                return ex;
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(LibraryActivity.this, R.string.dialog_canceled, Toast.LENGTH_SHORT).show();
            mergedSections = localSections;
            populateMergedData(currentSectionIndex > -1 && localSections.size() > 0 ? localSections.get(currentSectionIndex).getId() : -1);
        }

        @Override
        protected void onPostExecute(Exception error) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if (error == null) { //Success
                populateMergedData(currentSectionIndex > -1 && localSections.size() > 0 ? localSections.get(currentSectionIndex).getId() : -1);
                mActionIgnored.setVisibility(ignoredFound ? View.VISIBLE : View.GONE);
                isSynced = true;
                mActionSync.setVisibility(View.GONE);
            } else { //Error happened
                failed(error);
            }
            defineInsUpdAllState();
            if (isSynced && !mActionInsupd.isEnabled()) { //No updates
                Toast.makeText(LibraryActivity.this, R.string.library_insupdall_nothing, Toast.LENGTH_SHORT).show();
            }
        }

        /**
         * Error during import process. Request for re-import
         * @param exception Exception which was thrown
         */
        private void failed(Exception exception) {
            final Exception error = exception;
            DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            new SynchronizationTask().execute();
                            break;
                        case DialogInterface.BUTTON_NEUTRAL:
                            details(error);
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };
            AlertDialog.Builder dialog = new AlertDialog.Builder(LibraryActivity.this).setTitle(R.string.library_error)
                    .setIcon(android.R.drawable.ic_dialog_alert).setMessage(String.format(getString(R.string.library_error_sync), error.getMessage()))
                    .setPositiveButton(R.string.dialog_retry, ocl).setNegativeButton(R.string.dialog_cancel, ocl).setCancelable(false);
            if (error.getStackTrace() != null) {
                dialog.setNeutralButton(R.string.dialog_details, ocl);
            }
            try { //Necessary precaution
                dialog.show();
            } catch (Exception ex) {
                Toast.makeText(LibraryActivity.this, String.format(getString(R.string.library_error_sync), error.getMessage()), Toast.LENGTH_LONG).show();
            }
        }

        /**
         * Error during import process. Request for re-import
         * @param exception Exception which was thrown
         */
        private void details(Exception exception) {
            final Exception error = exception;
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            error.printStackTrace(pw);
            final String trace = sw.toString();
            DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"alexandr.semeniuk@gmail.com"});
                        i.putExtra(Intent.EXTRA_SUBJECT, "Synchronization error");
                        i.putExtra(Intent.EXTRA_TEXT, trace);
                        try {
                            startActivity(i);
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(LibraryActivity.this, R.string.library_error_send, Toast.LENGTH_SHORT).show();
                        }
                    }
                    failed(error);
                }
            };
            new AlertDialog.Builder(LibraryActivity.this).setTitle(R.string.library_error).setIcon(android.R.drawable.ic_dialog_alert).setMessage(trace)
                    .setPositiveButton(R.string.dialog_send, ocl).setNegativeButton(R.string.dialog_close, ocl).setCancelable(false).show();
        }

    }

    //====================================================================================================================

    /**
     * Class which is used for Book Info loading
     * @author Semeniuk A.D.
     */
    private class LoadBookInfoTask extends AsyncTask<Integer, Void, Book> {
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LibraryActivity.this);
            dialog.setMessage(getString(R.string.library_syncing));
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    LoadBookInfoTask.this.cancel(true);
                }
            });
            dialog.show();
        }

        @Override
        protected Book doInBackground(Integer... params) {
            try {
                return ServerConnector.getBookInfo(params[0]);
            } catch (Exception ex) { //Error happened
                return null;
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(LibraryActivity.this, R.string.dialog_canceled, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(Book book) {
            dialog.dismiss();
            if (book != null && book.getChapters() != null) { //Success
                showBookInfo(book);
            } else { //Error happened
                Toast.makeText(LibraryActivity.this, R.string.library_error_bookinfo, Toast.LENGTH_LONG).show();
            }
        }

    }

    //====================================================================================================================

    /**
     * Class which is used for Book installing
     * @author Semeniuk A.D.
     */
    private class InstallBookTask extends AsyncTask<Item, Void, Exception> {
        private ProgressDialog dialog;
        private Book book;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LibraryActivity.this);
            dialog.setMessage(getString(R.string.library_syncing));
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    InstallBookTask.this.cancel(true);
                }
            });
            dialog.show();
        }

        @Override
        protected Exception doInBackground(Item... params) {
            try {
                Section section = (Section) params[0];
                this.book = (Book) params[1];

                database.openWrite();

                // ------ Check whether section installation is needed ------
                if (!database.checkSectionExists(section.getId())) { //No section present in database
                    section = ServerConnector.getSectionInfo(section.getId());
                    FileOperations.createSectionDir(sdPath, section);
                    database.insertSection(section);
                }
                // ------

                Book b = ServerConnector.getBookInfo(book.getId());
                FileOperations.createBookDir(new File(sdPath, section.getPath()), b);
                // ------ Insert book into database
                b.setEnabled(true);
                b.setSectionId(section.getId());
                database.insertBook(b);
                for (Chapter c : b.getChapters()) {
                    c.setBookId(b.getId());
                    database.insertChapter(c);
                }
                // ------

                database.close();

                File path = new File(new File(sdPath, section.getPath()), b.getPath());
                File temp = new File(path, "book.zip");
                FileOperations.saveFile(ServerConnector.loadBookZip(b.getId()), temp);
                FileOperations.extractZip(temp, path);
                temp.delete();
                return null;
            } catch (Exception ex) { //Error happened
                return ex;
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(LibraryActivity.this, R.string.dialog_canceled, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(Exception error) {
            dialog.dismiss();
            if (error == null) { //Success
                LibraryActivity.this.libraryUpdated = true; //Contents changed
                //------ Change current section tag ------
                if (book.getTag() == TAG_NEW) {
                    ToggleButton cs = (ToggleButton) llSections.getChildAt(currentSectionIndex);
                    Integer tag = (Integer) cs.getTag();
                    tag = tag == null ? 0 : tag - 1;
                    setButtonTag(cs, mergedSections.get(currentSectionIndex).getName(), tag);
                }
                //------
                getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().remove(String.format("ign%d", book.getId())).commit();
                this.book.setTag(TAG_OLD);
                this.book.setEnabled(true);
                ((LibraryBookAdapter) lstBooks.getAdapter()).notifyDataSetChanged();
                defineInsUpdAllState();
            } else { //Error happened
                Toast.makeText(LibraryActivity.this, String.format(getString(R.string.library_error_bookinstall), error.getMessage()), Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    //====================================================================================================================

    /**
     * Class which is used for installing/updating of all available books
     * @author Semeniuk A.D.
     */
    private class InstallUpdateAllTask extends AsyncTask<Void, Integer, Exception> {
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LibraryActivity.this);
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setMessage(getString(R.string.library_syncing));
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    InstallUpdateAllTask.this.cancel(true);
                }
            });
            dialog.setMax(calculateInsUpdBookCount());
            dialog.show();
        }

        @Override
        protected Exception doInBackground(Void... params) {
            try {
                int index = 0;
                database.openWrite();
                for (Section section : mergedSections) {

                    // ------ Check whether section installation is needed ------
                    if (!database.checkSectionExists(section.getId())) { //No section present in database
                        Section section2 = ServerConnector.getSectionInfo(section.getId());
                        FileOperations.createSectionDir(sdPath, section2);
                        database.insertSection(section2);
                    }
                    // ------

                    for (Book book : section.getBooks()) {
                        if (book.getTag() == TAG_NEW || book.getTag() == TAG_UPD) { //Book need to be installed/updated
                            Book book2 = ServerConnector.getBookInfo(book.getId());
                            FileOperations.createBookDir(new File(sdPath, section.getPath()), book2);
                            // ------ Insert book into database
                            book2.setEnabled(true);
                            book2.setSectionId(section.getId());
                            if (book.getTag() == TAG_UPD) { //Update (need to delete first)
                                database.deleteBook(book.getId());
                            }
                            database.insertBook(book2);
                            for (Chapter c : book2.getChapters()) {
                                c.setBookId(book2.getId());
                                database.insertChapter(c);
                            }
                            // ------

                            File path = new File(new File(sdPath, section.getPath()), book2.getPath());
                            File temp = new File(path, "book.zip");
                            FileOperations.saveFile(ServerConnector.loadBookZip(book2.getId()), temp);
                            FileOperations.extractZip(temp, path);
                            temp.delete();
                            book.setTag(TAG_OLD);
                            book.setEnabled(true);
                            section.setTag(section.getTag() - 1);

                            publishProgress(++index);
                            LibraryActivity.this.libraryUpdated = true; //Contents changed
                        }
                    }
                }
                database.close();
                return null;
            } catch (Exception ex) { //Error happened
                database.close();
                return ex;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            dialog.setProgress(values[0]);
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(LibraryActivity.this, R.string.dialog_canceled, Toast.LENGTH_SHORT).show();
            populateMergedData(currentSectionIndex > -1 && mergedSections.size() > 0 ? mergedSections.get(currentSectionIndex).getId() : -1);
            defineInsUpdAllState();
        }

        @Override
        protected void onPostExecute(Exception error) {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            if (error != null) { //Error happened
                Toast.makeText(LibraryActivity.this, String.format(getString(R.string.library_error_bookinstall), error.getMessage()), Toast.LENGTH_LONG)
                        .show();
            }
            populateMergedData(currentSectionIndex > -1 && mergedSections.size() > 0 ? mergedSections.get(currentSectionIndex).getId() : -1);
            defineInsUpdAllState();
        }
    }
}