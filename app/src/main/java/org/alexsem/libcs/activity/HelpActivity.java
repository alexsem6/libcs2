package org.alexsem.libcs.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import org.alexsem.libcs.transfer.FileOperations;
import org.alexsem.libcs.transfer.ServerConnector;

import java.io.File;

public class HelpActivity extends Activity {

	private static final String VERSION = "h_2.7";

	private String storagePath;
	private WebView web;
	private File file;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPreferences preferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);

		web = new WebView(this);
		web.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (url.startsWith("mailto:")) { //"mailto:" link clicked
					url = url.replaceFirst("mailto:", "").trim();
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setType("plain/text").putExtra(Intent.EXTRA_EMAIL, new String[] { url });
					HelpActivity.this.startActivity(i);
					return true;
				} else {
					view.loadUrl(url);
					return true;
				}
			}
		});
		setContentView(web, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		storagePath = preferences.getString("SDpath", "");
		file = new File(new File(storagePath, "help"), "index.html");

		if (!preferences.contains(HelpActivity.VERSION)) {
			file.delete();
			preferences.edit().putBoolean(HelpActivity.VERSION, true).commit();
		}

		load();
	}

	/**
	 * Loads and displays contents of help index file
	 */
	private void load() {
		try {
			if (file.exists()) {
				web.loadUrl(file.toURL().toString());
			} else {
				error(true);
			}
		} catch (Exception e) { //Should never happen
			error(true);
		}
	}

	/**
	 * Shows error message
	 * @param firstAttempt true if "notfound" message should be shown, false if
	 *          load error
	 */
	private void error(boolean firstAttempt) {
		DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						new HelpLoaderTask().execute();
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						HelpActivity.this.finish();
						break;
				}
			}
		};
		new AlertDialog.Builder(this).setTitle(R.string.dialog_error).setIcon(android.R.drawable.ic_dialog_alert)
				.setMessage(firstAttempt ? R.string.help_error_notfound : R.string.help_error_load).setCancelable(false)
				.setPositiveButton(firstAttempt ? R.string.dialog_load : R.string.dialog_retry, ocl).setNegativeButton(R.string.dialog_close, ocl).show();
	}

	//-----------------------------------------------------------

	/**
	 * Task used for loading of help files
	 * @author Semeniuk A.D.
	 */
	private class HelpLoaderTask extends AsyncTask<Void, Void, Boolean> {
		private ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			progress = new ProgressDialog(HelpActivity.this);
			progress.setMessage(getString(R.string.dialog_loading));
			progress.show();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				File help = new File(storagePath, "help.zip");
				FileOperations.saveFile(ServerConnector.getHelpZip(), help);
				FileOperations.extractZip(help, new File(storagePath, "help"));
				help.delete();
				return true;
			} catch (Exception e) {
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (progress != null && progress.isShowing()) {
				progress.dismiss();
			}
			if (result) {
				load();
			} else {
				error(false);
			}
		}
	}

}