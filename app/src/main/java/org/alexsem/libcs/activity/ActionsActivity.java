package org.alexsem.libcs.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import org.alexsem.libcs.model.OptionsActions;
import org.alexsem.libcs.model.OptionsMain;
import org.alexsem.libcs.model.QuickAction;
import org.alexsem.libcs.model.ReaderZone;
import org.alexsem.libcs.transfer.OptionsManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Activity used to setup quick actions
 * @author Semeniuk A.D.
 */
public class ActionsActivity extends Activity {

    private OptionsActions optionsActions;

    private Map<ReaderZone, Button> buttons = new HashMap<ReaderZone, Button>();
    private String[] actions;
    private final int[] dividerIds = {R.id.actions_divider_top_12, R.id.actions_divider_top_23, R.id.actions_divider_horz_12, R.id.actions_divider_center_12, R.id.actions_divider_center_23, R.id.actions_divider_horz_23, R.id.actions_divider_bottom_12, R.id.actions_divider_bottom_23};


    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.actions);

        //------------------- Find UI controls --------------------
        buttons.put(ReaderZone.BOTTOM_LEFT, (Button) findViewById(R.id.actions_bottom_left));
        buttons.put(ReaderZone.BOTTOM, (Button) findViewById(R.id.actions_bottom_center));
        buttons.put(ReaderZone.BOTTOM_RIGHT, (Button) findViewById(R.id.actions_bottom_right));
        buttons.put(ReaderZone.LEFT, (Button) findViewById(R.id.actions_middle_left));
        buttons.put(ReaderZone.CENTER, (Button) findViewById(R.id.actions_middle_center));
        buttons.put(ReaderZone.RIGHT, (Button) findViewById(R.id.actions_middle_right));
        buttons.put(ReaderZone.TOP_LEFT, (Button) findViewById(R.id.actions_top_left));
        buttons.put(ReaderZone.TOP, (Button) findViewById(R.id.actions_top_center));
        buttons.put(ReaderZone.TOP_RIGHT, (Button) findViewById(R.id.actions_top_right));

        //-------------------- Load preferences --------------------
        OptionsMain optionsMain = OptionsManager.loadSettings(this);
        optionsActions = OptionsManager.loadActions(this);
        actions = getResources().getStringArray(R.array.actions_aliases);

        //------------------- Apply preferences ---------------------
        findViewById(android.R.id.content).setBackgroundColor(optionsMain.fc_back_color());
        for (int id : dividerIds) {
            findViewById(id).setBackgroundColor(optionsMain.fc_other_color_service);
        }
        switch (optionsMain.orientation) {
            case 0: //Sensor
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                break;
            case 1: //Portrait (0)
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            case 2: //Landscape (90)
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                break;
            case 3: //Portrait (180)
                setRequestedOrientation(9);
                break;
            case 4: //Landscape (270)
                setRequestedOrientation(8);
                break;
            case 5: //Sensor Portrait
                setRequestedOrientation(7);
                break;
            case 6: //Sensor Landscape
                setRequestedOrientation(6);
                break;
            case 7: //System
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
                break;
        }

        //------------------- Setup actions --------------------
        for (ReaderZone zone : ReaderZone.values()) {
            buttons.get(zone).setTextColor(optionsMain.fc_text_color());
            buttons.get(zone).setTag(zone);
            buttons.get(zone).setOnClickListener(buttonShortListener);
            buttons.get(zone).setOnLongClickListener(buttonLongListener);
            setButtonText(zone);
        }
    }


    /**
     * Set text for the specific button
     * @param zone Reader zone
     */
    private void setButtonText(ReaderZone zone) {
        if (optionsActions.actionsShort.get(zone) == QuickAction.GOTO_NEXT_PAGE_LONG) {
            buttons.get(zone).setText(actions[QuickAction.GOTO_NEXT_PAGE_LONG.ordinal()]);
        } else if (optionsActions.actionsShort.get(zone) == QuickAction.GOTO_PREVIOUS_PAGE_LONG) {
            buttons.get(zone).setText(actions[QuickAction.GOTO_PREVIOUS_PAGE_LONG.ordinal()]);
        } else {
            String shortAction = actions[optionsActions.actionsShort.get(zone).ordinal()];
            String longAction = actions[optionsActions.actionsLong.get(zone).ordinal()];
            SpannableString text = new SpannableString(String.format("%s\n- - -\n%s", shortAction, longAction));
            if (zone == ReaderZone.CENTER) {
                text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.neutral)), 0, shortAction.length(), SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
            }
            buttons.get(zone).setText(text);
        }
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_VOLUME_UP:
                    performVolumeClick(true);
                    return true;
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                    performVolumeClick(false);
                    return true;
            }
        }
        return (event.getAction() == KeyEvent.ACTION_UP && (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN)) || super.dispatchKeyEvent(event);
    }

    //================================= LISTENERS =================================

    /**
     * Listener for regular button press
     */
    private View.OnClickListener buttonShortListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final ReaderZone zone = (ReaderZone) view.getTag();
            if (zone == ReaderZone.CENTER) {
                return;
            }
            new AlertDialog.Builder(ActionsActivity.this)
                    .setTitle(R.string.actions_dialog_title)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setSingleChoiceItems(actions, optionsActions.actionsShort.get(zone).ordinal(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            QuickAction action = QuickAction.values()[which];
                            if (optionsActions.actionsLong.get(zone) == QuickAction.GOTO_NEXT_PAGE_LONG
                                    || optionsActions.actionsLong.get(zone) == QuickAction.GOTO_PREVIOUS_PAGE_LONG
                                    || action == QuickAction.GOTO_NEXT_PAGE_LONG
                                    || action == QuickAction.GOTO_PREVIOUS_PAGE_LONG) { //Need to populate the same to long action
                                optionsActions.actionsLong.put(zone, action);
                                OptionsManager.saveAction(ActionsActivity.this, true, zone, action);
                            }
                            optionsActions.actionsShort.put(zone, action);
                            OptionsManager.saveAction(ActionsActivity.this, false, zone, action);
                            setResult(RESULT_OK);
                            setButtonText(zone);
                            dialog.dismiss();
                        }
                    }).setNegativeButton(R.string.dialog_cancel, null).show();
        }
    };

    /**
     * Listener for long button press
     */
    private View.OnLongClickListener buttonLongListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            final ReaderZone zone = (ReaderZone) view.getTag();
            new AlertDialog.Builder(ActionsActivity.this)
                    .setTitle(R.string.actions_dialog_title)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setSingleChoiceItems(actions, optionsActions.actionsLong.get(zone).ordinal(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            QuickAction action = QuickAction.values()[which];
                            if (optionsActions.actionsShort.get(zone) == QuickAction.GOTO_NEXT_PAGE_LONG
                                    || optionsActions.actionsShort.get(zone) == QuickAction.GOTO_PREVIOUS_PAGE_LONG
                                    || action == QuickAction.GOTO_NEXT_PAGE_LONG
                                    || action == QuickAction.GOTO_PREVIOUS_PAGE_LONG) { //Need to populate the same to long action
                                if (zone == ReaderZone.CENTER) {
                                    return;
                                }
                                optionsActions.actionsShort.put(zone, action);
                                OptionsManager.saveAction(ActionsActivity.this, false, zone, action);
                            }
                            optionsActions.actionsLong.put(zone, action);
                            OptionsManager.saveAction(ActionsActivity.this, true, zone, action);
                            setResult(RESULT_OK);
                            setButtonText(zone);
                            dialog.dismiss();
                        }
                    }).setNegativeButton(R.string.dialog_cancel, null).show();
            return true;
        }
    };

    /**
     * Listener for long button press
     */
    private void performVolumeClick(final boolean isUp) {
        new AlertDialog.Builder(ActionsActivity.this).setTitle(R.string.actions_dialog_title).setIcon(android.R.drawable.ic_dialog_info)
                .setSingleChoiceItems(actions, optionsActions.actionsVolume.get(isUp).ordinal(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        QuickAction action = QuickAction.values()[which];
                        if (action == QuickAction.GOTO_NEXT_PAGE_LONG) {
                            action = QuickAction.GOTO_NEXT_PAGE_1;
                        } else if (action == QuickAction.GOTO_PREVIOUS_PAGE_LONG) {
                            action = QuickAction.GOTO_PREVIOUS_PAGE_1;
                        }
                        optionsActions.actionsVolume.put(isUp, action);
                        OptionsManager.saveVolumeAction(ActionsActivity.this, isUp, action);
                        setResult(Activity.RESULT_OK);
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.dialog_cancel, null).show();
    }

}