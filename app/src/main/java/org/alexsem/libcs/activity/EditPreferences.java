package org.alexsem.libcs.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;

import org.alexsem.libcs.widget.ColorPickerDialog;
import org.alexsem.libcs.widget.ColorPickerDialog.OnColorPickerListener;
import org.alexsem.libcs.widget.ColorPreference;
import org.alexsem.libcs.widget.FontSizePickerDialog;
import org.alexsem.libcs.widget.TypefacePickerDialog;
import org.alexsem.libcs.widget.TypefacePickerDialog.OnTypefacePickerListener;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class EditPreferences extends PreferenceActivity implements OnSharedPreferenceChangeListener {


    private Preference currentColorPreference;
    private static final Map<String, Integer> COLORS_DEFAULT;
    private static String TYPEFACE_DEFAULT;
    private static int FONTSIZE_DEFAULT;
    private static int FONTSIZE_MIN;
    private static int FONTSIZE_MAX;

    static {
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("fc_back_color_day", Color.parseColor("#eeeeee"));
        map.put("fc_back_color_night", Color.parseColor("#000000"));
        map.put("fc_text_color_day", Color.parseColor("#000000"));
        map.put("fc_text_color_night", Color.parseColor("#dddddd"));
        map.put("fc_text_color_cinnabar", Color.parseColor("#e34234"));
        map.put("fc_text_color_link", Color.parseColor("#0b7ff6"));
        map.put("fc_other_color_service", Color.parseColor("#808080"));
        COLORS_DEFAULT = Collections.unmodifiableMap(map);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        TYPEFACE_DEFAULT = getString(R.string.typeface_default);
        FONTSIZE_DEFAULT = getResources().getInteger(R.integer.picker_fontsize_default);
        FONTSIZE_MIN = getResources().getInteger(R.integer.picker_fontsize_min);
        FONTSIZE_MAX = getResources().getInteger(R.integer.picker_fontsize_max);

        updateSummary(getPreferenceScreen().findPreference("orientation"));
        updateSummary(getPreferenceScreen().findPreference("hyphenate_mode"));
        updateSummary(getPreferenceScreen().findPreference("columns_port"));
        updateSummary(getPreferenceScreen().findPreference("columns_land"));
        updateSummary(getPreferenceScreen().findPreference("fc_text_size"));
        updateSummary(getPreferenceScreen().findPreference("fc_text_spacing"));
        updateSummary(getPreferenceScreen().findPreference("notify_rate"));

        initTypefacePreference(getPreferenceScreen().findPreference("fc_text_face"));
        initFontSizePreference(getPreferenceScreen().findPreference("fc_text_size"));

        initColorPreference(getPreferenceScreen().findPreference("fc_back_color_day"));
        initColorPreference(getPreferenceScreen().findPreference("fc_back_color_night"));
        initColorPreference(getPreferenceScreen().findPreference("fc_text_color_day"));
        initColorPreference(getPreferenceScreen().findPreference("fc_text_color_night"));
        initColorPreference(getPreferenceScreen().findPreference("fc_text_color_cinnabar"));
        initColorPreference(getPreferenceScreen().findPreference("fc_text_color_link"));
        initColorPreference(getPreferenceScreen().findPreference("fc_other_color_service"));

        int mode = Integer.valueOf(((ListPreference) getPreferenceScreen().findPreference("hyphenate_mode")).getValue());
        CheckBoxPreference p = (CheckBoxPreference) getPreferenceScreen().findPreference("fill_line");
        if (mode > 0) {
            p.setEnabled(true);
        } else {
            p.setEnabled(false);
            p.setChecked(false);
        }

        //--- In case preview starts from "Fonts & Colors" right away ---
        if (getIntent().hasExtra("fonts")) {
            PreferenceScreen fonts = (PreferenceScreen) getPreferenceScreen().findPreference("pref_fc");
            getPreferenceScreen().onItemClick(null, null, fonts.getOrder() + 1, 0); //Weird
            if (fonts.getDialog() != null) {
                fonts.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        EditPreferences.this.finish();
                    }
                });
            }
        }

    }

    private OnPreferenceClickListener colorPreferenceClickListener = new OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            currentColorPreference = preference;
            new ColorPickerDialog(EditPreferences.this, PreferenceManager.getDefaultSharedPreferences(EditPreferences.this).getInt(
                    currentColorPreference.getKey(), COLORS_DEFAULT.get(currentColorPreference.getKey())), new OnColorPickerListener() {
                @Override
                public void onColorPicked(int color) {
                    PreferenceManager.getDefaultSharedPreferences(EditPreferences.this).edit().putInt(currentColorPreference.getKey(), color).commit();
                    ((ColorPreference) currentColorPreference).setColor(color);
                }
            }).show();
            return true;
        }
    };

    private OnPreferenceClickListener typefacePreferenceClickListener = new OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            final Preference currentPreference = preference;
            new TypefacePickerDialog(EditPreferences.this, PreferenceManager.getDefaultSharedPreferences(EditPreferences.this).getString(currentPreference.getKey(),
                    TYPEFACE_DEFAULT), new OnTypefacePickerListener() {
                @Override
                public void onTypefacePicked(String typeface) {
                    PreferenceManager.getDefaultSharedPreferences(EditPreferences.this).edit().putString(currentPreference.getKey(), typeface).commit();
                    currentPreference.setSummary(typeface);
                    setResult(RESULT_OK);
                }
            }).show();
            return true;
        }
    };

    /**
     * Listener for font size preference clicks
     */
    private OnPreferenceClickListener fontSizePreferenceClickListener = new OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            final Preference currentPreference = preference;
            int value = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(EditPreferences.this).getString(currentPreference.getKey(), String.valueOf(FONTSIZE_DEFAULT)));
            new FontSizePickerDialog(EditPreferences.this, value, FONTSIZE_MIN, FONTSIZE_MAX,
                    new FontSizePickerDialog.OnFontSizePickerListener() {
                        @Override
                        public void onFontSizePicked(int size) {
                            String value = String.valueOf(size);
                            PreferenceManager.getDefaultSharedPreferences(EditPreferences.this).edit().putString(currentPreference.getKey(), value).commit();
                            currentPreference.setSummary(value);
                            setResult(RESULT_OK);
                        }
                    }
            ).show();
            return true;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        setResult(RESULT_OK);
        updateSummary(findPreference(key));
        if (key.equals("notify_rate")) { //Notification rate changed
            Intent updateIntent = new Intent(this, UpdateService.class);
            updateIntent.putExtra("ratechanged", true);
            startService(updateIntent);
        }
        if (key.equals("hyphenate_mode")) { //Hyphenation changed
            int mode = Integer.valueOf(((ListPreference) getPreferenceScreen().findPreference(key)).getValue());
            CheckBoxPreference p = (CheckBoxPreference) getPreferenceScreen().findPreference("fill_line");
            if (mode > 0) {
                p.setEnabled(true);
            } else {
                p.setEnabled(false);
                p.setChecked(false);
            }
        }
    }

    /**
     * Sets list preference summary to its current value
     * @param p Preference in question
     */
    private void updateSummary(Preference p) {
        if (p instanceof ListPreference) {
            p.setSummary(((ListPreference) p).getEntry());
        }
    }

    /**
     * Initializes ColorPreference
     * @param p Preference in question
     */
    private void initColorPreference(Preference p) {
        p.setOnPreferenceClickListener(colorPreferenceClickListener);
        ((ColorPreference) p).setColor(PreferenceManager.getDefaultSharedPreferences(EditPreferences.this).getInt(p.getKey(),
                COLORS_DEFAULT.get(p.getKey())));
    }

    /**
     * Initializes TypefacePreference
     * @param p Preference in question
     */
    private void initTypefacePreference(Preference p) {
        p.setOnPreferenceClickListener(typefacePreferenceClickListener);
        p.setSummary(PreferenceManager.getDefaultSharedPreferences(EditPreferences.this).getString(p.getKey(), TYPEFACE_DEFAULT));
    }

    /**
     * Initializes FontSizePreference
     * @param p Preference in question
     */
    private void initFontSizePreference(Preference p) {
        p.setOnPreferenceClickListener(fontSizePreferenceClickListener);
        p.setSummary(PreferenceManager.getDefaultSharedPreferences(this).getString(p.getKey(), String.valueOf(FONTSIZE_DEFAULT)));
    }

}