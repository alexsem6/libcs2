package org.alexsem.libcs.transfer;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;

import org.alexsem.libcs.model.QuickAction;
import org.alexsem.libcs.activity.R;
import org.alexsem.libcs.model.OptionsActions;
import org.alexsem.libcs.model.OptionsMain;
import org.alexsem.libcs.model.ReaderZone;

/**
 * Helper class to manage user preferences
 */
public abstract class OptionsManager {

    /**
     * Load main preferences into specific object
     * @param context Context
     * @return Populated {@link OptionsMain} object
     */
    public static OptionsMain loadSettings(Context context) {
        OptionsMain options = new OptionsMain();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        options.everyRun = prefs.getBoolean("every_run", false);
        options.notifyRate = Integer.valueOf(prefs.getString("notify_rate", "0"));
        options.fc_back_color_day = prefs.getInt("fc_back_color_day", Color.parseColor("#eeeeee"));
        options.fc_back_color_night = prefs.getInt("fc_back_color_night", Color.parseColor("#000000"));
        options.fc_text_color_day = prefs.getInt("fc_text_color_day", Color.parseColor("#000000"));
        options.fc_text_color_night = prefs.getInt("fc_text_color_night", Color.parseColor("#dddddd"));
        options.fc_text_color_cinnabar = prefs.getInt("fc_text_color_cinnabar", Color.parseColor("#e34234"));
        options.fc_text_color_link = prefs.getInt("fc_text_color_link", Color.parseColor("#0b7ff6"));
        options.fc_other_color_service = prefs.getInt("fc_other_color_service", Color.parseColor("#808080"));
        options.fc_text_face = prefs.getString("fc_text_face", context.getString(R.string.typeface_default));
        options.fc_text_size = Float.valueOf(prefs.getString("fc_text_size", "21"));
        options.fc_text_size *= context.getResources().getDisplayMetrics().density;
        options.fc_text_spacing = Float.valueOf(prefs.getString("fc_text_spacing", "1.0"));
        options.orientation = Integer.valueOf(prefs.getString("orientation", "0"));
        options.animation = prefs.getBoolean("animation", true);
        options.immersive = prefs.getBoolean("immersive", false);
        options.neighbours = prefs.getBoolean("neighbours", true);
        options.hyphenateMode = Integer.valueOf(prefs.getString("hyphenate_mode", "2"));
        options.fillLine = prefs.getBoolean("fill_line", true);
        options.colPort = Integer.valueOf(prefs.getString("columns_port", "1"));
        options.colLand = Integer.valueOf(prefs.getString("columns_land", "2"));
        return options;
    }


    /**
     * Load quick actions into specific object
     * @param context Context
     * @return Populated {@link OptionsActions} object
     */
    public static OptionsActions loadActions(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
        OptionsActions options = new OptionsActions();
        options.actionsShort.put(ReaderZone.BOTTOM_LEFT, QuickAction.getByCode(prefs.getInt("act_short_bl", 2)));
        options.actionsShort.put(ReaderZone.BOTTOM, QuickAction.getByCode(prefs.getInt("act_short_bc", 1)));
        options.actionsShort.put(ReaderZone.BOTTOM_RIGHT, QuickAction.getByCode(prefs.getInt("act_short_br", 1)));
        options.actionsShort.put(ReaderZone.LEFT, QuickAction.getByCode(prefs.getInt("act_short_ml", 2)));
        options.actionsShort.put(ReaderZone.CENTER, QuickAction.getByCode(prefs.getInt("act_short_mc", 13)));
        options.actionsShort.put(ReaderZone.RIGHT, QuickAction.getByCode(prefs.getInt("act_short_mr", 1)));
        options.actionsShort.put(ReaderZone.TOP_LEFT, QuickAction.getByCode(prefs.getInt("act_short_tl", 2)));
        options.actionsShort.put(ReaderZone.TOP, QuickAction.getByCode(prefs.getInt("act_short_tc", 2)));
        options.actionsShort.put(ReaderZone.TOP_RIGHT, QuickAction.getByCode(prefs.getInt("act_short_tr", 1)));
        options.actionsLong.put(ReaderZone.BOTTOM_LEFT, QuickAction.getByCode(prefs.getInt("act_long_bl", 2)));
        options.actionsLong.put(ReaderZone.BOTTOM, QuickAction.getByCode(prefs.getInt("act_long_bc", 1)));
        options.actionsLong.put(ReaderZone.BOTTOM_RIGHT, QuickAction.getByCode(prefs.getInt("act_long_br", 1)));
        options.actionsLong.put(ReaderZone.LEFT, QuickAction.getByCode(prefs.getInt("act_long_ml", 2)));
        options.actionsLong.put(ReaderZone.CENTER, QuickAction.getByCode(prefs.getInt("act_long_mc", 14)));
        options.actionsLong.put(ReaderZone.RIGHT, QuickAction.getByCode(prefs.getInt("act_long_mr", 1)));
        options.actionsLong.put(ReaderZone.TOP_LEFT, QuickAction.getByCode(prefs.getInt("act_long_tl", 2)));
        options.actionsLong.put(ReaderZone.TOP, QuickAction.getByCode(prefs.getInt("act_long_tc", 2)));
        options.actionsLong.put(ReaderZone.TOP_RIGHT, QuickAction.getByCode(prefs.getInt("act_long_tr", 1)));
        options.actionsVolume.put(true, QuickAction.getByCode(prefs.getInt("act_volume_up", 4)));
        options.actionsVolume.put(false, QuickAction.getByCode(prefs.getInt("act_volume_dn", 3)));
        return options;
    }

    /**
     * Save action assigned to specific button
     * @param context Context
     * @param isLong  true for long press, false for regular press
     * @param zone    Reader zone
     * @param action  Action to assign
     */
    public static void saveAction(Context context, boolean isLong, ReaderZone zone, QuickAction action) {
        String zoneString;
        switch (zone) {
            case TOP:
                zoneString = "tc";
                break;
            case TOP_RIGHT:
                zoneString = "tr";
                break;
            case RIGHT:
                zoneString = "mr";
                break;
            case BOTTOM_RIGHT:
                zoneString = "br";
                break;
            case BOTTOM:
                zoneString = "bc";
                break;
            case BOTTOM_LEFT:
                zoneString = "bl";
                break;
            case LEFT:
                zoneString = "ml";
                break;
            case TOP_LEFT:
                zoneString = "tl";
                break;
            case CENTER:
                zoneString = "mc";
                break;
            default:
                zoneString = "";
                break;
        }
        String name = String.format("act_%s_%s", isLong ? "long" : "short", zoneString);
        context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE).edit().putInt(name, action.getCode()).commit();
    }

    /**
     * Save action assigned to specific volume buttons
     * @param context Context
     * @param isUp    true for "Volume Up", false for "Volume Down"
     * @param action  Action to assign
     */
    public static void saveVolumeAction(Context context, boolean isUp, QuickAction action) {
        String name = String.format("act_volume_%s", isUp ? "up" : "dn");
        context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE).edit().putInt(name, action.getCode()).commit();
    }


}
