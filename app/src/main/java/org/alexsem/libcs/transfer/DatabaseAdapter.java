package org.alexsem.libcs.transfer;

import java.util.ArrayList;
import java.util.List;

import org.alexsem.libcs.model.BmGroup;
import org.alexsem.libcs.model.Book;
import org.alexsem.libcs.model.Bookmark;
import org.alexsem.libcs.model.Chapter;
import org.alexsem.libcs.model.HistoryItem;
import org.alexsem.libcs.model.Position;
import org.alexsem.libcs.model.Report;
import org.alexsem.libcs.model.Section;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAdapter {

    //Variable to hold the database instance
    private SQLiteDatabase db;
    // Database open/upgrade helper
    private SQLiteOpenHelper dbHelper;

    private int connectionCount = 0;

    /**
     * Constructor
     * @param context Context used by the database
     */
    public DatabaseAdapter(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    /**
     * Opens connection to the database for reading
     * @throws SQLException if connection was not established
     */
    public synchronized void openRead() throws SQLException {
        connectionCount++;
        db = dbHelper.getReadableDatabase();
    }

    /**
     * Opens connection to the database for writing
     * @throws SQLException if connection was not established
     */
    public synchronized void openWrite() throws SQLException {
        connectionCount++;
        db = dbHelper.getWritableDatabase();
    }

    /**
     * Close connection to the database
     */
    public synchronized void close() {
        connectionCount--;
        if (connectionCount <= 0) {
            connectionCount = 0;
            db.close();
        }
    }

    /**
     * Inserts new section to the local database
     * @param section Section to insert
     * @return id of the inserted row or -1 if error happened
     */
    public long insertSection(Section section) {
        ContentValues values = new ContentValues();
        values.put("id", section.getId());
        values.put("ord", section.getOrder());
        values.put("name", section.getName());
        values.put("path", section.getPath());
        return db.insert("Section", null, values);
    }

    /**
     * Inserts new book to the local database
     * @param book Book to insert
     * @return id of the inserted row or -1 if error happened
     */
    public long insertBook(Book book) {
        ContentValues values = new ContentValues();
        updatePosition(book.getId(), new Position(-1));
        values.put("id", book.getId());
        values.put("ord", book.getOrder());
        values.put("name", book.getName());
        values.put("path", book.getPath());
        values.put("sectionID", book.getSectionId());
        values.put("name_cs", book.getNameCs());
        values.put("info", book.getInfo());
        values.put("enabled", book.isEnabled() ? 1 : 0);
        return db.insert("Book", null, values);
    }

    /**
     * Updates (or inserts if absent) book position to the local database
     * @param bookId   Book identifier
     * @param position Position to update (or insert)
     */
    public void updatePosition(int bookId, Position position) {
        if (position == null) {
            position = new Position(-1);
        }
        ContentValues values = new ContentValues();
        values.put("chapter", position.getChapterNumber());
        values.put("line", position.getLineNumber());
        values.put("character", position.getCharIndex());
        int rows = db.update("Position", values, String.format("bookID = %d", bookId), null);
        if (rows < 1) {
            values.put("bookID", bookId);
            db.insert("Position", null, values);
        }
    }

    /**
     * Inserts new chapter to the local database
     * @param chapter Chapter to insert
     * @return id of the inserted row or -1 if error happened
     */
    public long insertChapter(Chapter chapter) {
        ContentValues values = new ContentValues();
        values.put("id", chapter.getId());
        values.put("ord", chapter.getOrder());
        values.put("name", chapter.getName().length() > 0 ? chapter.getName() : "---");
        values.put("path", chapter.getPath());
        values.put("bookID", chapter.getBookId());
        values.put("name_cs", chapter.getNameCs());
        return db.insert("Chapter", null, values);
    }

    /**
     * Inserts new bookmark to the local database
     * @param bookmark Bookmark to insert
     * @return id of the inserted row or -1 if error happened
     */
    public long insertBookmark(Bookmark bookmark) {
        ContentValues values = new ContentValues();
        values.put("name", bookmark.getName());
        values.put("bookID", bookmark.getBookId());
        values.put("chapter", bookmark.getChapterNumber());
        values.put("line", bookmark.getLineNumber());
        values.put("character", bookmark.getCharIndex());
        values.put("groupID", bookmark.getGroupId());
        return db.insert("Bookmark", null, values);
    }

    /**
     * Inserts new bookmark group to the local database
     * @param group Bookmark group to insert
     * @return id of the inserted row or -1 if error happened
     */
    public long insertBmGroup(BmGroup group) {
        ContentValues values = new ContentValues();
        values.put("name", group.getName());
        return db.insert("BmGroup", null, values);
    }

    /**
     * Inserts new history entry to the local database
     * @param item Entry to insert
     * @return id of the inserted row or -1 if error happened
     */
    public long insertHistory(HistoryItem item) {
        ContentValues values = new ContentValues();
        values.put("name", item.getName());
        values.put("bookID", item.getBookId());
        return db.insert("History", null, values);
    }

    /**
     * Inserts new report to the local database
     * @param report Report to insert
     * @return id of the inserted row or -1 if error happened
     */
    public long insertReport(Report report) {
        ContentValues values = new ContentValues();
        values.put("word", report.getText());
        values.put("comment", report.getComment() + " ");
        values.put("chapterID", report.getChapterId());
        values.put("line", report.getLineNumber());
        values.put("character", report.getCharIndex());
        return db.insert("Report", null, values);
    }

    /**
     * Updates specified book in the local database
     * @param id      Book identifier
     * @param enabled defines whether book should be activated/deactivated
     * @return true if book was updated, false otherwise
     */
    public boolean updateBook(int id, boolean enabled) {
        ContentValues values = new ContentValues();
        values.put("enabled", enabled ? 1 : 0);
        return db.update("Book", values, String.format("id = %d", id), null) > 0;
    }

    /**
     * Deletes specified section from the local database
     * Cascading deletes all books
     * Cascading deletes all books chapters
     * @param id Section identifier
     */
    public void deleteSection(int id) {
        db.delete("Chapter", String.format("bookID IN (SELECT id FROM Book WHERE sectionId = %d)", id), null);
        db.delete("Book", String.format("sectionID = %d", id), null);
        db.delete("Section", String.format("id = %d", id), null);
    }

    /**
     * Deletes specified book from the local database
     * Cascading deletes all book chapters
     * @param id Book identifier
     */
    public void deleteBook(int id) {
        db.delete("Chapter", String.format("bookID = %d", id), null);
        db.delete("Position", String.format("bookID = %d", id), null);
        db.delete("Book", String.format("id = %d", id), null);
    }

    /**
     * Deletes all bookmarks from the local database
     */
    public void deleteBookmarks() {
        db.delete("Bookmark", null, null);
    }

    /**
     * Deletes all history data from the local database
     */
    public void deleteHistory() {
        db.delete("History", null, null);
    }

    /**
     * Deletes specified bookmark group from the local database
     * @param id Bookmark group identifier
     */
    public void deleteBmGroup(int id) {
        db.delete("BmGroup", String.format("id = %d", id), null);
    }

    /**
     * Deletes specified report from the local database
     * @param id Report identifier
     */
    public void deleteReport(int id) {
        db.delete("Report", String.format("id = %d", id), null);
    }

    /**
     * Checks whether specified section exists
     * @param id Section identifier
     * @return true if section exists, false otherwise
     */
    public boolean checkSectionExists(int id) {
        boolean result = true;
        String[] columns = {"COUNT(*)"};
        Cursor cursor = db.query("Section", columns, String.format("id = %d", id), null, null, null, null);
        if (cursor.moveToFirst()) { //At least one row present
            result = cursor.getInt(0) > 0;
        }
        cursor.close();
        return result;
    }

    /**
     * Checks whether specified section is empty
     * @param id Section identifier
     * @return true if section has no books, false otherwise
     */
    public boolean checkSectionEmpty(int id) {
        boolean result = true;
        String[] columns = {"COUNT(*)"};
        Cursor cursor = db.query("Book", columns, String.format("sectionID = %d", id), null, null, null, null);
        if (cursor.moveToFirst()) { //At least one row present
            result = cursor.getInt(0) == 0;
        }
        cursor.close();
        return result;
    }

    /**
     * Returns list of sections with list of books from local database
     * @return list of available sections
     */
    public List<Section> selectSectionsWithBooks() {
        List<Section> result = new ArrayList<Section>();
        String[] sectionColumns = {"id", "name", "path", "ord"};
        String[] bookColumns = {"id", "name", "path", "ord", "name_cs", "info", "enabled",
                "(SELECT COUNT(*) FROM Chapter WHERE Chapter.bookID = Book.id)"};

        Cursor sectionCursor = db.query("Section", sectionColumns, null, null, null, null, "ord");
        if (sectionCursor.moveToFirst()) { //At least one row present
            do {
                Section section = new Section();
                section.setId(sectionCursor.getInt(0));
                section.setName(sectionCursor.getString(1));
                section.setPath(sectionCursor.getString(2));
                section.setOrder(sectionCursor.getFloat(3));

                List<Book> books = new ArrayList<Book>();
                Cursor bookCursor = db.query("Book", bookColumns, String.format("sectionID = %d", section.getId()), null, null, null, "ord");
                if (bookCursor.moveToFirst()) { //At least one row present
                    do {
                        Book book = new Book();
                        book.setId(bookCursor.getInt(0));
                        book.setName(bookCursor.getString(1));
                        book.setPath(bookCursor.getString(2));
                        book.setOrder(bookCursor.getFloat(3));
                        book.setNameCs(bookCursor.getString(4));
                        book.setInfo(bookCursor.getString(5));
                        book.setEnabled(bookCursor.getInt(6) > 0);
                        book.setSize(bookCursor.getInt(7));
                        books.add(book);
                    } while (bookCursor.moveToNext());
                }
                bookCursor.close();

                section.setBooks(books);
                result.add(section);
            } while (sectionCursor.moveToNext());
        }
        sectionCursor.close();
        return result;
    }

    /**
     * Returns list of chapters from local database
     * @param bookId Book identifier
     * @return list of chapters from the specified book
     */
    public List<Chapter> selectBookChapters(int bookId) {
        List<Chapter> result = new ArrayList<Chapter>();
        String[] chapterColumns = {"id", "name", "path", "ord", "name_cs"};

        Cursor chapterCursor = db.query("Chapter", chapterColumns, String.format("bookID = %d", bookId), null, null, null, "ord");
        if (chapterCursor.moveToFirst()) { //At least one row present
            do {
                Chapter chapter = new Chapter();
                chapter.setId(chapterCursor.getInt(0));
                chapter.setName(chapterCursor.getString(1));
                chapter.setPath(chapterCursor.getString(2));
                chapter.setOrder(chapterCursor.getFloat(3));
                chapter.setNameCs(chapterCursor.getString(4));
                result.add(chapter);
            } while (chapterCursor.moveToNext());
        }
        chapterCursor.close();
        return result;
    }

    /**
     * Returns book with list of chapters from local database
     * @param bookId Section identifier
     * @return list of enabled books from the specified section
     */
    public Book selectBookWithChapters(int bookId) {
        Book result = null;
        String[] bookColumns = {"B.id", "B.name", "B.path", "B.ord", "B.name_cs", "S.id", "S.path"};

        Cursor bookCursor = db.query("Book as B inner join Section as S on B.sectionID = S.id", bookColumns, String.format("B.id = %d and B.enabled = 1", bookId), null,
                null, null, null);
        if (bookCursor.moveToFirst()) { //At least one row present
            result = new Book();
            result.setId(bookCursor.getInt(0));
            result.setName(bookCursor.getString(1));
            result.setPath(bookCursor.getString(2));
            result.setOrder(bookCursor.getFloat(3));
            result.setNameCs(bookCursor.getString(4));
            result.setSectionId(bookCursor.getInt(5));
            result.setSectionPath(bookCursor.getString(6));
            result.setChapters(selectBookChapters(result.getId()));
        }
        bookCursor.close();
        return result;
    }

    /**
     * Returns book position from local database
     * @param bookId Section identifier
     * @return list of enabled books from the specified section
     */
    public Position selectBookPosition(int bookId) {
        Position result = new Position(-1);
        String[] positionColumns = {"chapter", "line", "character"};
        Cursor positionCursor = db.query("Position", positionColumns, String.format("bookID = %d", bookId), null, null, null, null);
        if (positionCursor.moveToFirst()) { //At least one row present
            result.setChapterNumber(positionCursor.getInt(0));
            result.setLineNumber(positionCursor.getInt(1));
            result.setCharIndex(positionCursor.getInt(2));
        }
        positionCursor.close();
        return result;
    }

    /**
     * Select book which follows or precedes currently opened book
     * @param book           Current book
     * @param ascendingOrder true for searching for the next book, false for previous book
     * @return Next or previous book (id and name only)
     */
    public Book selectNeighbouringBook(Book book, boolean ascendingOrder) {
        String[] sectionColumns = {"id", "ord"};
        String[] bookColumns = {"id", "name", "ord"};

        boolean lookingForBooksInNextSections = false;
        Cursor sectionCursor = db.query("Section", sectionColumns, null, null, null, null, "ord" + (ascendingOrder ? " ASC" : " DESC"));
        try {
            if (sectionCursor.moveToFirst()) { //At least one row present
                do {
                    String order = (Math.round(Math.floor(sectionCursor.getFloat(1))) == 3 ? "name" : "ord") + (ascendingOrder ? " ASC" : " DESC");
                    //--- Check for first book in the next section ---
                    if (lookingForBooksInNextSections) {
                        Cursor bookCursor = db.query("Book", bookColumns, String.format("sectionID = %d", sectionCursor.getInt(0)), null, null, null, order);
                        try {
                            if (bookCursor.moveToFirst()) {
                                Book result = new Book();
                                result.setId(bookCursor.getInt(0));
                                result.setName(bookCursor.getString(1));
                                return result;
                            }
                        } finally {
                            bookCursor.close();
                        }
                        continue;
                    }
                    //--- Check whether needed section reached ---
                    if (sectionCursor.getInt(0) != book.getSectionId()) {
                        continue;
                    }
                    //--- Get all the books in current section ---
                    Cursor bookCursor = db.query("Book", bookColumns, String.format("sectionID = %d", sectionCursor.getInt(0)), null, null, null, order);
                    try {
                        if (bookCursor.moveToFirst()) { //At least one row present
                            do {
                                if (book.getId() == bookCursor.getInt(0)) {
                                    if (bookCursor.moveToNext()) { //Found book
                                        Book result = new Book();
                                        result.setId(bookCursor.getInt(0));
                                        result.setName(bookCursor.getString(1));
                                        return result;
                                    } else { //Need to search next sections
                                        lookingForBooksInNextSections = true;
                                        break;
                                    }
                                }
                            } while (bookCursor.moveToNext());
                        }
                    } finally {
                        bookCursor.close();
                    }
                } while (sectionCursor.moveToNext());
            }
        } finally {
            sectionCursor.close();
        }
        return null;
    }

    /**
     * Selects list of all bookmarks from the local database
     * @return List of bookmarks
     */
    public List<Bookmark> selectBookmarks() {
        List<Bookmark> result = new ArrayList<Bookmark>();
        String[] columns = {"name", "bookID", "chapter", "line", "character", "groupID"};
        Cursor cursor = db.query("Bookmark", columns, null, null, null, null, "id");
        if (cursor.moveToFirst()) { //At least one row present
            do {
                Bookmark bookmark = new Bookmark();
                bookmark.setName(cursor.getString(0));
                bookmark.setBookId(cursor.getInt(1));
                bookmark.setChapterNumber(cursor.getInt(2));
                bookmark.setLineNumber(cursor.getInt(3));
                bookmark.setCharIndex(cursor.getInt(4));
                bookmark.setGroupId(cursor.getInt(5));
                result.add(bookmark);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    /**
     * Selects list of all bookmark groups from the local database
     * @return List of bookmark groups
     */
    public List<BmGroup> selectBmGroups() {
        List<BmGroup> result = new ArrayList<BmGroup>();
        String[] columns = {"id", "name"};
        Cursor cursor = db.query("BmGroup", columns, null, null, null, null, "id");
        if (cursor.moveToFirst()) { //At least one row present
            do {
                BmGroup group = new BmGroup();
                group.setId(cursor.getInt(0));
                group.setName(cursor.getString(1));
                result.add(group);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    /**
     * Selects search history from the local database
     * @return List of history entries
     */
    public List<HistoryItem> selectHistory() {
        List<HistoryItem> result = new ArrayList<HistoryItem>();
        String[] columns = {"bookId", "name"};
        Cursor cursor = db.query("History", columns, null, null, null, null, "id");
        if (cursor.moveToFirst()) { //At least one row present
            do {
                result.add(new HistoryItem(cursor.getInt(0), cursor.getString(1)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    /**
     * Selects list of all reports from the local database
     * @return List of reports
     */
    public List<Report> selectReports() {
        List<Report> result = new ArrayList<Report>();
        String[] columns = {"id", "word", "comment", "chapterID", "line", "character"};
        Cursor cursor = db.query("Report", columns, null, null, null, null, null);
        if (cursor.moveToFirst()) { //At least one row present
            do {
                Report report = new Report(cursor.getInt(0));
                report.setText(cursor.getString(1));
                report.setComment(cursor.getString(2));
                report.setChapterId(cursor.getInt(3));
                report.setLineNumber(cursor.getInt(4));
                report.setCharIndex(cursor.getInt(5));
                result.add(report);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    /**
     * Returns number of books in local database
     * which differ from the books in remote database
     * @param remoteSections List of pre-fetched remote sections
     * @return Number of new or modified books
     */
    public int getNewModifiedBookCount(List<Section> remoteSections, SharedPreferences ignoreList) {
        int result = 0;
        List<Section> localSections = selectSectionsWithBooks();
        for (Section rs : remoteSections) {
            int lsIndex = -1;
            for (Section ls : localSections) {
                if (ls.getId() == rs.getId()) { //Found remote section among local sections
                    lsIndex = localSections.indexOf(ls);
                    break;
                }
            }
            if (lsIndex == -1) { //Remote section not found among local sections
                for (Book rb : rs.getBooks()) {
                    if (!ignoreList.contains(String.format("ign%d", rb.getId()))) { //Book is not ignored
                        result++;
                    }
                }
            } else { //Remote section is not brand new
                for (Book rb : rs.getBooks()) {
                    if (ignoreList.contains(String.format("ign%d", rb.getId()))) { //Book is ignored
                        continue; //Proceed to the next book
                    }
                    boolean found = false;
                    for (Book lb : localSections.get(lsIndex).getBooks()) {
                        if (rb.getId() == lb.getId() && rb.getSize() == lb.getSize() && Math.abs(rb.getOrder() - lb.getOrder()) < 0.0001f) { //Remote book found among local books
                            found = true;
                            break;
                        }
                    }
                    if (!found) { //Remote book is brand new or an update
                        result++;
                    }
                }
            }
        }
        return result;
    }
}
