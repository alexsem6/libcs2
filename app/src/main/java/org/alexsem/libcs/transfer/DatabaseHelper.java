package org.alexsem.libcs.transfer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

  public DatabaseHelper(Context context) {
    super(context, "library", null, 2);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL("CREATE TABLE Section  (id INTEGER PRIMARY KEY, name TEXT, path TEXT, ord REAL);");
    db.execSQL("CREATE TABLE Book     (id INTEGER PRIMARY KEY, name TEXT, path TEXT, ord REAL, sectionID INTEGER, name_cs TEXT, info TEXT, enabled INTEGER);");
    db.execSQL("CREATE TABLE Position (bookID INTEGER PRIMARY KEY, chapter INTEGER, line INTEGER, character INTEGER);");
    db.execSQL("CREATE TABLE Chapter  (id INTEGER PRIMARY KEY, name TEXT, path TEXT, ord REAL, bookID INTEGER, name_cs TEXT);");
    db.execSQL("CREATE TABLE Bookmark (id INTEGER PRIMARY KEY, name TEXT, bookID INTEGER, chapter INTEGER, line INTEGER, character INTEGER, groupID INTEGER default 1 NOT NULL);");
    db.execSQL("CREATE TABLE BmGroup  (id INTEGER PRIMARY KEY, name TEXT);");
    db.execSQL("CREATE TABLE History  (id INTEGER PRIMARY KEY, name TEXT, bookID INTEGER);");
    db.execSQL("CREATE TABLE Report   (id INTEGER PRIMARY KEY, word TEXT, comment TEXT, chapterID INTEGER, line INTEGER, character INTEGER);");

    db.execSQL("INSERT INTO BmGroup (id, name) VALUES (1, '')"); //Default bookmark group
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    boolean updated = false;
    if (oldVersion <= 1) { //Migrate from 1 to 2
      db.execSQL("ALTER TABLE Bookmark ADD groupID INTEGER default 1 NOT NULL;");
      db.execSQL("CREATE TABLE BmGroup  (id INTEGER PRIMARY KEY, name TEXT);");
      db.execSQL("INSERT INTO BmGroup (id, name) VALUES (1, '')"); //Default bookmark group
      updated = true;
    }
    if (!updated) { //Other cases
      db.execSQL("DROP TABLE IF EXISTS Report");
      db.execSQL("DROP TABLE IF EXISTS BmGroup");
      db.execSQL("DROP TABLE IF EXISTS Bookmark");
      db.execSQL("DROP TABLE IF EXISTS History");
      db.execSQL("DROP TABLE IF EXISTS Chapter");
      db.execSQL("DROP TABLE IF EXISTS Book");
      db.execSQL("DROP TABLE IF EXISTS Section");
      onCreate(db);
    }
  }

}
