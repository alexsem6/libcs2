package org.alexsem.libcs.transfer;

import android.util.Xml;

import org.alexsem.libcs.model.Book;
import org.alexsem.libcs.model.Chapter;
import org.alexsem.libcs.model.Section;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public abstract class FileOperations {
	private static final int BUFFER_SIZE = 1024;

	/**
	 * Performs attempt to import all books to the local database
	 * @param path Path to global book storage
	 * @param database DB into which to add data
	 * @return Number of successfully imported books
	 */
	public static int tryImport(File path, DatabaseAdapter database) {
		if (database == null) {
			return 0;
		}
		int result = 0;
		for (File file : path.listFiles()) {
			if (file.isDirectory()) { //(Probably section) directory found
				result += tryImportSection(file, database);
			}
		}
		return result;
	}

	/**
	 * Tries to import section to the local database
	 * @param path Path to section directory
	 * @param database DB into which to add data
	 * @return Number of successfully imported books
	 */
	private static int tryImportSection(File path, DatabaseAdapter database) {
		int result = 0;
		File info = new File(path, "db.xml");
		if (info.exists()) { //Found info file
			try {
				Section section = null;
				XmlPullParser xpp = XmlPullParserFactory.newInstance().newPullParser();
				xpp.setInput(new FileInputStream(info), "UTF-8");
				while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) { // Repeat until document end					
					if (xpp.getEventType() == XmlPullParser.START_TAG) { // Start of tag
						if (xpp.getName().equalsIgnoreCase("section")) { //Start of section
							section = new Section();
							section.setId(Integer.valueOf(xpp.getAttributeValue(null, "id")));
							section.setOrder(Float.valueOf(xpp.getAttributeValue(null, "order")));
							section.setPath(path.getName());
						} else if (xpp.getName().equalsIgnoreCase("name") && section != null) { //Name element
							section.setName(xpp.nextText());
						}
					}
					xpp.next();
				}
				if (database.insertSection(section) > -1) { //Section successfully inserted
					for (File file : path.listFiles()) {
						if (file.isDirectory() && tryImportBook(file, database, section.getId())) { //(Probably book) directory found && Book successfully imported 
							result++;
						} //if
					} //for
				} //if
			} catch (Exception ex) {
				ex.printStackTrace();
				return result;
			}
		}
		return result;
	}

	/**
	 * Tries to import book to the local database
	 * @param path Path to book directory
	 * @param database DB into which to add data
	 * @param sectionId ID of section this book belongs to
	 * @return true if book was successfully imported, false otherwise
	 */
	private static boolean tryImportBook(File path, DatabaseAdapter database, int sectionId) {
		File info = new File(path, "db.xml");
		if (info.exists()) { //Found info file
			try {
				Book book = null;
				List<Chapter> chapters = null;
				Chapter chapter = null;

				XmlPullParser xpp = XmlPullParserFactory.newInstance().newPullParser();
				xpp.setInput(new FileInputStream(info), "UTF-8");
				while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) { // Repeat until document end

					if (xpp.getEventType() == XmlPullParser.START_TAG) { // Start of tag

						if (xpp.getName().equalsIgnoreCase("book")) { //Start of section
							book = new Book();
							book.setId(Integer.valueOf(xpp.getAttributeValue(null, "id")));
							book.setOrder(Float.valueOf(xpp.getAttributeValue(null, "order")));
							book.setSectionId(sectionId);
							book.setPath(path.getName());
							book.setEnabled(true);
						} else if (xpp.getName().equalsIgnoreCase("name") && book != null) { //Name element
							if (chapter != null) { //Chapter name
								chapter.setName(xpp.nextText());
							} else { //Book name
								book.setName(xpp.nextText());
							}
						} else if ((xpp.getName().equalsIgnoreCase("name_cs") || xpp.getName().equalsIgnoreCase("name__cs")) && book != null) { //Name_cs element
							if (chapter != null) { //Chapter name_cs
								chapter.setNameCs(xpp.nextText());
							} else { //Book name_cs
								book.setNameCs(xpp.nextText());
							}
						} else if (xpp.getName().equalsIgnoreCase("info") && book != null) { //Name_cs element
							book.setInfo(xpp.nextText());
						} else if (xpp.getName().equalsIgnoreCase("chapters")) { //Start of chapters array
							chapters = new ArrayList<Chapter>();
						} else if (xpp.getName().equalsIgnoreCase("chapter")) { //Start of chapter
							chapter = new Chapter();
							chapter.setId(Integer.valueOf(xpp.getAttributeValue(null, "id")));
							chapter.setOrder(Float.valueOf(xpp.getAttributeValue(null, "order")));
						} else if (xpp.getName().equalsIgnoreCase("path") && chapter != null) { //Path element
							chapter.setPath(xpp.nextText());
						}

					} else if (xpp.getEventType() == XmlPullParser.END_TAG) { //End of tag

						if (xpp.getName().equalsIgnoreCase("chapter") && chapters != null) { //End of chapter
							chapters.add(chapter);
							chapter = null;
						} else if (xpp.getName().equalsIgnoreCase("chapters") && book != null) { //End of chapters array
							book.setChapters(chapters);
							chapters = null;
							chapter = null;
						}

					}
					xpp.next();
				}

				if (database.insertBook(book) > -1) { //Book successfully inserted
					for (Chapter chap : book.getChapters()) {
						chap.setBookId(book.getId());
						database.insertChapter(chap);
					}
					return true;
				} //if
			} catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}
		} //if
		return false;
	}

	/**
	 * Checks whether all book chapters are present on SD card
	 * @param path Path to the book directory on SD card
	 * @param database DB to get book information from
	 * @param bookID Book identifier
	 * @return true if book can be enabled, false otherwise
	 */
	public static boolean tryEnableBook(File path, DatabaseAdapter database, int bookID) {
		if (database == null) {
			return false;
		}
		for (Chapter chapter : database.selectBookChapters(bookID)) {
			if (!new File(path, chapter.getPath()).exists()) { //Could not find chapter file
				return false;
			}
		}
		return true;
	}

	/**
	 * Deletes directory recursively
	 * @param path Directory to delete
	 * @throws IOException in case deletion failed
	 */
	public static void deleteDir(File path) throws IOException {
		if (!path.exists()) {
			return;
		}
		if (path.isDirectory()) {
			for (File child : path.listFiles()) {
				deleteDir(child);
			}
		}
		// The directory is now empty so delete it
		if (!path.delete()) {
			throw new IOException("Could not delete " + path.getAbsolutePath());
		}
	}

	/**
	 * Creates new sections directory and adds "db.xml" file to it
	 * @param parentDir Directory in which to create section directory
	 * @param section Section to save
	 * @throws IOException in case file operations failed
	 */
	public static void createSectionDir(File parentDir, Section section) throws IOException {
		File path = new File(parentDir, section.getPath());
		deleteDir(path);
		path.mkdirs();

		FileOutputStream output = new FileOutputStream(new File(path, "db.xml"));
		XmlSerializer xml = Xml.newSerializer();
		xml.setOutput(output, "UTF-8");
		xml.startTag(null, "section");
		xml.attribute(null, "id", String.valueOf(section.getId()));
		xml.attribute(null, "order", String.valueOf(section.getOrder()));
		xml.startTag(null, "name");
		xml.text(section.getName());
		xml.endTag(null, "name");
		xml.startTag(null, "path");
		xml.text(section.getPath());
		xml.endTag(null, "path");
		xml.endTag(null, "section");
		xml.endDocument();
		xml.flush();
		output.close();
	}

	/**
	 * Creates new sections book and adds "db.xml" file to it
	 * @param parentDir Directory in which to create book directory
	 * @param book Book to save
	 * @throws IOException in case file operations failed
	 */
	public static void createBookDir(File parentDir, Book book) throws IOException {
		File path = new File(parentDir, book.getPath());
		deleteDir(path);
		path.mkdirs();
		
		FileOutputStream output = new FileOutputStream(new File(path, "db.xml"));
		XmlSerializer xml = Xml.newSerializer();
		xml.setOutput(output, "UTF-8");
		xml.startTag(null, "book");
		xml.attribute(null, "id", String.valueOf(book.getId()));
		xml.attribute(null, "order", String.valueOf(book.getOrder()));
		xml.startTag(null, "name");
		xml.text(book.getName());
		xml.endTag(null, "name");
		xml.startTag(null, "path");
		xml.text(book.getPath());
		xml.endTag(null, "path");
		xml.startTag(null, "name_cs");
		xml.text(book.getNameCs());
		xml.endTag(null, "name_cs");
		xml.startTag(null, "info");
		xml.text(book.getInfo());
		xml.endTag(null, "info");

		xml.startTag(null, "chapters");
		for (Chapter chapter: book.getChapters()) {
			xml.startTag(null, "chapter");
			xml.attribute(null, "id", String.valueOf(chapter.getId()));
			xml.attribute(null, "order", String.valueOf(chapter.getOrder()));
			xml.startTag(null, "name");
			xml.text(chapter.getName());
			xml.endTag(null, "name");
			xml.startTag(null, "path");
			xml.text(chapter.getPath());
			xml.endTag(null, "path");
			xml.startTag(null, "name_cs");
			xml.text(chapter.getNameCs());
			xml.endTag(null, "name_cs");
			xml.endTag(null, "chapter");
		}
		xml.endTag(null, "chapters");
		
		xml.endTag(null, "book");
		xml.endDocument();
		xml.flush();
		output.close();
	}

	/**
	 * Saves contents of InputStream to the specified file
	 * @param input Input stream to load file contents from
	 * @param target Target file to save contents to
	 * @throws IOException in case file operations failed
	 */
	public static void saveFile(InputStream input, File target) throws IOException {
		BufferedInputStream is = new BufferedInputStream(input);
		try {
			byte[] buffer = new byte[BUFFER_SIZE];
			FileOutputStream output = new FileOutputStream(target);
			int length;
			while ((length = is.read(buffer)) != -1) { //Read until the end of file
				output.write(buffer, 0, length);
			}
			output.close();
		} finally {
			is.close();
		}
	}

	/**
	 * Extracts contents of ZIP file to the specified location
	 * @param source File to copy data from
	 * @param path Path to folder
	 * @throws IOException in case file operations failed
	 */
	public static void extractZip(File source, File path) throws IOException {
		ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(source)));
		byte[] buffer = new byte[BUFFER_SIZE];
		try {
			ZipEntry entry;
			while ((entry = zis.getNextEntry()) != null) {
				if (entry.isDirectory()) { //Entry is a directory
					(new File(path, entry.getName())).mkdirs();
				} else { //Entry is a regular file
					File file = new File(path, entry.getName());
					if (!file.getParentFile().exists()) { //Parent directory does not exist
						file.getParentFile().mkdirs();
					}
					FileOutputStream output = new FileOutputStream(new File(path, entry.getName()));
					int length;
					while ((length = zis.read(buffer)) != -1) { //Read until the end of entry
						output.write(buffer, 0, length);
					}
					output.close();
				}
			}
		} finally {
			zis.close();
		}
	}

	/**
	 * Loads book contents from SD card
	 * @param sectionPath Path to section directory
	 * @param book Book to load
	 * @return Array of chapters with data
	 * @throws IOException in case file operations failed
	 */
	@SuppressWarnings("rawtypes")
	public static List[] loadBookContent(File sectionPath, Book book, OnLoadingCanceledListener cancel) throws IOException {
		List[] result = new ArrayList[book.getChapters().size()];
		File bookPath = new File(sectionPath, book.getPath());
		String line;
		for (int i = 0; i < result.length && !cancel.isCanceled(); i++) {
			List<String> data = new ArrayList<String>();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(bookPath, book.getChapters().get(i).getPath())),
					"Windows-1251"), BUFFER_SIZE);
			while ((line = reader.readLine()) != null && !cancel.isCanceled()) {
				data.add("  " + line);
			}
			reader.close();
            if (data.size() == 0) {
                data.add("   ");
            }
			result[i] = data;
		}
		return result;
	}

    //----------------------------------------------------------------------------------------------

    /**
     * Interface which helps to define whether lengthy file operations are interrupted
     */
    public interface OnLoadingCanceledListener {
        public boolean isCanceled();
    }

}
