package org.alexsem.libcs.transfer;

import org.alexsem.libcs.model.Book;
import org.alexsem.libcs.model.Chapter;
import org.alexsem.libcs.model.Report;
import org.alexsem.libcs.model.Section;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class ServerConnector {

    private static final String GLOBAL_PATH = "http://www.alexsem.org/libcs/sync.php";

    /**
     * Generates URI for server interaction
     * @param action Action to execute
     * @param id     optional object identifier
     * @return Generated URI
     */
    private static URI constructURI(String action, Integer id) {
        StringBuilder sb = new StringBuilder(GLOBAL_PATH);
        sb.append("?action=").append(action);
        if (id != null) {
            sb.append("&id=").append(id);
        }
        return URI.create(sb.toString());
    }

    /**
     * Generates URI for server interaction
     * @return Generated URI
     */
    private static URI constructHelpURI() {
        return URI.create(GLOBAL_PATH + "?action=help");
    }

    /**
     * Generates URI for server interaction
     * @param name Typeface name
     * @return Generated URI
     */
    private static URI constructFontURI(String name) {
        return URI.create(String.format("%s?action=font&name=%s", GLOBAL_PATH, name));
    }

    /**
     * Generates URI for report saving request
     * @param word      Word to send
     * @param comment   Supplementary comment
     * @param chapterID Unique identifier of chapter
     * @param position  Line number (or -1 for title)
     * @param index     Character index
     * @return Generated URI
     * @throws UnsupportedEncodingException (should never throw it)
     */
    private static URI constructReportURI(String word, String comment, int chapterID, int position, int index) throws UnsupportedEncodingException {
        return URI.create(String.format(Locale.getDefault(), "%s?action=report&word=%s&comment=%s&chapterID=%d&position=%d&index=%d",
                GLOBAL_PATH, URLEncoder.encode(word, "UTF-8"), URLEncoder.encode(comment, "UTF-8"), chapterID, position, index));
    }

    /**
     * Requests all sections and books available on server
     * @return List of sections (with books) from the server
     * @throws IOException            in case of interaction problems
     * @throws XmlPullParserException in case XML parsing fails
     */
    public static List<Section> getAvailableSections() throws IOException, XmlPullParserException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(constructURI("sync", null));
        HttpResponse httpResponse = client.execute(post);

        List<Section> result = new ArrayList<Section>();
        Section section = null;
        List<Book> books = null;
        Book book = null;

        XmlPullParser xpp = XmlPullParserFactory.newInstance().newPullParser();
        xpp.setInput(httpResponse.getEntity().getContent(), "UTF-8");
        while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) { // Repeat until document end

            if (xpp.getEventType() == XmlPullParser.START_TAG) { // Start of tag

                if (xpp.getName().equalsIgnoreCase("section")) { //Start of section
                    section = new Section();
                    section.setId(Integer.valueOf(xpp.getAttributeValue(null, "id")));
                    section.setOrder(Float.valueOf(xpp.getAttributeValue(null, "order")));
                } else if (xpp.getName().equalsIgnoreCase("name") && section != null) { //Name element
                    if (book != null) { //Book name
                        book.setName(xpp.nextText());
                    } else { //Section name
                        section.setName(xpp.nextText());
                    }
                } else if (xpp.getName().equalsIgnoreCase("path") && section != null) { //Path element
                    if (book != null) { //Book path
                        book.setPath(xpp.nextText());
                    } else { //Section path
                        section.setPath(xpp.nextText());
                    }
                } else if (xpp.getName().equalsIgnoreCase("books")) { //Start of books array
                    books = new ArrayList<Book>();
                } else if (xpp.getName().equalsIgnoreCase("book")) { //Start of book
                    book = new Book();
                    book.setId(Integer.valueOf(xpp.getAttributeValue(null, "id")));
                    book.setOrder(Float.valueOf(xpp.getAttributeValue(null, "order")));
                } else if (xpp.getName().equalsIgnoreCase("name_cs") && book != null) { //Name_cs element
                    book.setNameCs(xpp.nextText());
                } else if (xpp.getName().equalsIgnoreCase("size") && book != null) { //Size element
                    book.setSize(Integer.valueOf(xpp.nextText()));
                } else if (xpp.getName().equalsIgnoreCase("kb") && book != null) { //Kb element
                    book.setKb(Integer.valueOf(xpp.nextText()));
                }

            } else if (xpp.getEventType() == XmlPullParser.END_TAG) { //End of tag

                if (xpp.getName().equalsIgnoreCase("book") && books != null) { //End of book
                    books.add(book);
                    book = null;
                } else if (xpp.getName().equalsIgnoreCase("books") && section != null) { //End of books array
                    section.setBooks(books);
                    books = null;
                } else if (xpp.getName().equalsIgnoreCase("section")) { //End of section
                    result.add(section);
                    section = null;
                }

            }
            xpp.next();
        }
        return result;
    }

    /**
     * Requests specific section info from the server
     * @param id Section identifier
     * @return Section object
     * @throws IOException            in case of interaction problems
     * @throws XmlPullParserException in case XML parsing fails
     */
    public static Section getSectionInfo(int id) throws IOException, XmlPullParserException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(constructURI("section", id));
        HttpResponse httpResponse = client.execute(post);

        Section result = null;

        XmlPullParser xpp = XmlPullParserFactory.newInstance().newPullParser();
        xpp.setInput(httpResponse.getEntity().getContent(), "UTF-8");
        while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) { // Repeat until document end
            if (xpp.getEventType() == XmlPullParser.START_TAG) { // Start of tag
                if (xpp.getName().equalsIgnoreCase("section")) { //Start of section
                    result = new Section();
                    result.setId(Integer.valueOf(xpp.getAttributeValue(null, "id")));
                    result.setOrder(Float.valueOf(xpp.getAttributeValue(null, "order")));
                } else if (xpp.getName().equalsIgnoreCase("name") && result != null) { //Name element
                    result.setName(xpp.nextText());
                } else if (xpp.getName().equalsIgnoreCase("path") && result != null) { //Path element
                    result.setPath(xpp.nextText());
                }
            }
            xpp.next();
        }
        return result;
    }

    /**
     * Requests specific book info from the server
     * @param id Book identifier
     * @return Book object
     * @throws IOException            in case of interaction problems
     * @throws XmlPullParserException in case XML parsing fails
     */
    public static Book getBookInfo(int id) throws IOException, XmlPullParserException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(constructURI("book", id));
        HttpResponse httpResponse = client.execute(post);

        Book result = null;
        List<Chapter> chapters = null;
        Chapter chapter = null;

        XmlPullParser xpp = XmlPullParserFactory.newInstance().newPullParser();
        xpp.setInput(httpResponse.getEntity().getContent(), "UTF-8");
        while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) { // Repeat until document end

            if (xpp.getEventType() == XmlPullParser.START_TAG) { // Start of tag

                if (xpp.getName().equalsIgnoreCase("book")) { //Start of section
                    result = new Book();
                    result.setId(Integer.valueOf(xpp.getAttributeValue(null, "id")));
                    result.setOrder(Float.valueOf(xpp.getAttributeValue(null, "order")));
                } else if (xpp.getName().equalsIgnoreCase("name") && result != null) { //Name element
                    if (chapter != null) { //Chapter name
                        chapter.setName(xpp.nextText());
                    } else { //Book name
                        result.setName(xpp.nextText());
                    }
                } else if (xpp.getName().equalsIgnoreCase("path") && result != null) { //Path element
                    if (chapter != null) { //Chapter path
                        chapter.setPath(xpp.nextText());
                    } else { //Book path
                        result.setPath(xpp.nextText());
                    }
                } else if (xpp.getName().equalsIgnoreCase("name_cs") && result != null) { //Name_cs element
                    if (chapter != null) { //Chapter name_cs
                        chapter.setNameCs(xpp.nextText());
                    } else { //Book name_cs
                        result.setNameCs(xpp.nextText());
                    }
                } else if (xpp.getName().equalsIgnoreCase("info") && result != null) { //Info element
                    result.setInfo(xpp.nextText());
                } else if (xpp.getName().equalsIgnoreCase("chapters")) { //Start of chapters array
                    chapters = new ArrayList<Chapter>();
                } else if (xpp.getName().equalsIgnoreCase("chapter")) { //Start of chapter
                    chapter = new Chapter();
                    chapter.setId(Integer.valueOf(xpp.getAttributeValue(null, "id")));
                    chapter.setOrder(Float.valueOf(xpp.getAttributeValue(null, "order")));
                }

            } else if (xpp.getEventType() == XmlPullParser.END_TAG) { //End of tag

                if (xpp.getName().equalsIgnoreCase("chapter") && chapters != null) { //End of chapter
                    chapters.add(chapter);
                    chapter = null;
                } else if (xpp.getName().equalsIgnoreCase("chapters") && result != null) { //End of chapters array
                    result.setChapters(chapters);
                    chapters = null;
                    chapter = null;
                }

            }
            xpp.next();
        }
        return result;
    }

    /**
     * Requests specific book contents as ZIP file
     * @param id Book identifier
     * @return InputStream
     * @throws IOException in case of interaction problems
     */
    public static InputStream loadBookZip(int id) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(constructURI("zip", id));
        HttpResponse httpResponse = client.execute(post);
        return httpResponse.getEntity().getContent();
    }

    /**
     * Requests help contents as ZIP file
     * @return InputStream Stream to read response from
     * @throws IOException in case of interaction problems
     */
    public static InputStream getHelpZip() throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(constructHelpURI());
        HttpResponse httpResponse = client.execute(post);
        return httpResponse.getEntity().getContent();
    }

    /**
     * Sends report about missing or invalid word
     * @param report Report to send
     * @throws IOException in case of interaction problems
     */
    public static void saveReport(Report report) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(constructReportURI(report.getText(), report.getComment(), report.getChapterId(), report.getLineNumber(),
                report.getCharIndex()));
        client.execute(post);
    }

    /**
     * Loads list of all available fonts
     * @throws IOException in case of interaction problems
     */
    public static List<String> getAvailableFonts() throws IOException {
        List<String> result = new ArrayList<String>();
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(constructURI("fonts", null));
        HttpResponse httpResponse = client.execute(post);
        BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
        } finally {
            reader.close();
        }
        return result;
    }

    /**
     * Requests specified font file contents as ZIP file
     * @param name Font name
     * @return InputStream Stream to read response from
     * @throws IOException in case of interaction problems
     */
    public static InputStream loadFontFile(String name) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(constructFontURI(name));
        HttpResponse httpResponse = client.execute(post);
        return httpResponse.getEntity().getContent();
    }

}
