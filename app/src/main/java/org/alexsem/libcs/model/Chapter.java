package org.alexsem.libcs.model;

public class Chapter extends Item {

	private int bookId;
	private String nameCs = "";

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getNameCs() {
		return nameCs;
	}

	public void setNameCs(String nameCs) {
		this.nameCs = nameCs;
	}

	@Override
	public String toString() {
		return getName().length() > 0 ? getName() : "----";
	}

}
