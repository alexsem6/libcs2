package org.alexsem.libcs.model;

import java.util.List;

public class Book extends Item {

	private int sectionId;
	private String nameCs = "";
	private String info = "";
	private boolean enabled;

	private int size;
	private int kb;

	private List<Chapter> chapters;

	private String sectionPath = null;

	public int getSectionId() {
		return sectionId;
	}

	public void setSectionId(int sectionId) {
		this.sectionId = sectionId;
	}

	public String getNameCs() {
		return nameCs;
	}

	public void setNameCs(String nameCs) {
		this.nameCs = nameCs;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getKb() {
		return kb;
	}

	public void setKb(int kb) {
		this.kb = kb;
	}

	public List<Chapter> getChapters() {
		return chapters;
	}

	public void setChapters(List<Chapter> chapters) {
		this.chapters = chapters;
	}

	public String getSectionPath() {
		return sectionPath;
	}

	public void setSectionPath(String sectionPath) {
		this.sectionPath = sectionPath;
	}

}
