package org.alexsem.libcs.model;

public class OptionsMain {

  public int orientation;
  public boolean animation;
  public boolean immersive;
  public boolean neighbours;

  public int colPort;
  public int colLand;

  public int hyphenateMode;
  public boolean fillLine;

  public int fc_back_color_day;
  public int fc_back_color_night;
  public int fc_text_color_day;
  public int fc_text_color_night;
  public int fc_text_color_cinnabar;
  public int fc_text_color_link;
  public int fc_other_color_service;

  public final int fc_text_color_selected = 0xffffa020;

  public final int fc_text_color_dict = 0xffc080ff;
  public final int fc_text_color_error = 0xffff8080;

  public int fc_back_color() {
    return daytime ? fc_back_color_day : fc_back_color_night;
  }

  public int fc_text_color() {
    return daytime ? fc_text_color_day : fc_text_color_night;
  }

  public String fc_text_face;
  public float fc_text_size;
  public float fc_text_spacing;

  public boolean everyRun;
  public int notifyRate;

  public boolean daytime = true;

  public final int historySize = 20;

}
