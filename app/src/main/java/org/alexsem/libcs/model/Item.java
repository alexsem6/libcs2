package org.alexsem.libcs.model;


public class Item implements Comparable<Item> {
	private int id;
	private float order = 0.00f;
	private String name = "";
	private String path = "";

	private int tag = 0;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getOrder() {
		return order;
	}

	public void setOrder(float order) {
		this.order = order;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public boolean equals(Object o) {
		return this.id == ((Item) o).id;
	}

	@Override
	public int compareTo(Item another) {
		return Float.compare(order, another.order);
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	@Override
	public String toString() {
		return name;
	}

}
