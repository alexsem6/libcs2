package org.alexsem.libcs.model;

public class HistoryItem {
	private int bookId;
	private String name;

	public HistoryItem(int bookId, String name) {
		this.bookId = bookId;
		this.name = name;
	}

	public int getBookId() {
		return bookId;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		return this.bookId == ((HistoryItem) o).bookId;
	}

}
