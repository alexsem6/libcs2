package org.alexsem.libcs.model;

import java.util.List;

public class Section extends Item {

	private List<Book> books;

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

}
