package org.alexsem.libcs.model;

public class Bookmark {

  private String name;
  private int bookId;
  private int chapterNumber;
  private int lineNumber;
  private int charIndex;
  private int groupId;

  public Bookmark() {
  }

  public Bookmark(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getBookId() {
    return bookId;
  }

  public void setBookId(int bookId) {
    this.bookId = bookId;
  }

  public int getChapterNumber() {
    return chapterNumber;
  }

  public void setChapterNumber(int chapterNumber) {
    this.chapterNumber = chapterNumber;
  }

  public int getLineNumber() {
    return lineNumber;
  }

  public void setLineNumber(int lineNumber) {
    this.lineNumber = lineNumber;
  }

  public int getCharIndex() {
    return charIndex;
  }

  public void setCharIndex(int charIndex) {
    this.charIndex = charIndex;
  }

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }
}
