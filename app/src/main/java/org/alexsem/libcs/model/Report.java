package org.alexsem.libcs.model;

public class Report {
	private int id = -1;
	private String text;
	private String comment;
	private int chapterId;
	private int lineNumber;
	private int charIndex;

	public Report(int id) {
		this.id = id;
	}

	public Report(String text, String comment, int chapterId, int lineNumber, int charIndex) {
		this.text = text;
		this.comment = comment;
		this.chapterId = chapterId;
		this.lineNumber = lineNumber;
		this.charIndex = charIndex;
	}

	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getChapterId() {
		return chapterId;
	}

	public void setChapterId(int chapterId) {
		this.chapterId = chapterId;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public int getCharIndex() {
		return charIndex;
	}

	public void setCharIndex(int charIndex) {
		this.charIndex = charIndex;
	}

}
