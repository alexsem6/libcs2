package org.alexsem.libcs.model;

import java.util.HashMap;
import java.util.Map;

public class OptionsActions {

    public Map<ReaderZone, QuickAction> actionsShort = new HashMap<ReaderZone, QuickAction>();
    public Map<ReaderZone, QuickAction> actionsLong = new HashMap<ReaderZone, QuickAction>();
    public Map<Boolean, QuickAction> actionsVolume = new HashMap<Boolean, QuickAction>();
}
