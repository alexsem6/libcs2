package org.alexsem.libcs.model;

/**
 * Class for storing position of text on page
 */
public class Position {
	private int chapterNumber;
	private int lineNumber;
	private int charIndex;

	public Position(int chapterNumber) {
		this.chapterNumber = chapterNumber;
		this.lineNumber = -1;
		this.charIndex = 0;
	}

	public Position(int chapterNumber, int lineNumber, int charIndex) {
		this.chapterNumber = chapterNumber;
		this.lineNumber = lineNumber;
		this.charIndex = charIndex;
	}

	public Position(Position copy) {
		this.chapterNumber = copy.chapterNumber;
		this.lineNumber = copy.lineNumber;
		this.charIndex = copy.charIndex;
	}

	public int getChapterNumber() {
		return chapterNumber;
	}

	public void setChapterNumber(int chapterNumber) {
		this.chapterNumber = chapterNumber;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public int getCharIndex() {
		return charIndex;
	}

	public void setCharIndex(int charIndex) {
		this.charIndex = charIndex;
	}

	/**
	 * Checks whether current position matches specified numbers
	 * @param chapterNumber Chapter number to check
	 * @param lineNumber Line number to check
	 * @param charIndex Character index to check
	 * @return true if check is successful, false otherwise
	 */
	public boolean checkPosition(int chapterNumber, int lineNumber, int charIndex) {
		return this.chapterNumber == chapterNumber && this.lineNumber == lineNumber && this.charIndex == charIndex;
	}

}
