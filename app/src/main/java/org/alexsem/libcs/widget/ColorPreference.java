package org.alexsem.libcs.widget;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.alexsem.libcs.activity.R;

public class ColorPreference extends Preference {
    private ImageView preview;
    private int color = -1;

    public ColorPreference(Context context) {
        super(context);
    }

    public ColorPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ColorPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        RelativeLayout layout = (RelativeLayout) ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.prefs_color, parent, false);
        this.preview = (ImageView) layout.findViewById(R.id.color);
        return layout;
    }

    @Override
    protected void onBindView(View view) {
        if (preview != null) {
            ((GradientDrawable) preview.getBackground()).setColor(color);
        }
        if (view != null) {
            super.onBindView(view);
        }
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        onBindView(null);
    }

}
