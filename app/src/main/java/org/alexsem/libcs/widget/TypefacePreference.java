package org.alexsem.libcs.widget;

import android.content.Context;
import android.os.Bundle;
import android.preference.ListPreference;
import android.util.AttributeSet;

public class TypefacePreference extends ListPreference {

    public TypefacePreference(Context context) {
        super(context);
    }

    public TypefacePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void showDialog(Bundle state) {
        //Do nothing
    }
}
