package org.alexsem.libcs.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import org.alexsem.libcs.activity.R;

import java.io.File;

/**
 * Class which is used for picking font size
 * @author Semeniuk A.D.
 */
public class FontSizePickerDialog extends AlertDialog implements OnSeekBarChangeListener, DialogInterface.OnClickListener {

    /**
     * Listener for font size picker event
     */
    public interface OnFontSizePickerListener {
        public void onFontSizePicked(int size);
    }

    private int mSize;
    private int mMinSize;
    private int mMaxSize;
    private OnFontSizePickerListener mListener;

    private SeekBar sbSize;
    private EditText etSize;
    private TextView mPreviewText;
    private Resources resources;

    public FontSizePickerDialog(Context context, int valueCurrent, int valueMin, int valueMax, OnFontSizePickerListener listener) {
        super(context);
        this.mSize = valueCurrent;
        this.mMinSize = valueMin;
        this.mMaxSize = valueMax;
        this.mListener = listener;

        this.resources = context.getResources();
        setTitle(resources.getText(R.string.picker_fontsize_title));
        setButton(BUTTON_POSITIVE, resources.getText(R.string.dialog_ok), this);
        setButton(BUTTON_NEGATIVE, resources.getText(R.string.dialog_cancel), this);

        View root = LayoutInflater.from(context).inflate(R.layout.picker_fontsize, null);
        mPreviewText = (TextView) root.findViewById(R.id.fontsize_picker_preview);
        try {
            File folder = new File(new File(context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getString("SDpath", "")), context.getString(R.string.typeface_folder));
            File file = new File(folder, PreferenceManager.getDefaultSharedPreferences(context).getString("fc_text_face", "_") + ".ttf");
            mPreviewText.setTypeface(Typeface.createFromFile(file));
        } catch (Exception ex) {
            mPreviewText.setTypeface(Typeface.createFromAsset(context.getAssets(), context.getString(R.string.typeface_regular_asset)));
        }
        setView(root);

        sbSize = (SeekBar) root.findViewById(R.id.fontsize_picker_seek);
        sbSize.setMax(valueMax - mMinSize);
        sbSize.setProgress(valueCurrent - mMinSize);
        sbSize.setOnSeekBarChangeListener(this);

        etSize = (EditText) root.findViewById(R.id.fontsize_picker_edit);
        etSize.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int size;
                try {
                    size = Integer.valueOf(etSize.getText().toString());
                } catch (NumberFormatException ex) {
                    size = mMinSize;
                }
                if (size < mMinSize) {
                    size = mMinSize;
                }
                if (size > mMaxSize) {
                    etSize.setText(String.valueOf(mMaxSize));
                    return;
                }
                mSize = size;
                sbSize.setProgress(mSize - mMinSize);
                updatePreview();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etSize.setText(String.valueOf(mSize));

        updatePreview();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            mListener.onFontSizePicked(mSize);
        }
        dismiss();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            mSize = progress + mMinSize;
            etSize.setText(Integer.toString(mSize));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void updatePreview() {
        mPreviewText.setTextSize(TypedValue.COMPLEX_UNIT_SP, this.mSize);
    }

}