package org.alexsem.libcs.widget;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.alexsem.libcs.adapter.TypefacePickerAdapter;
import org.alexsem.libcs.activity.R;
import org.alexsem.libcs.transfer.FileOperations;
import org.alexsem.libcs.transfer.ServerConnector;

import java.io.File;
import java.util.List;

public class TypefacePickerDialog extends AlertDialog implements OnClickListener {

  private final File TYPEFACE_FOLDER;

  private Context mContext;
  private OnTypefacePickerListener mListener;
  private String mTypeface;
  private TextView mPreviewText;
  private TypefacePickerAdapter mAdapter;

  public TypefacePickerDialog(Context context, String typeface, OnTypefacePickerListener listener) {
    super(context);

    // ---- Field members ----
    this.mContext = context;
    this.mListener = listener;
    this.mTypeface = typeface;

    // ---- Static data ----
    File storagePath = new File(context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getString("SDpath", ""));
    TYPEFACE_FOLDER = new File(storagePath, context.getString(R.string.typeface_folder));
    setTitle(context.getString(R.string.picker_typeface_title));
    setButton(BUTTON_POSITIVE, context.getString(R.string.dialog_ok), this);
    setButton(BUTTON_NEGATIVE, context.getString(R.string.dialog_cancel), this);

    // ---- Typeface presence checks ----
    if (!TYPEFACE_FOLDER.exists()) { //Directory is missing
      TYPEFACE_FOLDER.mkdirs();
    }
    File file = new File(TYPEFACE_FOLDER, context.getString(R.string.typeface_default) + ".ttf");
    if (!file.exists()) { //Default typeface not present in fonts folder
      try {
        FileOperations.saveFile(context.getAssets().open(context.getString(R.string.typeface_regular_asset)), file);
      } catch (Exception ex) { //Writing failed
        new AlertDialog.Builder(context).setTitle(R.string.dialog_error).setIcon(android.R.drawable.ic_dialog_alert)
            .setMessage(R.string.picker_typeface_nosd).setPositiveButton(R.string.dialog_close, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            TypefacePickerDialog.this.dismiss();
          }
        }).setCancelable(false).show();
        return;
      }
    }

    // ---- View root ----
    LayoutInflater inflater = LayoutInflater.from(context);
    View root = inflater.inflate(R.layout.picker_typeface, null);
    setView(root);

    // ---- Preview field ----
    mPreviewText = (TextView) root.findViewById(R.id.picker_typeface_preview);
    SpannableString ss = new SpannableString(mPreviewText.getText());
    ss.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.cinnabar)), 0, 1, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
    mPreviewText.setText(ss);

    // ---- List and adapter ----
    mAdapter = new TypefacePickerAdapter(context, TYPEFACE_FOLDER.list(), typeface);
    ListView list = (ListView) root.findViewById(R.id.picker_typeface_list);
    list.addFooterView(inflater.inflate(R.layout.picker_typeface_footer, null));
    list.setAdapter(mAdapter);
    list.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
        if (position == mAdapter.getCount()) { //Footer clicked
          new FontListLoaderTask(mContext).execute();
        } else { //Regular item clicked
          if (mAdapter.isItemRemote(position)) { //Remote font
            new FontLoaderTask(mContext, position).execute(mAdapter.getItem(position));
          } else { //Regular (installed) typeface
            String item = mAdapter.getItem(position);
            mAdapter.setCurrentItem(item);
            updatePreview(item);
          }
        }
      }
    });
    list.setSelection(mAdapter.getPosition(typeface));

    updatePreview(typeface);
  }

  private void updatePreview(String typeface) {
    File file = new File(TYPEFACE_FOLDER, typeface + ".ttf");
    try {
      Typeface tf = Typeface.createFromFile(file);
      mPreviewText.setTypeface(tf);
      mTypeface = typeface;
    } catch (Exception ex) {
      file.delete();
      Toast.makeText(mContext, R.string.picker_typeface_notfound, Toast.LENGTH_SHORT).show();
    }
  }

  public void onClick(DialogInterface dialog, int which) {
    if (which == DialogInterface.BUTTON_POSITIVE) {
      mListener.onTypefacePicked(mTypeface);
    }
    dismiss();
  }

  //-----------------------------------------------------------------------

  /**
   * Interface use to send data back to Activity
   *
   * @author Semeniuk A.D.
   */
  public interface OnTypefacePickerListener {
    /**
     * Called when user pressed OK button
     *
     * @param typeface Selected typeface
     */
    public void onTypefacePicked(String typeface);
  }

  //-----------------------------------------------------------------------

  /**
   * Task used to load list of available fonts
   *
   * @author Semeniuk A.D.
   */
  private class FontListLoaderTask extends AsyncTask<Void, Void, List<String>> {

    private Context mContext;
    private ProgressDialog mDialog;

    public FontListLoaderTask(Context context) {
      this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
      mDialog = new ProgressDialog(mContext);
      mDialog.setMessage(mContext.getString(R.string.dialog_loading));
      mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          FontListLoaderTask.this.cancel(true);
        }
      });
      mDialog.show();
    }

    @Override
    protected List<String> doInBackground(Void... params) {
      try {
        return ServerConnector.getAvailableFonts();
      } catch (Exception ex) {
        return null;
      }
    }

    @Override
    protected void onCancelled() {
      Toast.makeText(mContext, R.string.dialog_canceled, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostExecute(List<String> result) {
      mDialog.dismiss();
      if (result != null) { //Success
        if (mAdapter.appendAll(result) == 0) { //No new typefaces
          Toast.makeText(mContext, R.string.picker_typeface_nonew, Toast.LENGTH_SHORT).show();
        }
      } else { //Error happened
        Toast.makeText(mContext, R.string.reader_error_sync, Toast.LENGTH_SHORT).show();
      }
    }

  }

  //-----------------------------------------------------------------------

  /**
   * Task used to load list of available fonts
   *
   * @author Semeniuk A.D.
   */
  private class FontLoaderTask extends AsyncTask<String, Void, Boolean> {

    private Context mContext;
    private int mPosition;
    private ProgressDialog mDialog;

    public FontLoaderTask(Context context, int position) {
      this.mContext = context;
      this.mPosition = position;
    }

    @Override
    protected void onPreExecute() {
      mDialog = new ProgressDialog(mContext);
      mDialog.setMessage(mContext.getString(R.string.dialog_loading));
      mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          FontLoaderTask.this.cancel(true);
        }
      });
      mDialog.show();
    }

    @Override
    protected Boolean doInBackground(String... params) {
      try {
        String name = params[0];
        File target = new File(TYPEFACE_FOLDER, name + ".ttf");
        FileOperations.saveFile(ServerConnector.loadFontFile(name), target);
        return true;
      } catch (Exception ex) {
        return false;
      }
    }

    @Override
    protected void onCancelled() {
      Toast.makeText(mContext, R.string.dialog_canceled, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostExecute(Boolean result) {
      mDialog.dismiss();
      if (result) { //Success
        updatePreview(mAdapter.remoteLoaded(mPosition));
      } else { //Error happened
        Toast.makeText(mContext, R.string.reader_error_sync, Toast.LENGTH_SHORT).show();
      }
    }

  }

}
