package org.alexsem.libcs.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;

public class ActionToolbar extends LinearLayout {

	public enum Orientation {
		TOP_LEFT, TOP_RIGHT, BOTTOM_RIGHT, BOTTOM_LEFT
	}

	private float hotPointDistX = 0f;
	private float hotPointDistY = 0f;

	private boolean opened = true;
	private int duration = 200;
	private float pivotX = hotPointDistX;
	private float pivotY = hotPointDistY;
	private float width = 1f;
	private float height = 1f;

	private boolean requestHide = true;

	public ActionToolbar(Context context) {
		super(context);
	}

	public ActionToolbar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if (w > 0 && h > 0) {
			this.width = w;
			this.height = h;
			if (requestHide) { //Need to hide bar
				requestHide = false;
				hideWithoutAnimation();
			}
		}
	}

	public boolean isOpened() {
		return opened;
	}

	/**
	 * Sets HotPoint (point which is used as center for scaling animation)
	 * Point is set relative to TOP-LEFT orientation
	 * @param distX Distance from top-left corner on X-axis
	 * @param distY Distance from top-left corner on Y-axis
	 */
	public void setHotPoint(float distX, float distY) {
		this.hotPointDistX = distX;
		this.hotPointDistY = distY;
	}

	/**
	 * Shows panel by performing the animation
	 */
	public void show() {
		if (!opened) { //If panel is not shown
			ScaleAnimation anim = new ScaleAnimation(0f, 1.0f, 0f, 1.0f, Animation.RELATIVE_TO_SELF, pivotX / this.width, Animation.RELATIVE_TO_SELF,
					pivotY / this.height);
			anim.setAnimationListener(showAnimationListener);
			anim.setDuration(duration);
			anim.setInterpolator(new AccelerateInterpolator(1.0f));
			changeStateToShown();
			this.startAnimation(anim);
			opened = true;
		}
	}

	/**
	 * Hides panel by performing the animation
	 */
	public void hide() {
		if (requestHide) {
			hideWithoutAnimation();
			return;
		}
		if (opened) { //If panel is shown
			ScaleAnimation anim = new ScaleAnimation(1.0f, 0f, 1.0f, 0f, Animation.RELATIVE_TO_SELF, pivotX / this.width, Animation.RELATIVE_TO_SELF,
					pivotY / this.height);
			anim.setAnimationListener(hideAnimationListener);
			anim.setDuration(duration);
			anim.setInterpolator(new AccelerateInterpolator(1.0f));
			this.startAnimation(anim);
			opened = false;
		}
	}

	/**
	 * Requests toolbar to be instantly hidden during next layout operation
	 */
	public void requestHide() {
		this.requestHide = true;
	}

	/**
	 * Shows panel without performing the animation
	 */
	public void showWithoutAnimation() {
		if (!opened) { //If panel is shown
			changeStateToShown();
			opened = true;
		}
	}

	/**
	 * Hides panel without performing the animation
	 */
	private void hideWithoutAnimation() {
		if (opened) { //If panel is hidden
			changeStateToHidden();
			opened = false;
		}
	}

	/**
	 * Hide panel if it visible or show if it is hidden
	 */
	public void toggle() {
		if (opened) {
			hide();
		} else {
			show();
		}
	}

	/**
	 * Sets the "speed" of hiding/showing animations
	 * @param duration Animation duration in milliseconds
	 */
	public void setAnimationDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * Sets the "orientation" of action bar
	 * @param orientation LEFT or RIGHT
	 */
	public void setOrientation(Orientation orientation) {
		switch (orientation) {
			case TOP_LEFT:
				this.pivotX = hotPointDistX;
				this.pivotY = hotPointDistY;
				if (this.getBackground() != null) {
					this.getBackground().setLevel(0);
				}
				break;
			case TOP_RIGHT:
				this.pivotX = this.getWidth() - hotPointDistX;
				this.pivotY = hotPointDistY;
				if (this.getBackground() != null) {
					this.getBackground().setLevel(1);
				}
				break;
			case BOTTOM_RIGHT:
				this.pivotX = this.getWidth() - hotPointDistX;
				this.pivotY = this.getHeight() - hotPointDistY;
				if (this.getBackground() != null) {
					this.getBackground().setLevel(2);
				}
				break;
			case BOTTOM_LEFT:
				this.pivotX = hotPointDistX;
				this.pivotY = this.getHeight() - hotPointDistY;
				if (this.getBackground() != null) {
					this.getBackground().setLevel(3);
				}
				break;
		}
		this.invalidate();
	}

	/**
	 * Performs all necessary layout and view transformations when panel is about
	 * to be hidden
	 */
	private void changeStateToHidden() {
		this.setVisibility(View.GONE);
	}

	/**
	 * Performs all necessary layout and view transformations when panel is about
	 * to be shown
	 */
	private void changeStateToShown() {
		this.setVisibility(View.VISIBLE);
		this.bringToFront();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		return true;
	}

	/**
	 * Class used to trigger events after showing animation happened
	 * @author Semeniuk A.D.
	 */
	private class ShowAnimationListener implements AnimationListener {
		@Override
		public void onAnimationStart(Animation animation) {
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			//Do nothing
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			if (onShowAnimationEndsListener != null) {
				onShowAnimationEndsListener.animationEnds();
			}
		}
	}

	private ShowAnimationListener showAnimationListener = new ShowAnimationListener();

	/**
	 * Class used to trigger events after hiding animation happened
	 * @author Semeniuk A.D.
	 */
	private class HideAnimationListener implements AnimationListener {
		@Override
		public void onAnimationStart(Animation animation) {
			//Do nothing
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			//Do nothing
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			changeStateToHidden();
			if (onHideAnimationEndsListener != null) {
				onHideAnimationEndsListener.animationEnds();
			}
		}
	}

	private HideAnimationListener hideAnimationListener = new HideAnimationListener();

	/**
	 * Interface for animation end events
	 * @author Semeniuk A.D.
	 * 
	 */
	public interface OnAnimationEndListener {
		public void animationEnds();
	}

	private OnAnimationEndListener onHideAnimationEndsListener;
	private OnAnimationEndListener onShowAnimationEndsListener;

	public void setOnHideAnimationEndsListener(OnAnimationEndListener l) {
		this.onHideAnimationEndsListener = l;
	}

	public void setOnShowAnimationEndsListener(OnAnimationEndListener l) {
		this.onShowAnimationEndsListener = l;
	}

}
